﻿using System;
namespace HmiApiLib.Common.Enums
{
    public enum AudioStreamingIndicator
	{
        PLAY_PAUSE,
        PLAY,
        PAUSE,
        STOP
	}
}
