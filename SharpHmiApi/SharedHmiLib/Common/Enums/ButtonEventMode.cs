﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum ButtonEventMode
	{
		BUTTONUP,
		BUTTONDOWN
	}
}