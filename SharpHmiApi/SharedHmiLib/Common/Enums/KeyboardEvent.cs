﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum KeyboardEvent
	{
		KEYPRESS,
		ENTRY_SUBMITTED,
		ENTRY_VOICE,
		ENTRY_CANCELLED,
		ENTRY_ABORTED
	}
}