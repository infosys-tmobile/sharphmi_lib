﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum CharacterSet
	{
		TYPE2SET,
		TYPE5SET,
		CID1SET,
		CID2SET
	}
}
