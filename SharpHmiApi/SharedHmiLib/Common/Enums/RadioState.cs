﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum RadioState
	{
		ACQUIRING,
		ACQUIRED,
        MULTICAST,
        NOT_FOUND
	}
}
