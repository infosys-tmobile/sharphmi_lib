﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum HMILevel
	{
		FULL,
		LIMITED,
		BACKGROUND,
		NONE
	}
}
