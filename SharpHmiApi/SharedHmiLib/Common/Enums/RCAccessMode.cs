﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum RCAccessMode
	{
		AUTO_ALLOW,
		AUTO_DENY,
        ASK_DRIVER
	}
}
