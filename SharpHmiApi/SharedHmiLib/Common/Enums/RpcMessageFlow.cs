﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum RpcMessageFlow
	{
		INCOMING,
		OUTGOING
	}
}
