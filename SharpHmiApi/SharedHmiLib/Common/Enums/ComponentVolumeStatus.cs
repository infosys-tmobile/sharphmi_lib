﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum ComponentVolumeStatus
	{
		UNKNOWN,
		NORMAL,
		LOW,
		FAULT,
		ALERT,
		NOT_SUPPORTED
	}
}