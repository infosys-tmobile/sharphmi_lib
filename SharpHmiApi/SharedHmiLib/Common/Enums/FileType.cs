﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum FileType
	{
		GRAPHIC_BMP,
		GRAPHIC_JPEG,
		GRAPHIC_PNG,
		AUDIO_WAVE,
		AUDIO_MP3,
		AUDIO_AAC,
		BINARY,
		JSON
	}
}