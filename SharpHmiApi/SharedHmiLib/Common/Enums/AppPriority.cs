﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum AppPriority
	{
		EMERGENCY,
		NAVIGATION,
		VOICE_COMMUNICATION,
		COMMUNICATION,
		NORMAL,
		NONE
	}
}
