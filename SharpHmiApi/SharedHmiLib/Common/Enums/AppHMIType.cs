﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum AppHMIType
	{
		DEFAULT,
		COMMUNICATION,
		MEDIA,
		MESSAGING,
		NAVIGATION,
		INFORMATION,
		SOCIAL,
		BACKGROUND_PROCESS,
		TESTING,
		SYSTEM,
        PROJECTION,
        REMOTE_CONTROL
	}
}
