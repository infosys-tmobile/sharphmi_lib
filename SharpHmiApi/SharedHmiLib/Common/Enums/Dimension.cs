﻿using System;
using System.Runtime.Serialization;

namespace HmiApiLib.Common.Enums
{
	public enum Dimension
	{
		[EnumMember(Value = "NO_FIX")]
		Dimension_NO_FIX,
		[EnumMember(Value = "2D")]
		Dimension_2D,
		[EnumMember(Value = "3D")]
		Dimension_3D
	}
}