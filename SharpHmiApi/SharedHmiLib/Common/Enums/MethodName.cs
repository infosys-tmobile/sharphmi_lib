﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum MethodName
	{
		ALERT,
		SPEAK,
		AUDIO_PASS_THRU,
		ALERT_MANEUVER
	}
}
