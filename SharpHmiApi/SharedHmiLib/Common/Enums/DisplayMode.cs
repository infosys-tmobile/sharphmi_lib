﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum DisplayMode
	{
		DAY,
        NIGHT,
        AUTO
	}
}