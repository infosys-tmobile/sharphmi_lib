﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum PRNDL
	{
		PARK,
		REVERSE,
		NEUTRAL,
		DRIVE,
		SPORT,
		LOWGEAR,
		FIRST,
		SECOND,
		THIRD,
		FOURTH,
		FIFTH,
		SIXTH,
		SEVENTH,
		EIGHTH,
		FAULT
	}
}
