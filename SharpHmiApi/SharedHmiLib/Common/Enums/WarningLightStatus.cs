﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum WarningLightStatus
	{
		OFF,
		ON,
		FLASH,
		NOT_USED
	}
}
