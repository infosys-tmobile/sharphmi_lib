﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum LightStatus
	{
		ON,
		OFF,
        RAMP_UP,
        RAMP_DOWN,
        UNKNOWN,
        INVALID
	}
}
