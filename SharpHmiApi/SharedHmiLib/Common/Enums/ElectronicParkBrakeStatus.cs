﻿using System;
namespace HmiApiLib.Common.Enums
{
    public enum ElectronicParkBrakeStatus
    {
        CLOSED,
        TRANSITION,
        OPEN,
        DRIVE_ACTIVE,
        FAULT
    }
}
