﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum TemperatureUnit
	{
		FAHRENHEIT,
		CELSIUS
	}
}
