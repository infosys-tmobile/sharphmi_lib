﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum EmergencyEventType
	{
		NO_EVENT,
		FRONTAL,
		SIDE,
		REAR,
		ROLLOVER,
		NOT_SUPPORTED,
		FAULT
	}
}