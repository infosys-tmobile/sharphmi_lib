﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum RpcMessageType
	{
		REQUEST,
		RESPONSE,
		NOTIFICATION,
		REQUEST_NOTIFY
	}
}
