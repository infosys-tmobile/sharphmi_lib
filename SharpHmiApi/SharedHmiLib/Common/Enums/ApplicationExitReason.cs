﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum ApplicationExitReason
	{
		DRIVER_DISTRACTION_VIOLATION,
		USER_EXIT,
		UNAUTHORIZED_TRANSPORT_REGISTRATION,
		UNSUPPORTED_HMI_RESOURCE
	}
}
