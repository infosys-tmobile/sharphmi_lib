﻿using System;
namespace HmiApiLib.Common.Enums
{
    public enum TurnSignal
    {
        OFF,
        LEFT,
        RIGHT,
        BOTH
    }
}
