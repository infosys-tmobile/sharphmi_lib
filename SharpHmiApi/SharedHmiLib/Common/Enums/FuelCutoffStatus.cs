﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum FuelCutoffStatus
	{
		TERMINATE_FUEL,
		NORMAL_OPERATION,
		FAULT
	}
}