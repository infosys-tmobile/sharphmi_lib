﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum ECallConfirmationStatus
	{
		NORMAL,
		CALL_IN_PROGRESS,
		CALL_CANCELLED,
		CALL_COMPLETED,
		CALL_UNSUCCESSFUL,
		ECALL_CONFIGURED_OFF,
		CALL_COMPLETE_DTMF_TIMEOUT
	}
}