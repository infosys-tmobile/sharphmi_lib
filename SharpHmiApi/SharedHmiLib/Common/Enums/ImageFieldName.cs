﻿using System;
namespace HmiApiLib.Common.Enums
{
public enum ImageFieldName
	{
		softButtonImage,
		choiceImage,
		choiceSecondaryImage,
		vrHelpItem,
		turnIcon,
		menuIcon,
		cmdIcon,
		appIcon,
		graphic,
		showConstantTBTIcon,
		showConstantTBTNextTurnIcon,
		locationImage
	}
}
