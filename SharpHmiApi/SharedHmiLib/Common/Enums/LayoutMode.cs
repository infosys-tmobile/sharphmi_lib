﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum LayoutMode
	{
		ICON_ONLY,
		ICON_WITH_SEARCH,
		LIST_ONLY,
		LIST_WITH_SEARCH,
		KEYBOARD
	}
}
