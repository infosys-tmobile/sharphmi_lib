﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum TextAlignment
	{
		LEFT_ALIGNED,
		RIGHT_ALIGNED,
		CENTERED
	}
}
