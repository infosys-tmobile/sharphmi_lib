﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum PrimaryAudioSource
	{
		NO_SOURCE_SELECTED,
        CD,
		USB,
		USB2,
		BLUETOOTH_STEREO_BTST,
		LINE_IN,
		IPOD,
		MOBILE_APP,
        RADIO_TUNER
	}
}
