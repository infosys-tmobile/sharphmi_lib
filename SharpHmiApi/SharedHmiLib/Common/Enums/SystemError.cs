﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum SystemError
	{
		SYNC_REBOOTED,
		SYNC_OUT_OF_MEMMORY
	}
}
