﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum SystemAction
	{
		DEFAULT_ACTION,
		STEAL_FOCUS,
		KEEP_CONTEXT
	}
}
