﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum IgnitionStatus
	{
		UNKNOWN,
		OFF,
		ACCESSORY,
		RUN,
		START,
		INVALID
	}
}
