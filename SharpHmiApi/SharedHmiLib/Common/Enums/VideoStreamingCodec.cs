﻿using System;
namespace HmiApiLib.Common.Enums
{
    public enum VideoStreamingCodec
	{
        H264,
        H265,
        Theora,
        VP8,
        VP9
	}
}
