﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum EventTypes
	{
		AUDIO_SOURCE,
		EMBEDDED_NAVI,
		PHONE_CALL,
		EMERGENCY_EVENT,
		DEACTIVATE_HMI
	}
}
