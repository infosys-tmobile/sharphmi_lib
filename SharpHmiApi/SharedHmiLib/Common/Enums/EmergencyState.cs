﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum EmergencyState
	{
		EMERGENCY_ON,
		EMERGENCY_OFF
	}
}