﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum DistanceUnit
	{
		MILES,
        KILOMETERS
	}
}