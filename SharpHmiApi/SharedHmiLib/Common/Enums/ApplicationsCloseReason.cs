﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum ApplicationsCloseReason
	{
		IGNITION_OFF,
		MASTER_RESET,
		FACTORY_DEFAULTS,
		SUSPEND
	}
}
