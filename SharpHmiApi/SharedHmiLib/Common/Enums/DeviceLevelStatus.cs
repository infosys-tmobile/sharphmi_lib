﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum DeviceLevelStatus
	{
		ZERO_LEVEL_BARS,
		ONE_LEVEL_BARS,
		TWO_LEVEL_BARS,
		THREE_LEVEL_BARS,
		FOUR_LEVEL_BARS,
		NOT_PROVIDED
	}
}
