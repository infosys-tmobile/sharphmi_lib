﻿using System;
namespace HmiApiLib.Common.Enums
{
	public enum ClockUpdateMode
	{
		COUNTUP,
		COUNTDOWN,
		PAUSE,
		RESUME,
		CLEAR
	}
}