﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class Image : RpcStruct
	{
		public String value;
		public ImageType? imageType;
        public bool? isTemplate;

		public Image()
		{
			
		}

		public String getValue()
		{
			return value;
		}

		public ImageType? getImageType()
		{
			return imageType;		}

        public bool? getIsTemplate()
        {
            return isTemplate;
        }
	}
}
