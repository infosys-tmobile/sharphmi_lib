﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class AudioPassThruCapabilities : RpcStruct
	{
		public SamplingRate? samplingRate;
		public BitsPerSample? bitsPerSample;
		public AudioType? audioType;

		public AudioPassThruCapabilities()
		{
		}

		public SamplingRate? getSamplingRate()
		{
			return samplingRate;
		}

		public BitsPerSample? getBitsPerSample()
		{
			return bitsPerSample;		}

		public AudioType? getAudioType()
		{
			return audioType;		}

	}
}