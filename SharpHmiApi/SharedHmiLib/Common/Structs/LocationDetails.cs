﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class LocationDetails : RpcStruct
	{
		public Coordinate coordinate;
		public string locationName;
		public List<string> addressLines;
		public string locationDescription;
		public string phoneNumber;
		public Image locationImage;
		public OASISAddress searchAddress;

		public LocationDetails()
		{
		}

		public Coordinate getCoordinate()
		{
			return coordinate;
		}

		public string getLocationName()
		{
			return locationName;		}

		public List<string> getAddressLines()
		{
			return addressLines;		}

		public string getLocationDescription()
		{
			return locationDescription;		}

		public string getPhoneNumber()
		{
			return phoneNumber;		}

		public Image getLocationImage()
		{
			return locationImage;		}

		public OASISAddress getSearchAddress()
		{
			return searchAddress;		}

	}
}