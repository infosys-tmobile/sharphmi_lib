﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class EqualizerSettings : RpcStruct
	{
		public int? channelId;
		public string channelName;
        public int? channelSetting;

		public EqualizerSettings()
		{
		}

		public int? getChannelId()
		{
			return channelId;
		}

		public string getChannelName()
		{
			return channelName;		}

		public int? getChannelSetting()
		{
			return channelSetting;
		}
	}
}
