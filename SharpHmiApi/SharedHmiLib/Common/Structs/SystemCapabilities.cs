﻿using HmiApiLib.Base;
using System.Collections.Generic;

namespace HmiApiLib.Common.Structs
{
    public class SystemCapabilities : RpcStruct
	{
        public NavigationCapability navigationCapability;
        public PhoneCapability phoneCapability;
        public VideoStreamingCapability videoStreamingCapability;
		public List<DisplayCapability> displaycapability;
		public SeatLocationCapability seatLocationCapability;

		public SystemCapabilities()
		{
		}

        public NavigationCapability getNavigationCapability()
		{
            return navigationCapability;
		}
		public List<DisplayCapability> getDisplayCapability()
		{
			return displaycapability;
		}
		public PhoneCapability getPhoneCapability()
        {
            return phoneCapability;
        }
		public SeatLocationCapability getSeatCapability()
		{
			return seatLocationCapability;
		}
		public VideoStreamingCapability getVideoStreamingCapability()
        {
            return videoStreamingCapability;
        }
	}
}
