﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class ModuleData : RpcStruct
	{
		public ModuleType?         moduleType;
		public string moduleId;

		public RadioControlData   radioControlData;
        public ClimateControlData climateControlData;
        public SeatControlData    seatControlData;
        public AudioControlData   audioControlData;
        public LightControlData   lightControlData;
        public HMISettingsControlData hmiSettingsControlData;

		public ModuleData()
		{
		}

		public ModuleType? getModuleType()
		{
			return moduleType;
		}
		public string getModuleId()
		{
			return moduleId;
		}
		public RadioControlData getRadioControlData()
		{
			return radioControlData;
		}

		public ClimateControlData getClimateControlData()
		{
			return climateControlData;
		}

		public SeatControlData getSeatControlData()
		{
			return seatControlData;
		}

		public AudioControlData getAudioControlData()
		{
			return audioControlData;
		}

		public LightControlData getLightControlData()
		{
			return lightControlData;
		}

		public HMISettingsControlData getHmiSettingsControlData()
		{
			return hmiSettingsControlData;
		}
    }
}
