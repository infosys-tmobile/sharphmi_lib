﻿using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class SisData : RpcStruct
	{
		public string stationShortName;
        public StationIDNumber stationIDNumber;
        public string stationLongName;
        public GPSData stationLocation;
        public string stationMessage;

		public SisData()
		{
		}

		public string getStationShortName()
		{
			return stationShortName;
		}

		public StationIDNumber getStationIDNumber()
		{
			return stationIDNumber;
		}

		public string getStationLongName()
		{
			return stationLongName;
		}

        public GPSData getStationLocation()
		{
			return stationLocation;
		}

		public string getStationMessage()
		{
			return stationMessage;
		}
	}
}
