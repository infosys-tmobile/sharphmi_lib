﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class LightState : RpcStruct
	{
		public LightName? id;
        public LightStatus? status;
        public float? density;
        public RGBColor color;

		public LightState()
		{
		}

		public LightName? getId()
		{
			return id;
		}

		public LightStatus? getStatus()
		{
			return status;
		}

		public float? getDensity()
		{
			return density;
		}

		public RGBColor getRGBColor()
		{
            return color;
		}
	}
}
