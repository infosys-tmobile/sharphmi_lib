﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using System.Collections.Generic;

namespace HmiApiLib.Common.Structs
{
	public class ClimateControlCapabilities : RpcStruct
	{
		public string moduleName;
        public bool? currentTemperatureAvailable;
        public bool? fanSpeedAvailable;
		public bool? desiredTemperatureAvailable;
        public bool? acEnableAvailable;
        public bool? acMaxEnableAvailable;
        public bool? circulateAirEnableAvailable;
        public bool? autoModeEnableAvailable;
        public bool? dualModeEnableAvailable;
        public bool defrostZoneAvailable;
        public List<DefrostZone> defrostZone;
        public bool? ventilationModeAvailable;
        public List<VentilationMode> ventilationMode;
        public bool? heatedSteeringWheelAvailable;
        public bool? heatedWindshieldAvailable;
        public bool? heatedRearWindowAvailable;
        public bool? heatedMirrorsAvailable;
        public bool? climateEnableAvailable;
		public ModuleInfo moduleInfo;

		public ClimateControlCapabilities()
		{
		}

		public string getModuleName()
		{
			return moduleName;
		}
		public ModuleInfo getModuleInfo()
		{
			return moduleInfo;
		}
		
		public bool? getCurrentTemperatureAvailable()
        {
            return currentTemperatureAvailable;
        }

		public bool? getFanSpeedAvailable()
		{
			return fanSpeedAvailable;		}

		public bool? getDesiredTemperatureAvailable()
		{
			return desiredTemperatureAvailable;
		}

		public bool? getAcEnableAvailable()
		{
			return acEnableAvailable;
		}

		public bool? getAcMaxEnableAvailable()
		{
			return acMaxEnableAvailable;
		}

		public bool? getCirculateAirEnableAvailable()
		{
			return circulateAirEnableAvailable;
		}

		public bool? getAutoModeEnableAvailable()
		{
			return autoModeEnableAvailable;
		}

		public bool? getDualModeEnableAvailable()
		{
			return dualModeEnableAvailable;
		}

		public bool? getDefrostZoneAvailable()
		{
			return defrostZoneAvailable;
		}

		public List<DefrostZone> getDefrostZone()
		{
			return defrostZone;
		}

		public bool? getVentilationModeAvailable()
		{
			return ventilationModeAvailable;
		}

		public List<VentilationMode> getVentilationMode()
		{
			return ventilationMode;
		}

		public bool? getHeatedSteeringWheelAvailable()
		{
			return heatedSteeringWheelAvailable;
		}

		public bool? getHeatedWindshieldAvailable()
		{
			return heatedWindshieldAvailable;
		}

		public bool? getHeatedRearWindowAvailable()
		{
			return heatedRearWindowAvailable;
		}

		public bool? getHeatedMirrorsAvailable()
		{
			return heatedMirrorsAvailable;
		}

        public bool? getClimateEnableAvailable()
        {
            return climateEnableAvailable;
        }
    }
}
