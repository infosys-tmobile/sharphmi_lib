﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class UserFriendlyMessage : RpcStruct
	{
		public string messageCode;
		public string ttsString;
		public string label;
		public string line1;
		public string line2;
		public string textBody;

		public UserFriendlyMessage()
		{
		}

		public string getMessageCode()
		{
			return messageCode;
		}

		public string geTtsString()
		{
			return ttsString;		}

		public string getLabel()
		{
			return label;		}

		public string getLine1()
		{
			return line1;		}

		public string getLine2()
		{
			return line2;		}

		public string getTextBody()
		{
			return textBody;		}

	}
}
