﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class VehicleType : RpcStruct
	{
		public string make;
		public string model;
		public string modelYear;
		public string trim;

		public VehicleType()
		{
		}

		public string getMake()
		{
			return make;
		}

		public string getModel()
		{
			return model;
		}

		public string getModelYear()
		{
			return modelYear;
		}

		public string getTrim()
		{
			return trim;
		}

	}
}