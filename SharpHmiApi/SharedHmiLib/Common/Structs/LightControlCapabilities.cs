﻿using System.Collections.Generic;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class LightControlCapabilities : RpcStruct
	{
		public string moduleName;
        public List<LightCapabilities> supportedLights;
		public ModuleInfo moduleInfo;
		public LightControlCapabilities()
		{
		}
		public ModuleInfo getModuleInfo()
		{
			return moduleInfo;
		}
		public string getModuleName()
		{
			return moduleName;
		}

		public List<LightCapabilities> getSupportedLights()
		{
			return supportedLights;
		}
	}
}
