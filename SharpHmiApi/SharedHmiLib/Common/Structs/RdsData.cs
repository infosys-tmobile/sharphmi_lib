﻿using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class RdsData : RpcStruct
	{
		public string PS;
		public string RT;
        public string CT;
        public string PI;
		public int?    PTY;
		public bool?   TP;
        public bool?   TA;
        public string REG;

		public RdsData()
		{
		}

		public string getProgramService()
		{
			return PS;
		}

		public string getRadioText()
		{
			return RT;
		}

		public string getClockText()
		{
			return CT;
		}

		public string getProgramIdentification()
		{
			return PI;
		}

		public int? getProgramType()
		{
			return PTY;
		}

		public bool? getTrafficProgram()
		{
			return TP;
		}

		public bool? getTrafficAnnouncement()
		{
			return TA;
		}

		public string getRegion()
		{
			return REG;
		}
	}
}
