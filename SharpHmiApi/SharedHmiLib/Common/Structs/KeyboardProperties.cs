﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class KeyboardProperties : RpcStruct
	{
		public Language? language;
		public KeyboardLayout? keyboardLayout;
		public KeypressMode? keypressMode;
		public List<string> limitedCharacterList;
		public string autoCompleteText;
        public List<string> autoCompleteList;

		public KeyboardProperties()
		{
		}

		public Language? getLanguage()
		{
			return language;
		}

		public KeyboardLayout? getKeyboardLayout()
		{
			return keyboardLayout;		}

		public KeypressMode? getKeypressMode()
		{
			return keypressMode;		}

		public List<string> getLimitedCharacterList()
		{
			return limitedCharacterList;		}

		public string getAutoCompleteText()
		{
			return autoCompleteText;		}

		public List<string> getAutoCompleteList()
		{
			return autoCompleteList;
		}
	}
}