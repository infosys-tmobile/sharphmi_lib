﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class SeatControlData : RpcStruct
	{
		public SupportedSeat? id;
        public Boolean? heatingEnabled;
		public Boolean? coolingEnabled;
        public int? heatingLevel;
        public int? coolingLevel;
        public int? horizontalPosition;
        public int? verticalPosition;
        public int? frontVerticalPosition;
        public int? backVerticalPosition;
        public int? backTiltAngle;
        public int? headSupportHorizontalPosition;
        public int? headSupportVerticalPosition;
        public Boolean? massageEnabled;
        public List<MassageModeData> massageMode;
        public List<MassageCushionFirmness> massageCushionFirmness;
        public SeatMemoryAction memory;

		public SeatControlData()
		{
		}

		public SupportedSeat? getId()
		{
			return id;		}

		public Boolean? getHeatingEnabled()
		{
			return heatingEnabled;
		}

		public Boolean? getCoolingEnabled()
		{
			return coolingEnabled;
		}

		public int? getHeatingLevel()
		{
			return heatingLevel;
		}

		public int? getCoolingLevel()
		{
			return coolingLevel;
		}

		public int? getHorizontalPosition()
		{
			return horizontalPosition;
		}

		public int? getVerticalPosition()
		{
			return verticalPosition;
		}

		public int? getFrontVerticalPosition()
		{
			return frontVerticalPosition;
		}

		public int? getBackVerticalPosition()
		{
			return backVerticalPosition;
		}

		public int? getBackTiltAngle()
		{
			return backTiltAngle;
		}

		public int? getHeadSupportHorizontalPosition()
		{
			return headSupportHorizontalPosition;
		}

		public int? getHeadSupportVerticalPosition()
		{
			return headSupportVerticalPosition;
		}

		public Boolean? getMassageEnabled()
		{
			return massageEnabled;
		}

		public List<MassageModeData> getMassageMode()
		{
			return massageMode;
		}

		public List<MassageCushionFirmness> getMassageCushionFirmness()
		{
			return massageCushionFirmness;
		}

		public SeatMemoryAction getMemory()
		{
			return memory;
		}
	}
}