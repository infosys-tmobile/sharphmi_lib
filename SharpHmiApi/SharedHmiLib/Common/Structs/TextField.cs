﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class TextField : RpcStruct
	{
		public TextFieldName? name;
		public CharacterSet? characterSet;
		public int? width;
		public int? rows;

		public TextField()
		{
		}

		public TextFieldName? getName()
		{
			return name;
		}

		public CharacterSet? getCharacterSet()
		{
			return characterSet;		}

		public int? getWidth()
		{
			return width;		}

		public int? getRows()
		{
			return rows;		}

	}
}
