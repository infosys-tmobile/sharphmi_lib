﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class MassageModeData : RpcStruct
	{
		public MassageZone? massageZone;
		public MassageMode? massageMode;

		public MassageModeData()
		{
		}

		public MassageZone? getMassageZone()
		{
			return massageZone;
		}

		public MassageMode? getMassageMode()
		{
			return massageMode;		}
	}
}