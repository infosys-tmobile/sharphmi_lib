﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class HeadLampStatus : RpcStruct
	{
		public Boolean? lowBeamsOn;
		public Boolean? highBeamsOn;
		public AmbientLightStatus? ambientLightSensorStatus;

		public HeadLampStatus()
		{
		}

		public Boolean? getLowBeamsOn()
		{
			return lowBeamsOn;
		}

		public Boolean? getHighBeamsOn()
		{
			return highBeamsOn;		}

		public AmbientLightStatus? getAmbientLightSensorStatus()
		{
			return ambientLightSensorStatus;		}

	}
}