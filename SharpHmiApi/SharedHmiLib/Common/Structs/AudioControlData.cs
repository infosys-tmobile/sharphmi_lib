﻿using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class AudioControlData : RpcStruct
	{
		public PrimaryAudioSource? source;
		public bool? keepContext;
		public int? volume;
        public List<EqualizerSettings> equalizerSettings;

		public AudioControlData()
		{
		}

		public PrimaryAudioSource? getSource()
		{
			return source;
		}

		public bool? getKeepContext()
		{
			return keepContext;
		}

		public int? getVolume()
		{
			return volume;
		}

		public List<EqualizerSettings> getEqualizerSettings()
		{
			return equalizerSettings;
		}
	}
}
