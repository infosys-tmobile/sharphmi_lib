﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class ImageResolution : RpcStruct
	{
		public int? resolutionWidth;
		public int? resolutionHeight;

		public ImageResolution()
		{
		}

		public int? getResolutionWidth()
		{
			return resolutionWidth;
		}

		public int? getResolutionHeight()
		{
			return resolutionHeight;		}

	}
}