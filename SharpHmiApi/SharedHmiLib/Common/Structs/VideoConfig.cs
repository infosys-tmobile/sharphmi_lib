﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
    public class VideoConfig : RpcStruct
	{
        public VideoStreamingProtocol? videoStreamingProtocol;
        public VideoStreamingCodec? videoStreamingCodec;
        public int? width;
        public int? height;

        public VideoConfig()
		{
		}

        public VideoStreamingProtocol? getVideoStreamingProtocol()
		{
            return videoStreamingProtocol;
		}

        public VideoStreamingCodec? getVideoStreamingCodec()
        {
            return videoStreamingCodec;
        }

        public int? getWidth()
		{
            return width;
		}

        public int? getHeight()
        {
            return height;
        }
	}
}
