﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class RGBColor : RpcStruct
	{
		public int? red;
		public int? green;
		public int? blue;

        public RGBColor()
		{
		}

		public int? getRed()
		{
			return red;
		}

		public int? getGreen()
		{
			return green;		}

		public int? getBlue()
		{
			return blue;		}
	}
}
