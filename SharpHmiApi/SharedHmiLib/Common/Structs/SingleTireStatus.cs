﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class SingleTireStatus : RpcStruct
	{
		public ComponentVolumeStatus? status;

		public SingleTireStatus()
		{
		}

		public ComponentVolumeStatus? getStatus()
		{
			return status;
		}
	}
}