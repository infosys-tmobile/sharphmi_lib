﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class DisplayCapabilities : RpcStruct
	{
		public DisplayType? displayType;
		public List<TextField> textFields;
		public List<ImageField> imageFields;
		public List<MediaClockFormat> mediaClockFormats;
		public List<ImageType> imageCapabilities;
		public Boolean? graphicSupported;
		public List<string> templatesAvailable;
		public ScreenParams screenParams;
		public int? numCustomPresetsAvailable;
		//public string displayName;
	//	public WindowTypeCapabilities windowTypeSupported;
	//	public WindowCapability windowCapabilities;
		public DisplayCapabilities()
		{
		}

		public DisplayType? getDisplayType()
		{
			return displayType;
		}
		
		
		
		public List<TextField> getTextFields()
		{
			return textFields;		}

		public List<ImageField> getImageFields()
		{
			return imageFields;		}

		public List<MediaClockFormat> getMediaClockFormats()
		{
			return mediaClockFormats;		}

		public List<ImageType> getImageCapabilities()
		{
			return imageCapabilities;		}

		public Boolean? getGraphicSupported()
		{
			return graphicSupported;		}

		public List<string> getTemplatesAvailable()
		{
			return templatesAvailable;		}

		public ScreenParams getScreenParams()
		{
			return screenParams;		}
		

		public int? getNumCustomPresetsAvailable()
		{
			return numCustomPresetsAvailable;		}

	}
}