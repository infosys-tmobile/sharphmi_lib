﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
    public class VideoStreamingFormat : RpcStruct
	{
        public VideoStreamingProtocol? protocol;
        public VideoStreamingCodec? codec;

        public VideoStreamingFormat()
		{
		}

        public VideoStreamingProtocol? getProtocol()
		{
            return protocol;
		}

        public VideoStreamingCodec? getCodec()
        {
            return codec;
        }
	}
}
