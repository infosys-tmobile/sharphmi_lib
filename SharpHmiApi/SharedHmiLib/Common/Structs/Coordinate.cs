using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class Coordinate : RpcStruct
	{
		public float? latitudeDegrees;
		public float? longitudeDegrees;

		public Coordinate()
		{

		}

		public float? getLatitudeDegrees()
		{
			return latitudeDegrees;
		}

		public float? getLongitudeDegrees()
		{
			return longitudeDegrees;		}

	}
}