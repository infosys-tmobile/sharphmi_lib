﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class TimeFormat : RpcStruct
	{
		public int? hours;
		public int? minutes;
		public int? seconds;

		public TimeFormat()
		{
		}

		public int? getHours()
		{
			return hours;
		}

		public int? getMinutes()
		{
			return minutes;		}

		public int? getSeconds()
		{
			return seconds;		}

	}
}
