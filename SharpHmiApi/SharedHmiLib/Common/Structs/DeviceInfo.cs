﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class DeviceInfo : RpcStruct
	{
		public String name;
		public String id;
		public TransportType? transportType;
		public Boolean? isSDLAllowed;


		public DeviceInfo()
		{
		}

		public String getName()
		{
			return name;
		}

		public String getId()
		{
			return id;		}

		public TransportType? getTransportType()
		{
			return transportType;		}

		public Boolean? getIsSDLAllowed()
		{
			return isSDLAllowed;		}

	}
}
