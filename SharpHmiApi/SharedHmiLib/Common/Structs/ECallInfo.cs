﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class ECallInfo : RpcStruct
	{
		public VehicleDataNotificationStatus? eCallNotificationStatus;
		public VehicleDataNotificationStatus? auxECallNotificationStatus;
		public ECallConfirmationStatus? eCallConfirmationStatus;

		public ECallInfo()
		{
		}

		public VehicleDataNotificationStatus? getECallNotificationStatus()
		{
			return eCallNotificationStatus;
		}

		public VehicleDataNotificationStatus? getAuxECallNotificationStatus()
		{
			return auxECallNotificationStatus;		}

		public ECallConfirmationStatus? getECallConfirmationStatus()
		{
			return eCallConfirmationStatus;		}

	}
}