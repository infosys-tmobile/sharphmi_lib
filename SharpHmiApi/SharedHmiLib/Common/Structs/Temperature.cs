﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class Temperature : RpcStruct
	{
		public TemperatureUnit? unit;
		public float? value;

		public Temperature()
		{
		}

		public TemperatureUnit? getUnit()
		{
			return unit;
		}

		public float? getValue()
		{
			return value;		}
	}
}
