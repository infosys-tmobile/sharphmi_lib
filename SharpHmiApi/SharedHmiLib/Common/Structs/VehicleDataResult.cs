﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class VehicleDataResult : RpcStruct
	{
		public VehicleDataType? dataType;
		public VehicleDataResultCode? resultCode;

		public VehicleDataResult()
		{
		}

		public VehicleDataType? getDataType()
		{
			return dataType;
		}

		public VehicleDataResultCode? getResultCode()
		{
			return resultCode;		}

	}
}
