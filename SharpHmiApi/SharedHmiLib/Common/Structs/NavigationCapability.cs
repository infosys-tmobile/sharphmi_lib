﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
    public class NavigationCapability : RpcStruct
	{
        public Boolean? sendLocationEnabled;
        public Boolean? getWayPointsEnabled;

        public NavigationCapability()
		{
		}

        public Boolean? getSendLocationEnabled()
		{
            return sendLocationEnabled;
		}

        public Boolean? getGetWayPointsEnabled()
        {
            return getWayPointsEnabled;
        }

	}
}