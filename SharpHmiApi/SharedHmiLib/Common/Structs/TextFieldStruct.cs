﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class TextFieldStruct : RpcStruct
	{
		public TextFieldName? fieldName;
		public string fieldText;

		public TextFieldStruct()
		{
		}

		public TextFieldName? getFieldName()
		{
			return fieldName;
		}

		public string getFieldText()
		{
			return fieldText;		}

	}
}
