﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class HMISettingsControlData : RpcStruct
	{
		public DisplayMode? displayMode;
		public TemperatureUnit? temperatureUnit;
		public DistanceUnit? distanceUnit;

		public HMISettingsControlData()
		{
		}

		public DisplayMode? getDisplayMode()
		{
			return displayMode;
		}

		public TemperatureUnit? getTemperatureUnit()
		{
			return temperatureUnit;
		}

		public DistanceUnit? getDistanceUnit()
		{
			return distanceUnit;
		}
	}
}
