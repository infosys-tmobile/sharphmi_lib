﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class TouchEvent : RpcStruct
	{
		public int? id;
		public List<int> ts;
		public List<TouchCoord> c;

		public TouchEvent()
		{
		}

		public int? getId()
		{
			return id;
		}

		public List<int> getTs()
		{
			return ts;		}

		public List<TouchCoord> getC()
		{
			return c;		}

	}
}