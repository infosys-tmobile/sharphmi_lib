﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class BeltStatus : RpcStruct
	{
		public VehicleDataEventStatus? driverBeltDeployed;
		public VehicleDataEventStatus? passengerBeltDeployed;
		public VehicleDataEventStatus? passengerBuckleBelted;
		public VehicleDataEventStatus? driverBuckleBelted;
		public VehicleDataEventStatus? leftRow2BuckleBelted;
		public VehicleDataEventStatus? passengerChildDetected;
		public VehicleDataEventStatus? rightRow2BuckleBelted;
		public VehicleDataEventStatus? middleRow2BuckleBelted;
		public VehicleDataEventStatus? middleRow3BuckleBelted;
		public VehicleDataEventStatus? leftRow3BuckleBelted;
		public VehicleDataEventStatus? rightRow3BuckleBelted;
		public VehicleDataEventStatus? leftRearInflatableBelted;
		public VehicleDataEventStatus? rightRearInflatableBelted;
		public VehicleDataEventStatus? middleRow1BeltDeployed;
		public VehicleDataEventStatus? middleRow1BuckleBelted;

		public BeltStatus()
		{
		}

		public VehicleDataEventStatus? getDriverBeltDeployed()
		{
			return driverBeltDeployed;
		}

		public VehicleDataEventStatus? getPassengerBeltDeployed()
		{
			return passengerBeltDeployed;		}

		public VehicleDataEventStatus? getPassengerBuckleBelted()
		{
			return passengerBuckleBelted;		}

		public VehicleDataEventStatus? getDriverBuckleBelted()
		{
			return driverBuckleBelted;		}

		public VehicleDataEventStatus? getLeftRow2BuckleBelted()
		{
			return leftRow2BuckleBelted;		}

		public VehicleDataEventStatus? getPassengerChildDetected()
		{
			return passengerChildDetected;		}

		public VehicleDataEventStatus? getRightRow2BuckleBelted()
		{
			return rightRow2BuckleBelted;		}

		public VehicleDataEventStatus? getMiddleRow2BuckleBelted()
		{
			return middleRow2BuckleBelted;		}

		public VehicleDataEventStatus? getMiddleRow3BuckleBelted()
		{
			return middleRow3BuckleBelted;		}

		public VehicleDataEventStatus? getLeftRow3BuckleBelted()
		{
			return leftRow3BuckleBelted;		}

		public VehicleDataEventStatus? getRightRow3BuckleBelted()
		{
			return rightRow3BuckleBelted;		}

		public VehicleDataEventStatus? getLeftRearInflatableBelted()
		{
			return leftRearInflatableBelted;		}

		public VehicleDataEventStatus? getRightRearInflatableBelted()
		{
			return rightRearInflatableBelted;		}

		public VehicleDataEventStatus? getMiddleRow1BeltDeployed()
		{
			return middleRow1BeltDeployed;		}

		public VehicleDataEventStatus? getMiddleRow1BuckleBelted()
		{
			return middleRow1BuckleBelted;		}

	}
}