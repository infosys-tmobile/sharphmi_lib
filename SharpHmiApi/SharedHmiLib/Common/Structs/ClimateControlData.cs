﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
    public class ClimateControlData : RpcStruct
    {
        public int? fanSpeed;
        public Temperature currentTemperature;
        public Temperature desiredTemperature;
        public bool? acEnable;
        public bool? circulateAirEnable;
        public bool? autoModeEnable;
        public DefrostZone? defrostZone;
        public bool? dualModeEnable;
        public bool? acMaxEnable;
        public VentilationMode? ventilationMode;
        public bool? heatedSteeringWheelEnable;
        public bool? heatedWindshieldEnable;
        public bool? heatedRearWindowEnable;
        public bool? heatedMirrorsEnable;
        public bool? climateEnable;

        public ClimateControlData()
        {
        }

        public int? getFanSpeed()
        {
            return fanSpeed;
        }

        public Temperature getCurrentTemperature()
        {
            return currentTemperature;
        }

        public Temperature getDesiredTemperature()
        {
            return desiredTemperature;
        }

        public bool? getAcEnable()
        {
            return acEnable;
        }

        public bool? getCirculateAirEnable()
        {
            return circulateAirEnable;
        }

        public bool? getAutoModeEnable()
        {
            return autoModeEnable;
        }

        public DefrostZone? getDefrostZone()
        {
            return defrostZone;
        }

        public bool? getDualModeEnable()
        {
            return dualModeEnable;
        }

        public bool? getAcMaxEnable()
        {
            return acMaxEnable;
        }

        public VentilationMode? getVentilationMode()
        {
            return ventilationMode;
        }

        public bool? getHeatedSteeringWheelEnable()
        {
            return heatedSteeringWheelEnable;
        }

        public bool? getHeatedWindshieldEnable()
        {
            return heatedWindshieldEnable;
        }

        public bool? getHeatedRearWindowEnable()
        {
            return heatedRearWindowEnable;
        }

        public bool? getHeatedMirrorsEnable()
        {
            return heatedMirrorsEnable;
        }

        public bool? getClimateEnable()
        {
            return climateEnable;
        }
    }
}
