﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class PresetBankCapabilities : RpcStruct
	{
		public Boolean? onScreenPresetsAvailable;

		public PresetBankCapabilities()
		{
		}

		public Boolean? getOnScreenPresetsAvailable()
		{
			return onScreenPresetsAvailable;
		}
	}
}
