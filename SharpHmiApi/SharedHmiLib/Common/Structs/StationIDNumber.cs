﻿using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class StationIDNumber : RpcStruct
	{
		public int? countryCode;
        public int? fccFacilityId;

		public StationIDNumber()
		{
		}

		public int? getCountryCode()
		{
			return countryCode;
		}

		public int? getFccFacilityId()
		{
			return fccFacilityId;
		}
	}
}
