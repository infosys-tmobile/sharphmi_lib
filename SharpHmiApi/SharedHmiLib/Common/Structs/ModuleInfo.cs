﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class ModuleInfo : RpcStruct
	{
		public string moduleId;
		public Grid location;
		public Grid serviceArea;
		public bool? allowMultipleAccess;

		public ModuleInfo()
		{
		}

		public string getModuleId()
		{
			return moduleId;
		}

		public Grid getLocation()
		{
			return location;
		}

		public Grid getserviceArea()
		{
			return serviceArea;
		}
		public bool? getallowMultipleAccess()
		{
			return allowMultipleAccess;
		}
	}
}
