﻿using System.Collections.Generic;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class LightControlData : RpcStruct
	{
        public List<LightState> lightState;

		public LightControlData()
		{
		}

		public List<LightState> getLightState()
		{
			return lightState;
		}
	}
}
