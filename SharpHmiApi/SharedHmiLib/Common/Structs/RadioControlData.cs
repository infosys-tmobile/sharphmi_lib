﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class RadioControlData : RpcStruct
	{
		public int?    frequencyInteger;
		public int?   frequencyFraction;
        public RadioBand?   band;
        public RdsData rdsData;

        [ObsoleteAttribute]
        public int? availableHDs;

		public int? hdChannel;
        public int? signalStrength;
        public int? signalChangeThreshold;
        public bool? radioEnable;
        public RadioState? state;
        public SisData sisData;
        public bool? hdRadioEnable;
        public List<int> availableHdChannels;

        public RadioControlData()
		{
		}

		public int? getFrequencyInteger()
		{
			return frequencyInteger;
		}

		public int? getFrequencyFraction()
		{
			return frequencyFraction;
		}

		public RadioBand? getBand()
		{
			return band;
		}

		public RdsData getRdsData()
		{
			return rdsData;
		}

        [ObsoleteAttribute]
		public int? getAvailableHDs()
		{
			return availableHDs;
		}

		
		public int? getHdChannel()
		{
			return hdChannel;
		}

		public int? getSignalStrength()
		{
			return signalStrength;
		}

        public int? getSignalChangeThreshold()
        {
            return signalChangeThreshold;
        }

		public bool? getRadioEnable()
		{
			return radioEnable;
		}
		
        public RadioState? getState()
		{
			return state;
		}

		public SisData getSisData()
		{
			return sisData;
		}

        public bool? getHdRadioEnable()
        {
            return hdRadioEnable;
        }

        public List<int> getAvailableHdChannels()
        {
            return availableHdChannels;
        }
    }
}
