﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
    public class FuelRange : RpcStruct
    {
        public FuelType? type;
        public float? range;

        public FuelRange()
        {
        }

        public FuelType? getType()
        {
            return type;
        }

        public float? getRange()
        {
            return range;
        }
    }
}
