﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class Turn : RpcStruct
	{
		public TextFieldStruct navigationText;
		public Image turnIcon;

		public Turn()
		{
		}

		public TextFieldStruct getNavigationText()
		{
			return navigationText;
		}

		public Image getTurnIcon()
		{
			return turnIcon;		}

	}
}