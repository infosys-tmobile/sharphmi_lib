﻿using System.Collections.Generic;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
    public class VideoStreamingCapability : RpcStruct
	{
        public ImageResolution preferredResolution;
        public int? maxBitrate;
        public List<VideoStreamingFormat> supportedFormats;
        public bool? hapticSpatialDataSupported;

        public VideoStreamingCapability()
		{
		}

        public ImageResolution getImageResolution()
		{
            return preferredResolution;
		}

        public int? getMaxBitrate()
		{
            return maxBitrate;
		}

        public List<VideoStreamingFormat> getSupportedFormats()
        {
            return supportedFormats;
        }

        public bool? getHapticSpatialDataSupported()
		{
            return hapticSpatialDataSupported;
		}
		
	}
}
