﻿using System.Collections.Generic;
using HmiApiLib.Base;
namespace HmiApiLib.Common.Structs
{
	public class SeatLocationCapability : RpcStruct
	{
		public int? columns;
		public int? rows;
		public int? levels;
		public List<SeatLocation> seats;
		public SeatLocationCapability()
		{
		}

		public int? getcolumns()
		{
			return columns;
		}
		public int? getrows()
		{
			return rows;
		}
		public int? getlevels()
		{
			return levels;
		}
		public List<SeatLocation> getSeats()
		{
			return seats;
		}
	}
}
