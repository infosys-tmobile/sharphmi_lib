﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class RadioControlCapabilities : RpcStruct
	{
		public string moduleName;
		public bool? radioEnableAvailable;
        public bool? radioBandAvailable;
        public bool? radioFrequencyAvailable;
        public bool? hdChannelAvailable;
        public bool? rdsDataAvailable;
		public ModuleInfo moduleInfo;
		[ObsoleteAttribute]
        public bool? availableHDsAvailable;

		public bool? stateAvailable;
        public bool? signalStrengthAvailable;
        public bool? signalChangeThresholdAvailable;
        public bool? sisDataAvailable;
        public bool? hdRadioEnableAvailable;
        public bool? siriusxmRadioAvailable;
        public bool? availableHdChannelsAvailable;

        public RadioControlCapabilities()
		{
		}
		public ModuleInfo getModuleInfo()
		{
			return moduleInfo;
		}
		public string getModuleName()
		{
			return moduleName;
		}
		

		public bool? getRadioEnableAvailable()
		{
			return radioEnableAvailable;		}

		public bool? getRadioBandAvailable()
		{
			return radioBandAvailable;
		}

		public bool? getRadioFrequencyAvailable()
		{
			return radioFrequencyAvailable;
		}

		public bool? getHdChannelAvailable()
		{
			return hdChannelAvailable;
		}

		public bool? getRdsDataAvailable()
		{
			return rdsDataAvailable;
		}

        [ObsoleteAttribute]
        public bool? getAvailableHDsAvailable()
		{
			return availableHDsAvailable;
		}

		public bool? getStateAvailable()
		{
			return stateAvailable;
		}

		public bool? getSignalStrengthAvailable()
		{
			return signalStrengthAvailable;
		}

		public bool? getSignalChangeThresholdAvailable()
		{
			return signalChangeThresholdAvailable;
		}

		public bool? getSisDataAvailable()
		{
			return sisDataAvailable;
		}

        public bool? getHdRadioEnableAvailable()
        {
            return hdRadioEnableAvailable;
        }

        public bool? getSiriusxmRadioAvailable()
        {
            return siriusxmRadioAvailable;
        }

        public bool? getAvailableHdChannelsAvailable()
        {
            return availableHdChannelsAvailable;
        }
    }
}
