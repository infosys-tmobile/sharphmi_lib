﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
    public class PhoneCapability : RpcStruct
	{
        public Boolean? dialNumberEnabled;

        public PhoneCapability()
		{
		}

        public Boolean? getDialNumberEnabled()
		{
            return dialNumberEnabled;
		}

	}
}