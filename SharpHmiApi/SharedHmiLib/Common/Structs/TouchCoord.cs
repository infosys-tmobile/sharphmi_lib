﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class TouchCoord : RpcStruct
	{
		public int? x;
		public int? y;

		public TouchCoord()
		{
		}

		public int? getX()
		{
			return x;
		}

		public int? getY()
		{
			return y;		}

	}
}
