﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class DateTime : RpcStruct
	{
		public int? millisecond;
		public int? second;
		public int? minute;
		public int? hour;
		public int? day;
		public int? month;
		public int? year;
		public int? tz_hour;
		public int? tz_minute;

		public DateTime()
		{
		}

		public int? getMillisecond()
		{
			return millisecond;
		}

		public int? getSecond()
		{
			return second;		}

		public int? getMinute()
		{
			return minute;		}

		public int? getHour()
		{
			return hour;		}

		public int? getDay()
		{
			return day;		}

		public int? getMonth()
		{
			return month;		}

		public int? getYear()
		{
			return year;		}

		public int? getTz_hour()
		{
			return tz_hour;		}

		public int? getTz_minute()
		{
			return tz_minute;		}

	}
}