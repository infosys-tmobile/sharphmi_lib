﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Common.Structs
{
	public class ScreenParams : RpcStruct
	{
		public ImageResolution resolution;
		public TouchEventCapabilities touchEventAvailable;

		public ScreenParams()
		{
		}

		public ImageResolution getResolution()
		{
			return resolution;
		}

		public TouchEventCapabilities getTouchEventAvailable()
		{
			return touchEventAvailable;		}

	}
}