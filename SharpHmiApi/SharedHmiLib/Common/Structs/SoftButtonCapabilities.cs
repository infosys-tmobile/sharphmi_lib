﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib.Common.Structs
{
	public class SoftButtonCapabilities : RpcStruct
	{
		public Boolean? shortPressAvailable;
		public Boolean? longPressAvailable;
		public Boolean? upDownAvailable;
		public Boolean? imageSupported;
		public Boolean? textSupported;

		public SoftButtonCapabilities()
		{
		}

		public Boolean? getShortPressAvailable()
		{
			return shortPressAvailable;
		}

		public Boolean? getLongPressAvailable()
		{
			return longPressAvailable;		}

		public Boolean? getUpDownAvailable()
		{
			return upDownAvailable;		}

		public Boolean? getImageSupported()
		{
			return imageSupported;		}
		public Boolean? getTextSupported()
		{
			return textSupported;					}

	}
}