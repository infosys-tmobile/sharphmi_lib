﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using HmiApiLib.Interfaces;

namespace HmiApiLib
{
	public sealed class ConsoleLogDispatchManager<T>
	{
		private static volatile ConsoleLogDispatchManager<T> instance;
		private static object syncRoot = new Object();
		BlockingCollection<T> _queue = null;
		private Thread _consoleLogDispatchingThread = null;
		IDispatchingHelper<T> _dispatchingHelper = null;
		private String THREAD_NAME = "CONSOLE_LOG_MESSAGE";

		// Boolean to track if disposed
		private Boolean dispatcherDisposed = false;

		public static ConsoleLogDispatchManager<T> Instance
		{
			get
			{
				if (instance == null)
				{
					lock (syncRoot)
					{
						if (instance == null)
							instance = new ConsoleLogDispatchManager<T>();
					}
				}

				return instance;
			}
		}

		private ConsoleLogDispatchManager()
		{
		}

		public void initConsoleLogDispatchManager(IDispatchingHelper<T> dispatchingHelper)
		{
			_queue = new BlockingCollection<T>();

			_dispatchingHelper = dispatchingHelper;

			try
			{
				// Create dispatching thread
				_consoleLogDispatchingThread = new Thread(new ThreadStart(handleMessages));
				_consoleLogDispatchingThread.Name = THREAD_NAME;
				_consoleLogDispatchingThread.Start();
			}
			catch (ThreadStateException ex)
			{
				_dispatchingHelper.handleDispatchingError("Error occurred during starting of dispatching thread.", ex);
				_consoleLogDispatchingThread = null;
			}
		}

		public IDispatchingHelper<T> getDispatchingHelper()
		{
			return this._dispatchingHelper;
		}

		public void dispose()
		{
			dispatcherDisposed = true;

			if (_consoleLogDispatchingThread != null)
			{
				_consoleLogDispatchingThread.Interrupt();
				_consoleLogDispatchingThread = null;
			}
		}

		private void handleMessages()
		{

			try
			{
				T thisMessage;

				while (dispatcherDisposed == false)
				{
					thisMessage = _queue.Take();
					_dispatchingHelper.dispatch(thisMessage);
				}
			}
			catch (ThreadInterruptedException)
			{
				// Thread was interrupted by dispose() method, no action required
				return;
			}
			catch (Exception ex)
			{
				_dispatchingHelper.handleDispatchingError("Error occurred during dispatching log message.", ex);
			}
		}

		public void queueMessage(T message)
		{
			try
			{
				_queue.Add(message);
			}
			catch (Exception ex)
			{
				_dispatchingHelper.handleQueueingError("Exception encountered when queueing log message.", ex);
			}
		}
	}
}
