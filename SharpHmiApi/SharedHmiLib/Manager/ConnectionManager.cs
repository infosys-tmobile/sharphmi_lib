﻿using System;
using HmiApiLib.Types;
using WebSocket4Net;
using HmiApiLib.Builder;
using HmiApiLib.Interfaces;
using HmiApiLib.Handshaking;
using HmiApiLib.Base;
using System.Runtime.CompilerServices;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HmiApiLib.Manager
{
	public sealed class ConnectionManager
	{
		private static volatile ConnectionManager instance;
		private static object syncRoot = new Object();
		public static Boolean bRecycled = false;
		public IMessageListener messageListener;
		public IConnectionListener connectionListener;
        public InitialConnectionCommandConfig initialConnectionCommandConfig = null;
		public ConsoleLogDispatchManager<LogMessage> connectionLogDispatchManager;

		WebSocket ws;
		String ipAddress = "127.0.0.1";   // default ipAddress
		int portNumber = 8087;            // default portNumber

		public static ConnectionManager Instance
		{
			get
			{
				if (instance == null)
				{
					lock (syncRoot)
					{
						if (instance == null)
							instance = new ConnectionManager();
					}
				}
				else
				{
					bRecycled = true;
				}

				return instance;
			}
		}

		private ConnectionManager()
		{
		}

		public void setMessageListener(IMessageListener listener)
		{
			messageListener = listener;
		}

		public void setConnectionListener(IConnectionListener connListener)
		{
			connectionListener = connListener;
		}

		public void setConsoleLogListener(IDispatchingHelper<LogMessage> dispatchingHelper)
		{
			connectionLogDispatchManager = ConsoleLogDispatchManager<LogMessage>.Instance;
			connectionLogDispatchManager.initConsoleLogDispatchManager(dispatchingHelper);
		}

		// Queue incoming Console Log Message
		[MethodImpl(MethodImplOptions.Synchronized)]
		public void queueConsoleMessage(LogMessage message)
		{
			if (connectionLogDispatchManager != null)
			{
				connectionLogDispatchManager.queueMessage(message);
			}
		}

		public void setIpAddress(String ipAddress){
			this.ipAddress = ipAddress;
		
		}

		public String getIpAddress(){
			return this.ipAddress;
		}

		public void setPortNumber(int portNumber)
		{
			this.portNumber = portNumber;

		}

		public int getPortNumber()
		{
			return this.portNumber;
		}

        public void setInitialConnectionCommandConfig(InitialConnectionCommandConfig initialConnectionCommandConfig)
		{
			this.initialConnectionCommandConfig = initialConnectionCommandConfig;

		}

		public InitialConnectionCommandConfig getInitialConnectionCommandConfig()
		{
			return this.initialConnectionCommandConfig;
		}

		private void handleIncomingMessage(string json)
		{

            if (json == null) return;

			string method = null;
            StringLogMessage myStringLog = null;

            method = Utils.getMethodName(json);

            if (method == null) {
				myStringLog = new StringLogMessage(json);
				handleInputConsoleLog(myStringLog);
                return;
            }

			if (messageListener == null) return;

            try
            {
                if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnAppRegistered))
                {
                    Controllers.BasicCommunication.IncomingNotifications.OnAppRegistered onAppReg = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingNotifications.OnAppRegistered>(json);
                    handleInputConsoleLog(new RpcLogMessage(onAppReg));
                    messageListener.onBcAppRegisteredNotification(onAppReg);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnAppUnregistered))
                {
                    Controllers.BasicCommunication.IncomingNotifications.OnAppUnregistered onAppUnReg = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingNotifications.OnAppUnregistered>(json);
                    handleInputConsoleLog(new RpcLogMessage(onAppUnReg));
                    messageListener.onBcAppUnRegisteredNotification(onAppUnReg);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnResumeAudioSource))
                {
                    Controllers.BasicCommunication.IncomingNotifications.OnResumeAudioSource onResumeAudioSource = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingNotifications.OnResumeAudioSource>(json);
                    handleInputConsoleLog(new RpcLogMessage(onResumeAudioSource));
                    messageListener.onBcOnResumeAudioSourceNotification(onResumeAudioSource);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnSDLPersistenceComplete))
                {
                    Controllers.BasicCommunication.IncomingNotifications.OnSDLPersistenceComplete onSDLPersistenceComplete = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingNotifications.OnSDLPersistenceComplete>(json);
                    handleInputConsoleLog(new RpcLogMessage(onSDLPersistenceComplete));
                    messageListener.onBcOnSDLPersistenceCompleteNotification(onSDLPersistenceComplete);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnFileRemoved))
                {
                    Controllers.BasicCommunication.IncomingNotifications.OnFileRemoved onFileRemoved = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingNotifications.OnFileRemoved>(json);
                    handleInputConsoleLog(new RpcLogMessage(onFileRemoved));
                    messageListener.onBcOnFileRemovedNotification(onFileRemoved);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnSDLClose))
                {
                    Controllers.BasicCommunication.IncomingNotifications.OnSDLClose onSDLClose = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingNotifications.OnSDLClose>(json);
                    handleInputConsoleLog(new RpcLogMessage(onSDLClose));
                    messageListener.onBcOnSDLCloseNotification(onSDLClose);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnPutFile))
                {
                    Controllers.BasicCommunication.IncomingNotifications.OnPutFile onPutFile = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingNotifications.OnPutFile>(json);
                    handleInputConsoleLog(new RpcLogMessage(onPutFile));
                    messageListener.onBcPutfileNotification(onPutFile);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.MixingAudioSupported))
                {
                    Controllers.BasicCommunication.IncomingRequests.MixingAudioSupported mix = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingRequests.MixingAudioSupported>(json);
                    handleInputConsoleLog(new RpcLogMessage(mix));
                    messageListener.onBcMixingAudioSupportedRequest(mix);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.ActivateApp))
                {
                    Controllers.BasicCommunication.IncomingRequests.ActivateApp activateApp = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingRequests.ActivateApp>(json);
                    handleInputConsoleLog(new RpcLogMessage(activateApp));
                    if (initialConnectionCommandConfig != null)
                    {
                        messageListener.onBcActivateAppRequest(activateApp, initialConnectionCommandConfig.getActivateAppResultCode());
                    }
                    else
                    {
                        messageListener.onBcActivateAppRequest(activateApp, Common.Enums.Result.SUCCESS);
                    }
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.AllowDeviceToConnect))
                {
                    Controllers.BasicCommunication.IncomingRequests.AllowDeviceToConnect allowDeviceToConnect = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingRequests.AllowDeviceToConnect>(json);
                    handleInputConsoleLog(new RpcLogMessage(allowDeviceToConnect));
                    messageListener.onBcAllowDeviceToConnectRequest(allowDeviceToConnect);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.DialNumber))
                {
                    Controllers.BasicCommunication.IncomingRequests.DialNumber dialNumber = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingRequests.DialNumber>(json);
                    handleInputConsoleLog(new RpcLogMessage(dialNumber));
                    messageListener.onBcDialNumberRequest(dialNumber);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.GetSystemInfo))
                {
                    Controllers.BasicCommunication.IncomingRequests.GetSystemInfo getSystemInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingRequests.GetSystemInfo>(json);
                    handleInputConsoleLog(new RpcLogMessage(getSystemInfo));
                    messageListener.onBcGetSystemInfoRequest(getSystemInfo);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.GetSystemTime))
                {
                    Controllers.BasicCommunication.IncomingRequests.GetSystemTime getSystemTime = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingRequests.GetSystemTime>(json);
                    handleInputConsoleLog(new RpcLogMessage(getSystemTime));
                    messageListener.onBcGetSystemTimeRequest(getSystemTime);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.PolicyUpdate))
                {
                    Controllers.BasicCommunication.IncomingRequests.PolicyUpdate policyUpdate = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingRequests.PolicyUpdate>(json);
                    handleInputConsoleLog(new RpcLogMessage(policyUpdate));
                    messageListener.onBcPolicyUpdateRequest(policyUpdate);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.SystemRequest))
                {
                    Controllers.BasicCommunication.IncomingRequests.SystemRequest systemRequest = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingRequests.SystemRequest>(json);
                    handleInputConsoleLog(new RpcLogMessage(systemRequest));
                    messageListener.onBcSystemRequestRequest(systemRequest);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.UpdateAppList))
                {
                    Controllers.BasicCommunication.IncomingRequests.UpdateAppList updateAppList = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingRequests.UpdateAppList>(json);
                    handleInputConsoleLog(new RpcLogMessage(updateAppList));
                    messageListener.onBcUpdateAppListRequest(updateAppList);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.UpdateDeviceList))
                {
                    Controllers.BasicCommunication.IncomingRequests.UpdateDeviceList updateDeviceList = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingRequests.UpdateDeviceList>(json);
                    handleInputConsoleLog(new RpcLogMessage(updateDeviceList));
                    messageListener.onBcUpdateDeviceListRequest(updateDeviceList);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.DecryptCertificate))
                {
                    Controllers.BasicCommunication.IncomingRequests.DecryptCertificate decryptCertificate = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.BasicCommunication.IncomingRequests.DecryptCertificate>(json);
                    handleInputConsoleLog(new RpcLogMessage(decryptCertificate));
                    messageListener.onBcDecryptCertificateRequest(decryptCertificate);
                }
                else if (method.Equals(InterfaceType.Buttons + "." + FunctionType.GetCapabilities))
                {
                    Controllers.Buttons.IncomingRequests.GetCapabilities caps = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Buttons.IncomingRequests.GetCapabilities>(json);
                    handleInputConsoleLog(new RpcLogMessage(caps));

                    if (initialConnectionCommandConfig != null)
                    {
                        messageListener.onButtonsGetCapabilitiesRequest(caps, initialConnectionCommandConfig.getButtonsCapabilitiesResponseParam());
                    }
                    else
                    {
                        messageListener.onButtonsGetCapabilitiesRequest(caps, null);
                    }
                }
                else if (method.Equals(InterfaceType.Buttons + "." + FunctionType.ButtonPress))
                {
                    Controllers.Buttons.IncomingRequests.ButtonPress buttonPress = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Buttons.IncomingRequests.ButtonPress>(json);
                    handleInputConsoleLog(new RpcLogMessage(buttonPress));
                    messageListener.onButtonsButtonPressRequest(buttonPress);
                }
                else if (method.Equals(InterfaceType.Buttons + "." + FunctionType.OnButtonSubscription))
                {
                    Controllers.Buttons.IncomingNotifications.OnButtonSubscription onButtonSubs = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Buttons.IncomingNotifications.OnButtonSubscription>(json);

                    handleInputConsoleLog(new RpcLogMessage(onButtonSubs));
                    messageListener.OnButtonSubscriptionNotification(onButtonSubs);
                }


                else if (method.Equals(InterfaceType.UI + "." + FunctionType.OnRecordStart))
                {
                    Controllers.UI.IncomingNotifications.OnRecordStart recordStart = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingNotifications.OnRecordStart>(json);
                    handleInputConsoleLog(new RpcLogMessage(recordStart));
                    messageListener.onUiRecordStartNotification(recordStart);
                }


                else if (method.Equals(InterfaceType.UI + "." + FunctionType.IsReady))
                {
                    Controllers.UI.IncomingRequests.IsReady ready = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.IsReady>(json);
                    handleInputConsoleLog(new RpcLogMessage(ready));
                    messageListener.onUiIsReadyRequest(ready);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.SetAppIcon))
                {
                    Controllers.UI.IncomingRequests.SetAppIcon appIcon = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.SetAppIcon>(json);
                    handleInputConsoleLog(new RpcLogMessage(appIcon));
                    messageListener.onUiSetAppIconRequest(appIcon);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.Show))
                {
                    Controllers.UI.IncomingRequests.Show show = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.Show>(json);
                    handleInputConsoleLog(new RpcLogMessage(show));
                    messageListener.onUiShowRequest(show);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.AddCommand))
                {
                    Controllers.UI.IncomingRequests.AddCommand addCommand = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.AddCommand>(json);
                    handleInputConsoleLog(new RpcLogMessage(addCommand));
                    messageListener.onUiAddCommandRequest(addCommand);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.AddSubMenu))
                {
                    Controllers.UI.IncomingRequests.AddSubMenu addSubMenu = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.AddSubMenu>(json);
                    handleInputConsoleLog(new RpcLogMessage(addSubMenu));
                    messageListener.onUiAddSubMenuRequest(addSubMenu);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.ShowAppMenu))
                {
                    Controllers.UI.IncomingRequests.ShowAppMenu showAppMenu = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.ShowAppMenu>(json);
                    handleInputConsoleLog(new RpcLogMessage(showAppMenu));
                    messageListener.onUiShowAppMenuRequest(showAppMenu);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.CloseApplication))
                {
                    Controllers.UI.IncomingRequests.CloseApplication closeApplication = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.CloseApplication>(json);
                    handleInputConsoleLog(new RpcLogMessage(closeApplication));
                    messageListener.onUiCloseApplicationRequest(closeApplication);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.Alert))
                {
                    Controllers.UI.IncomingRequests.Alert alert = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.Alert>(json);
                    handleInputConsoleLog(new RpcLogMessage(alert));
                    messageListener.onUiAlertRequest(alert);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.ChangeRegistration))
                {
                    Controllers.UI.IncomingRequests.ChangeRegistration changeRegistration = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.ChangeRegistration>(json);
                    handleInputConsoleLog(new RpcLogMessage(changeRegistration));
                    messageListener.onUiChangeRegistrationRequest(changeRegistration);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.ClosePopUp))
                {
                    Controllers.UI.IncomingRequests.ClosePopUp closePopUp = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.ClosePopUp>(json);
                    handleInputConsoleLog(new RpcLogMessage(closePopUp));
                    messageListener.onUiClosePopUpRequest(closePopUp);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.DeleteSubMenu))
                {
                    Controllers.UI.IncomingRequests.DeleteSubMenu deleteSubMenu = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.DeleteSubMenu>(json);
                    handleInputConsoleLog(new RpcLogMessage(deleteSubMenu));
                    messageListener.onUiDeleteSubMenuRequest(deleteSubMenu);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.EndAudioPassThru))
                {
                    Controllers.UI.IncomingRequests.EndAudioPassThru endAudioPassThru = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.EndAudioPassThru>(json);
                    handleInputConsoleLog(new RpcLogMessage(endAudioPassThru));
                    messageListener.onUiEndAudioPassThruRequest(endAudioPassThru);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.GetCapabilities))
                {
                    Controllers.UI.IncomingRequests.GetCapabilities getCapabilities = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.GetCapabilities>(json);
                    handleInputConsoleLog(new RpcLogMessage(getCapabilities));
                    messageListener.onUiGetCapabilitiesRequest(getCapabilities);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.GetSupportedLanguages))
                {
                    Controllers.UI.IncomingRequests.GetSupportedLanguages getSupportedLanguage = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.GetSupportedLanguages>(json);
                    handleInputConsoleLog(new RpcLogMessage(getSupportedLanguage));
                    messageListener.onUiGetSupportedLanguagesRequest(getSupportedLanguage);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.PerformAudioPassThru))
                {
                    Controllers.UI.IncomingRequests.PerformAudioPassThru performAudioPassThru = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.PerformAudioPassThru>(json);
                    handleInputConsoleLog(new RpcLogMessage(performAudioPassThru));
                    messageListener.onUiPerformAudioPassThruRequest(performAudioPassThru);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.PerformInteraction))
                {
                    Controllers.UI.IncomingRequests.PerformInteraction performInteraction = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.PerformInteraction>(json);
                    handleInputConsoleLog(new RpcLogMessage(performInteraction));
                    messageListener.onUiPerformInteractionRequest(performInteraction);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.ScrollableMessage))
                {
                    Controllers.UI.IncomingRequests.ScrollableMessage scrollableMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.ScrollableMessage>(json);
                    handleInputConsoleLog(new RpcLogMessage(scrollableMessage));
                    messageListener.onUiScrollableMessageRequest(scrollableMessage);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.SetDisplayLayout))
                {
                    Controllers.UI.IncomingRequests.SetDisplayLayout setDisplayLayout = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.SetDisplayLayout>(json);
                    handleInputConsoleLog(new RpcLogMessage(setDisplayLayout));
                    messageListener.onUiSetDisplayLayoutRequest(setDisplayLayout);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.SetGlobalProperties))
                {
                    Controllers.UI.IncomingRequests.SetGlobalProperties setGlobalProperties = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.SetGlobalProperties>(json);
                    handleInputConsoleLog(new RpcLogMessage(setGlobalProperties));
                    messageListener.onUiSetGlobalPropertiesRequest(setGlobalProperties);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.SetMediaClockTimer))
                {
                    Controllers.UI.IncomingRequests.SetMediaClockTimer setMediaClockTimer = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.SetMediaClockTimer>(json);
                    handleInputConsoleLog(new RpcLogMessage(setMediaClockTimer));
                    messageListener.onUiSetMediaClockTimerRequest(setMediaClockTimer);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.ShowCustomForm))
                {
                    Controllers.UI.IncomingRequests.ShowCustomForm showCustomForm = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.ShowCustomForm>(json);
                    handleInputConsoleLog(new RpcLogMessage(showCustomForm));
                    messageListener.onUiShowCustomFormRequest(showCustomForm);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.Slider))
                {
                    Controllers.UI.IncomingRequests.Slider slider = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.Slider>(json);
                    handleInputConsoleLog(new RpcLogMessage(slider));
                    messageListener.onUiSliderRequest(slider);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.GetLanguage))
                {
                    Controllers.UI.IncomingRequests.GetLanguage lang = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.GetLanguage>(json);
                    handleInputConsoleLog(new RpcLogMessage(lang));
                    messageListener.onUiGetLanguageRequest(lang);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.DeleteCommand))
                {
                    Controllers.UI.IncomingRequests.DeleteCommand deleteCommand = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.DeleteCommand>(json);
                    handleInputConsoleLog(new RpcLogMessage(deleteCommand));
                    messageListener.onUiDeleteCommandRequest(deleteCommand);
                }
				else if (method.Equals(InterfaceType.UI + "." + FunctionType.CreateWindow))
				{
					Controllers.UI.IncomingRequests.CreateWindow createWindow = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.CreateWindow>(json);
					handleInputConsoleLog(new RpcLogMessage(createWindow));
					messageListener.onUiCreateWindowRequest(createWindow);
				}
				else if (method.Equals(InterfaceType.UI + "." + FunctionType.DeleteWindow))
				{
					Controllers.UI.IncomingRequests.DeleteWindow deleteWindow = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.UI.IncomingRequests.DeleteWindow>(json);
					handleInputConsoleLog(new RpcLogMessage(deleteWindow));
					messageListener.onUiDeleteWindowRequest(deleteWindow);
				}
				else if (method.Equals(InterfaceType.VR + "." + FunctionType.IsReady))
                {
                    Controllers.VR.IncomingRequests.IsReady ready = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.VR.IncomingRequests.IsReady>(json);
                    handleInputConsoleLog(new RpcLogMessage(ready));
                    messageListener.onVrIsReadyRequest(ready);
                }
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.AddCommand))
                {
                    Controllers.VR.IncomingRequests.AddCommand addCommand = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.VR.IncomingRequests.AddCommand>(json);
                    handleInputConsoleLog(new RpcLogMessage(addCommand));
                    messageListener.onVrAddCommandRequest(addCommand);
                }
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.ChangeRegistration))
                {
                    Controllers.VR.IncomingRequests.ChangeRegistration changeRegistration = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.VR.IncomingRequests.ChangeRegistration>(json);
                    handleInputConsoleLog(new RpcLogMessage(changeRegistration));
                    messageListener.onVrChangeRegistrationRequest(changeRegistration);
                }
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.GetCapabilities))
                {
                    Controllers.VR.IncomingRequests.GetCapabilities getCapabilities = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.VR.IncomingRequests.GetCapabilities>(json);
                    handleInputConsoleLog(new RpcLogMessage(getCapabilities));
                    messageListener.onVrGetCapabilitiesRequest(getCapabilities);
                }
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.GetSupportedLanguages))
                {
                    Controllers.VR.IncomingRequests.GetSupportedLanguages getSupportedLanguages = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.VR.IncomingRequests.GetSupportedLanguages>(json);
                    handleInputConsoleLog(new RpcLogMessage(getSupportedLanguages));
                    messageListener.onVrGetSupportedLanguagesRequest(getSupportedLanguages);
                }
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.PerformInteraction))
                {
                    Controllers.VR.IncomingRequests.PerformInteraction performInteraction = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.VR.IncomingRequests.PerformInteraction>(json);
                    handleInputConsoleLog(new RpcLogMessage(performInteraction));
                    messageListener.onVrPerformInteractionRequest(performInteraction);
                }
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.GetLanguage))
                {
                    Controllers.VR.IncomingRequests.GetLanguage lang = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.VR.IncomingRequests.GetLanguage>(json);
                    handleInputConsoleLog(new RpcLogMessage(lang));
                    messageListener.onVrGetLanguageRequest(lang);
                }
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.DeleteCommand))
                {
                    Controllers.VR.IncomingRequests.DeleteCommand deleteCommand = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.VR.IncomingRequests.DeleteCommand>(json);
                    handleInputConsoleLog(new RpcLogMessage(deleteCommand));
                    messageListener.onVrDeleteCommandRequest(deleteCommand);
                }
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.IsReady))
                {
                    Controllers.TTS.IncomingRequests.IsReady ready = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.TTS.IncomingRequests.IsReady>(json);
                    handleInputConsoleLog(new RpcLogMessage(ready));
                    messageListener.onTtsIsReadyRequest(ready);
                }
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.Speak))
                {
                    Controllers.TTS.IncomingRequests.Speak speak = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.TTS.IncomingRequests.Speak>(json);
                    handleInputConsoleLog(new RpcLogMessage(speak));
                    messageListener.onTtsSpeakRequest(speak);
                }
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.StopSpeaking))
                {
                    Controllers.TTS.IncomingRequests.StopSpeaking stopSpeak = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.TTS.IncomingRequests.StopSpeaking>(json);
                    handleInputConsoleLog(new RpcLogMessage(stopSpeak));
                    messageListener.onTtsStopSpeakingRequest(stopSpeak);
                }
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.GetLanguage))
                {
                    Controllers.TTS.IncomingRequests.GetLanguage lang = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.TTS.IncomingRequests.GetLanguage>(json);
                    handleInputConsoleLog(new RpcLogMessage(lang));
                    messageListener.onTtsGetLanguageRequest(lang);
                }
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.ChangeRegistration))
                {
                    Controllers.TTS.IncomingRequests.ChangeRegistration changeRegistration = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.TTS.IncomingRequests.ChangeRegistration>(json);
                    handleInputConsoleLog(new RpcLogMessage(changeRegistration));
                    messageListener.onTtsChangeRegistrationRequest(changeRegistration);
                }
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.GetCapabilities))
                {
                    Controllers.TTS.IncomingRequests.GetCapabilities getCapabilities = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.TTS.IncomingRequests.GetCapabilities>(json);
                    handleInputConsoleLog(new RpcLogMessage(getCapabilities));
                    messageListener.onTtsGetCapabilitiesRequest(getCapabilities);
                }
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.GetSupportedLanguages))
                {
                    Controllers.TTS.IncomingRequests.GetSupportedLanguages getSupportedLanguages = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.TTS.IncomingRequests.GetSupportedLanguages>(json);
                    handleInputConsoleLog(new RpcLogMessage(getSupportedLanguages));
                    messageListener.onTtsGetSupportedLanguagesRequest(getSupportedLanguages);
                }
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.SetGlobalProperties))
                {
                    Controllers.TTS.IncomingRequests.SetGlobalProperties setGlobalProperties = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.TTS.IncomingRequests.SetGlobalProperties>(json);
                    handleInputConsoleLog(new RpcLogMessage(setGlobalProperties));
                    messageListener.onTtsSetGlobalPropertiesRequest(setGlobalProperties);
                }
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.AlertManeuver))
                {
                    Controllers.Navigation.IncomingRequests.AlertManeuver alertManeuver = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Navigation.IncomingRequests.AlertManeuver>(json);
                    handleInputConsoleLog(new RpcLogMessage(alertManeuver));
                    messageListener.onNavAlertManeuverRequest(alertManeuver);
                }
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.SetVideoConfig))
                {
                    Controllers.Navigation.IncomingRequests.SetVideoConfig setVideoConfig = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Navigation.IncomingRequests.SetVideoConfig>(json);
                    handleInputConsoleLog(new RpcLogMessage(setVideoConfig));
                    messageListener.onNavSetVideoConfigRequest(setVideoConfig);
                }
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.OnAudioDataStreaming))
                {
                    Controllers.Navigation.IncomingNotifications.OnAudioDataStreaming onAudioDataStreaming = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Navigation.IncomingNotifications.OnAudioDataStreaming>(json);
                    handleInputConsoleLog(new RpcLogMessage(onAudioDataStreaming));
                    messageListener.onNavOnAudioDataStreamingNotification(onAudioDataStreaming);
                }
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.OnVideoDataStreaming))
                {
                    Controllers.Navigation.IncomingNotifications.OnVideoDataStreaming onVideoDataStreaming = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Navigation.IncomingNotifications.OnVideoDataStreaming>(json);
                    handleInputConsoleLog(new RpcLogMessage(onVideoDataStreaming));
                    messageListener.onNavOnVideoDataStreamingNotification(onVideoDataStreaming);
                }
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.OnWayPointChange))
                {
                    Controllers.Navigation.IncomingNotifications.OnWayPointChange onWayPointChange = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Navigation.IncomingNotifications.OnWayPointChange>(json);
                    handleInputConsoleLog(new RpcLogMessage(onWayPointChange));
                    messageListener.onNavOnWayPointChangeNotification(onWayPointChange);
                }
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.ActivateApp))
                {
                    Controllers.SDL.IncomingResponses.ActivateApp onActivateApp = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.SDL.IncomingResponses.ActivateApp>(json);
                    handleInputConsoleLog(new RpcLogMessage(onActivateApp));
                    messageListener.onSDLActivateAppResponse(onActivateApp);
                }
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.GetListOfPermissions))
				{
					Controllers.SDL.IncomingResponses.GetListOfPermissions onGetListOfPermissions = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.SDL.IncomingResponses.GetListOfPermissions>(json);
					handleInputConsoleLog(new RpcLogMessage(onGetListOfPermissions));
                    messageListener.onSDLGetListOfPermissionsResponse(onGetListOfPermissions);
				}
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.GetStatusUpdate))
				{
					Controllers.SDL.IncomingResponses.GetStatusUpdate onGetStatusUpdate = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.SDL.IncomingResponses.GetStatusUpdate>(json);
					handleInputConsoleLog(new RpcLogMessage(onGetStatusUpdate));
                    messageListener.onSDLGetStatusUpdateResponse(onGetStatusUpdate);
				}
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.GetURLS))
				{
					Controllers.SDL.IncomingResponses.GetURLS onGetURLS = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.SDL.IncomingResponses.GetURLS>(json);
					handleInputConsoleLog(new RpcLogMessage(onGetURLS));
                    messageListener.onSDLGetURLSResponse(onGetURLS);
				}
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.GetUserFriendlyMessage))
				{
					Controllers.SDL.IncomingResponses.GetUserFriendlyMessage onGetUserFriendlyMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.SDL.IncomingResponses.GetUserFriendlyMessage>(json);
					handleInputConsoleLog(new RpcLogMessage(onGetUserFriendlyMessage));
                    messageListener.onSDLGetUserFriendlyMessageResponse(onGetUserFriendlyMessage);
				}
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.UpdateSDL))
				{
					Controllers.SDL.IncomingResponses.UpdateSDL onUpdateSDL = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.SDL.IncomingResponses.UpdateSDL>(json);
					handleInputConsoleLog(new RpcLogMessage(onUpdateSDL));
                    messageListener.onSDLUpdateSDLResponse(onUpdateSDL);
				}
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.OnAppPermissionChanged))
                {
                    Controllers.SDL.IncomingNotifications.OnAppPermissionChanged onAppPermissionChanged = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.SDL.IncomingNotifications.OnAppPermissionChanged>(json);
                    handleInputConsoleLog(new RpcLogMessage(onAppPermissionChanged));
                    messageListener.OnSDLOnAppPermissionChangedNotification(onAppPermissionChanged);
                }
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.OnSDLConsentNeeded))
                {
                    Controllers.SDL.IncomingNotifications.OnSDLConsentNeeded onSDLConsentNeeded = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.SDL.IncomingNotifications.OnSDLConsentNeeded>(json);
                    handleInputConsoleLog(new RpcLogMessage(onSDLConsentNeeded));
                    messageListener.OnSDLOnSDLConsentNeededNotification(onSDLConsentNeeded);
                }
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.OnStatusUpdate))
                {
                    Controllers.SDL.IncomingNotifications.OnStatusUpdate onStatusUpdate = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.SDL.IncomingNotifications.OnStatusUpdate>(json);
                    handleInputConsoleLog(new RpcLogMessage(onStatusUpdate));
                    messageListener.OnSDLOnStatusUpdateNotification(onStatusUpdate);
                }
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.OnSystemError))
                {
                    Controllers.SDL.IncomingNotifications.OnSystemError onSystemError = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.SDL.IncomingNotifications.OnSystemError>(json);
                    handleInputConsoleLog(new RpcLogMessage(onSystemError));
                    messageListener.OnSDLOnSystemErrorNotification(onSystemError);
                }
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.AddStatisticsInfo))
                {
                    Controllers.SDL.IncomingNotifications.AddStatisticsInfo addStatisticsInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.SDL.IncomingNotifications.AddStatisticsInfo>(json);
                    handleInputConsoleLog(new RpcLogMessage(addStatisticsInfo));
                    messageListener.OnSDLAddStatisticsInfoNotification(addStatisticsInfo);
                }
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.OnDeviceStateChanged))
                {
                    Controllers.SDL.IncomingNotifications.OnDeviceStateChanged onDeviceStateChanged = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.SDL.IncomingNotifications.OnDeviceStateChanged>(json);
                    handleInputConsoleLog(new RpcLogMessage(onDeviceStateChanged));
                    messageListener.OnSDLOnDeviceStateChangedNotification(onDeviceStateChanged);
                }
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.GetWayPoints))
                {
                    Controllers.Navigation.IncomingRequests.GetWayPoints getWayPoints = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Navigation.IncomingRequests.GetWayPoints>(json);
                    handleInputConsoleLog(new RpcLogMessage(getWayPoints));
                    messageListener.onNavGetWayPointsRequest(getWayPoints);
                }
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.IsReady))
                {
                    Controllers.Navigation.IncomingRequests.IsReady ready = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Navigation.IncomingRequests.IsReady>(json);
                    handleInputConsoleLog(new RpcLogMessage(ready));
                    messageListener.onNavIsReadyRequest(ready);
                }
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.SendLocation))
                {
                    Controllers.Navigation.IncomingRequests.SendLocation sendLocation = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Navigation.IncomingRequests.SendLocation>(json);
                    handleInputConsoleLog(new RpcLogMessage(sendLocation));
                    messageListener.onNavSendLocationRequest(sendLocation);
                }
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.ShowConstantTBT))
                {
                    Controllers.Navigation.IncomingRequests.ShowConstantTBT showConstantTBT = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Navigation.IncomingRequests.ShowConstantTBT>(json);
                    handleInputConsoleLog(new RpcLogMessage(showConstantTBT));
                    messageListener.onNavShowConstantTBTRequest(showConstantTBT);
                }
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.StartAudioStream))
                {
                    Controllers.Navigation.IncomingRequests.StartAudioStream startAudioStream = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Navigation.IncomingRequests.StartAudioStream>(json);
                    handleInputConsoleLog(new RpcLogMessage(startAudioStream));
                    messageListener.onNavStartAudioStreamRequest(startAudioStream);
                }
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.StartStream))
                {
                    Controllers.Navigation.IncomingRequests.StartStream startStream = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Navigation.IncomingRequests.StartStream>(json);
                    handleInputConsoleLog(new RpcLogMessage(startStream));
                    messageListener.onNavStartStreamRequest(startStream);
                }
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.StopAudioStream))
                {
                    Controllers.Navigation.IncomingRequests.StopAudioStream stopAudioStream = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Navigation.IncomingRequests.StopAudioStream>(json);
                    handleInputConsoleLog(new RpcLogMessage(stopAudioStream));
                    messageListener.onNavStopAudioStreamRequest(stopAudioStream);
                }
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.StopStream))
                {
                    Controllers.Navigation.IncomingRequests.StopStream stopStream = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Navigation.IncomingRequests.StopStream>(json);
                    handleInputConsoleLog(new RpcLogMessage(stopStream));
                    messageListener.onNavStopStreamRequest(stopStream);
                }
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.SubscribeWayPoints))
                {
                    Controllers.Navigation.IncomingRequests.SubscribeWayPoints subscribeWayPoints = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Navigation.IncomingRequests.SubscribeWayPoints>(json);
                    handleInputConsoleLog(new RpcLogMessage(subscribeWayPoints));
                    messageListener.onNavSubscribeWayPointsRequest(subscribeWayPoints);
                }
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.UnsubscribeWayPoints))
                {
                    Controllers.Navigation.IncomingRequests.UnsubscribeWayPoints unsubscribeWayPoints = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Navigation.IncomingRequests.UnsubscribeWayPoints>(json);
                    handleInputConsoleLog(new RpcLogMessage(unsubscribeWayPoints));
                    messageListener.onNavUnsubscribeWayPointsRequest(unsubscribeWayPoints);
                }
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.UpdateTurnList))
                {
                    Controllers.Navigation.IncomingRequests.UpdateTurnList updateTurnList = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.Navigation.IncomingRequests.UpdateTurnList>(json);
                    handleInputConsoleLog(new RpcLogMessage(updateTurnList));
                    messageListener.onNavUpdateTurnListRequest(updateTurnList);
                }
                else if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.DiagnosticMessage))
                {
                    Controllers.VehicleInfo.IncomingRequests.DiagnosticMessage diagnosticMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.VehicleInfo.IncomingRequests.DiagnosticMessage>(json);
                    handleInputConsoleLog(new RpcLogMessage(diagnosticMessage));
                    messageListener.onVehicleInfoDiagnosticMessageRequest(diagnosticMessage);
                }
                else if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.GetDTCs))
                {
                    Controllers.VehicleInfo.IncomingRequests.GetDTCs getDTCs = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.VehicleInfo.IncomingRequests.GetDTCs>(json);
                    handleInputConsoleLog(new RpcLogMessage(getDTCs));
                    messageListener.onVehicleInfoGetDTCsRequest(getDTCs);
                }
                else if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.GetVehicleData))
                {
                    Controllers.VehicleInfo.IncomingRequests.GetVehicleData getVehicleData = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.VehicleInfo.IncomingRequests.GetVehicleData>(json);
                    handleInputConsoleLog(new RpcLogMessage(getVehicleData));
                    messageListener.onVehicleInfoGetVehicleDataRequest(getVehicleData);
                }
                else if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.GetVehicleType))
                {
                    Controllers.VehicleInfo.IncomingRequests.GetVehicleType getVehicleType = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.VehicleInfo.IncomingRequests.GetVehicleType>(json);
                    handleInputConsoleLog(new RpcLogMessage(getVehicleType));
                    messageListener.onVehicleInfoGetVehicleTypeRequest(getVehicleType);
                }
                else if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.IsReady))
                {
                    Controllers.VehicleInfo.IncomingRequests.IsReady ready = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.VehicleInfo.IncomingRequests.IsReady>(json);
                    handleInputConsoleLog(new RpcLogMessage(ready));
                    messageListener.onVehicleInfoIsReadyRequest(ready);
                }
                else if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.ReadDID))
                {
                    Controllers.VehicleInfo.IncomingRequests.ReadDID readDID = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.VehicleInfo.IncomingRequests.ReadDID>(json);
                    handleInputConsoleLog(new RpcLogMessage(readDID));
                    messageListener.onVehicleInfoReadDIDRequest(readDID);
                }
                else if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.SubscribeVehicleData))
                {
                    Controllers.VehicleInfo.IncomingRequests.SubscribeVehicleData subscribeVehicleData = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.VehicleInfo.IncomingRequests.SubscribeVehicleData>(json);
                    handleInputConsoleLog(new RpcLogMessage(subscribeVehicleData));
                    messageListener.onVehicleInfoSubscribeVehicleDataRequest(subscribeVehicleData);
                }
                else if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.UnsubscribeVehicleData))
                {
                    Controllers.VehicleInfo.IncomingRequests.UnsubscribeVehicleData unsubscribeVehicleData = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.VehicleInfo.IncomingRequests.UnsubscribeVehicleData>(json);
                    handleInputConsoleLog(new RpcLogMessage(unsubscribeVehicleData));
                    messageListener.onVehicleInfoUnsubscribeVehicleDataRequest(unsubscribeVehicleData);
                }
                else if (method.Equals(InterfaceType.RC + "." + FunctionType.GetCapabilities))
                {
                    Controllers.RC.IncomingRequests.GetCapabilities getCapabilities = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.RC.IncomingRequests.GetCapabilities>(json);
                    handleInputConsoleLog(new RpcLogMessage(getCapabilities));
                    messageListener.onRcGetCapabilitiesRequest(getCapabilities);
                }
                else if (method.Equals(InterfaceType.RC + "." + FunctionType.GetInteriorVehicleData))
                {
                    Controllers.RC.IncomingRequests.GetInteriorVehicleData getInteriorVehicleData = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.RC.IncomingRequests.GetInteriorVehicleData>(json);
                    handleInputConsoleLog(new RpcLogMessage(getInteriorVehicleData));
                    messageListener.onRcGetInteriorVehicleDataRequest(getInteriorVehicleData);
                }
                else if (method.Equals(InterfaceType.RC + "." + FunctionType.GetInteriorVehicleDataConsent))
                {
                    Controllers.RC.IncomingRequests.GetInteriorVehicleDataConsent getInteriorVehicleDataConsent = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.RC.IncomingRequests.GetInteriorVehicleDataConsent>(json);
                    handleInputConsoleLog(new RpcLogMessage(getInteriorVehicleDataConsent));
                    messageListener.onRcGetInteriorVehicleDataConsentRequest(getInteriorVehicleDataConsent);
                }
				else if (method.Equals(InterfaceType.RC + "." + FunctionType.ReleaseInteriorVehicleDataConsent))
				{
					Controllers.RC.IncomingRequests.ReleaseInteriorVehicleDataConsent releaseInteriorVehicleDataConsent = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.RC.IncomingRequests.ReleaseInteriorVehicleDataConsent>(json);
					handleInputConsoleLog(new RpcLogMessage(releaseInteriorVehicleDataConsent));
					messageListener.onRcReleaseInteriorVehicleDataConsentRequest(releaseInteriorVehicleDataConsent);
				}
				
				else if (method.Equals(InterfaceType.RC + "." + FunctionType.IsReady))
                {
                    Controllers.RC.IncomingRequests.IsReady isReady = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.RC.IncomingRequests.IsReady>(json);
                    handleInputConsoleLog(new RpcLogMessage(isReady));
                    messageListener.onRcIsReadyRequest(isReady);
                }
                else if (method.Equals(InterfaceType.RC + "." + FunctionType.SetInteriorVehicleData))
                {
                    Controllers.RC.IncomingRequests.SetInteriorVehicleData setInteriorVehicleData = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.RC.IncomingRequests.SetInteriorVehicleData>(json);
                    handleInputConsoleLog(new RpcLogMessage(setInteriorVehicleData));
                    messageListener.onRcSetInteriorVehicleDataRequest(setInteriorVehicleData);
				}
				else if (method.Equals(InterfaceType.RC + "." + FunctionType.OnRCStatus))
				{
					Controllers.RC.IncomingNotifications.OnRCStatus onRCStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<Controllers.RC.IncomingNotifications.OnRCStatus>(json);
					handleInputConsoleLog(new RpcLogMessage(onRCStatus));
                    messageListener.onRcOnRCStatusNotification(onRCStatus);
				}
                else
                {
                    myStringLog = new StringLogMessage(json);
                    handleInputConsoleLog(myStringLog);
                }

            }
            catch (Exception ex)
            {
                myStringLog = new StringLogMessage("Incoming: could not deserialize, json = " + json);
                myStringLog.setData("Exception = " + ex);
                handleInputConsoleLog(myStringLog);
            }

		}

		public void send(string json)
		{
            if (ws.State == WebSocketState.Open) {
				ws.Send(json);   
            }
		}

		public void sendRpc(RpcMessage rpcMessage)
		{
			if (rpcMessage == null)
				return;

			handleInputConsoleLog(new RpcLogMessage(rpcMessage));

			string json = JsonConvert.SerializeObject(rpcMessage, Formatting.None, new JsonSerializerSettings
			{
				NullValueHandling = NullValueHandling.Ignore
			});

			send(json);
		}

		public void handleInputConsoleLog (LogMessage inputMessage)
		{
			if (inputMessage == null) return;

			if ((connectionLogDispatchManager != null) && (connectionLogDispatchManager.getDispatchingHelper() != null))
			{
				queueConsoleMessage(inputMessage);
			}
			else
			{
				if (inputMessage is RpcLogMessage)
				{
					string json = Newtonsoft.Json.JsonConvert.SerializeObject(((RpcLogMessage)inputMessage).getMessage());
					Console.WriteLine(((RpcLogMessage)inputMessage).getMessage().getRpcMessageFlow().ToString().ToLower()
									  + " " + ((RpcLogMessage)inputMessage).getMessage().getRpcMessageType().ToString().ToLower()
					                  + " :" + "\n" + json);
				}
				else if (inputMessage is StringLogMessage)
				{
					Console.WriteLine(((StringLogMessage)inputMessage).getMessage());
				}
			}
		}

		public void handleSocket()
		{
			ws = new WebSocket("ws://" + getIpAddress() + ":" + getPortNumber());

            ws.Opened += (sender, e) =>
			{
				handleInputConsoleLog(new StringLogMessage("Socket: Opened."));
                //send startup rpc's

                String json = "";
                List<InterfaceType> registerComponents = null;
                List<PropertyName> subscribeNotifications = null;
                Boolean isBcOnReady = false;
                Boolean isBcOnSystemTimeReady = false;

				if(initialConnectionCommandConfig != null){
                    registerComponents = initialConnectionCommandConfig.getRegisterComponents();
                    isBcOnReady = initialConnectionCommandConfig.getIsBcOnReady();
                    isBcOnSystemTimeReady = initialConnectionCommandConfig.getIsBcOnSystemTimeReady();
                    subscribeNotifications = initialConnectionCommandConfig.getSubscribeNotifications();
                } else {
                    registerComponents = new List<InterfaceType>();

                    registerComponents.Add(InterfaceType.UI);
                    registerComponents.Add(InterfaceType.BasicCommunication);
                    registerComponents.Add(InterfaceType.Buttons);
                    registerComponents.Add(InterfaceType.VR);
                    registerComponents.Add(InterfaceType.TTS);
                    registerComponents.Add(InterfaceType.Navigation);
                    registerComponents.Add(InterfaceType.VehicleInfo);
                    registerComponents.Add(InterfaceType.RC);

                    isBcOnReady = true;

                    isBcOnSystemTimeReady = true;

                    subscribeNotifications = new List<PropertyName>();

					PropertyName propertyName = new PropertyName();

                    propertyName.setPropertyName(ComponentPrefix.Buttons, FunctionType.OnButtonSubscription);
                    subscribeNotifications.Add(propertyName);

                    propertyName = new PropertyName();
					propertyName.setPropertyName(ComponentPrefix.BasicCommunication, FunctionType.OnAppRegistered);
					subscribeNotifications.Add(propertyName);

                    propertyName = new PropertyName();
					propertyName.setPropertyName(ComponentPrefix.BasicCommunication, FunctionType.OnAppUnregistered);
					subscribeNotifications.Add(propertyName);

                    propertyName = new PropertyName();
					propertyName.setPropertyName(ComponentPrefix.BasicCommunication, FunctionType.OnPutFile);
					subscribeNotifications.Add(propertyName);

                    propertyName = new PropertyName();
					propertyName.setPropertyName(ComponentPrefix.Navigation, FunctionType.OnVideoDataStreaming);
					subscribeNotifications.Add(propertyName);

                    propertyName = new PropertyName();
                    propertyName.setPropertyName(ComponentPrefix.Navigation, FunctionType.OnAudioDataStreaming);
                    subscribeNotifications.Add(propertyName);
                }

				if (registerComponents != null)
				{
					for (int i = 0; i < registerComponents.Count; i++)
					{
						json = BuildRpc.buildRegisterComponent(registerComponents[i]);
						send(json);
					}
				}

                if (isBcOnReady)
                    sendRpc(BuildRpc.buildBasicCommunicationOnReady());

                if (isBcOnSystemTimeReady)
                    sendRpc(BuildRpc.buildBasicCommunicationOnSystemTimeReady());

				if (subscribeNotifications != null)
				{
					for (int j = 0; j < subscribeNotifications.Count; j++)
					{
						json = BuildRpc.buildSubscribeToNotification(subscribeNotifications[j]);
						send(json);
					}
				}

				if (connectionListener != null)
				{
					connectionListener.onOpen();
				}
			};

            ws.MessageReceived += (sender, e) =>
			{
				//process incoming messages

                handleIncomingMessage(e.Message);
			};

            ws.Closed += (sender, e) =>
			{
				if (connectionListener != null)
				{
					connectionListener.onClose();
				}
                handleInputConsoleLog(new StringLogMessage("Socket: Closed." + e));
			};

            ws.Error += (sender, e) =>
			{
				if (connectionListener != null)
				{
					connectionListener.onError();	
				}
                handleInputConsoleLog(new StringLogMessage("Socket: Error Occured. " + e));
			};

            ws.Open();
		}
	}
}
