﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Structs;
using HmiApiLib.Common.Enums;
using System.Collections.Generic;

namespace HmiApiLib.Controllers.SDL.OutGoingNotifications
{
	public class OnAppPermissionConsent : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.SDL;
		public Object @params;

		public class InternalData
		{
			public int? appID;
			public List<PermissionItem> consentedFunctions;
			public ConsentSource? source;
		}

		public void setAppId(int? appID)
		{
			((InternalData)@params).appID = appID;
		}

		public void setConsentedFunctions(List<PermissionItem> consentedFunctions)
		{
			((InternalData)@params).consentedFunctions = consentedFunctions;
		}

		public void setSource(ConsentSource? source)
		{
			((InternalData)@params).source = source;
		}

		public int? getAppId()
		{
			return ((InternalData)@params).appID;
		}

		public List<PermissionItem> getConsentedFunctions()
		{
			return ((InternalData)@params).consentedFunctions;
		}

		public ConsentSource? getSource()
		{
			return ((InternalData)@params).source;		}

		public OnAppPermissionConsent() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnAppPermissionConsent.ToString();
			@params = new InternalData();
		}
	}
}