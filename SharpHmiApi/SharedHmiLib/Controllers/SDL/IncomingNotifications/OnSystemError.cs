﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.SDL.IncomingNotifications
{
	public class OnSystemError : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.SDL;
		public InternalData data = null;
		public Object @params;

		public class InternalData
		{
			public SystemError error;
		}

		public SystemError getError()
		{
			if (data == null)
				setData();
			return data.error;
		}

		public OnSystemError() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnSystemError.ToString();
		}

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(@params);
			data = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}
	}
}