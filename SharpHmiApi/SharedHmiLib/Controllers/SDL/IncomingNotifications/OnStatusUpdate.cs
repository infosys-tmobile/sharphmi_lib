﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.SDL.IncomingNotifications
{
	public class OnStatusUpdate : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.SDL;
		public InternalData data = null;
		public Object @params;

		public class InternalData
		{
			public UpdateResult status;
		}

		public UpdateResult getStatus()
		{
			if (data == null)
				setData();
			return data.status;
		}

		public OnStatusUpdate() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnStatusUpdate.ToString();
		}

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(@params);
			data = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}
	}
}