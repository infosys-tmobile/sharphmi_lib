﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.SDL.IncomingNotifications
{
	public class OnDeviceStateChanged : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.SDL;
		public InternalData data = null;
		public Object @params;

		public class InternalData
		{
			public DeviceState deviceState;
			public String deviceInternalId;
			public DeviceInfo deviceId;
		}

		public DeviceState getDeviceState()
		{
			if (data == null)
				setData();
			return data.deviceState;
		}

		public String getDeviceInternalId()
		{
			if (data == null)
				setData();
			return data.deviceInternalId;
		}

		public DeviceInfo getDeviceId()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.deviceId;
		}

		public OnDeviceStateChanged() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnDeviceStateChanged.ToString();
		}

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(@params);
			data = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}
	}
}