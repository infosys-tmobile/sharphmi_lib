﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.SDL.IncomingNotifications
{
	public class AddStatisticsInfo : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.SDL;
		public InternalData data = null;
		public Object @params;

		public class InternalData
		{
			public StatisticsType statisticType;
		}

		public StatisticsType getStatisticType()
		{
			if (data == null)
				setData();
			return data.statisticType;
		}

		public AddStatisticsInfo() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.AddStatisticsInfo.ToString();
		}

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(@params);
			data = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}
	}
}