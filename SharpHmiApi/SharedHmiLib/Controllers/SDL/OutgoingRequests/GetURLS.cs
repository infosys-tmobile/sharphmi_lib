﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.SDL.OutgoingRequests
{

	public class GetURLS : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.SDL;

		public class InternalData
		{
			public int? service;
		}

		public GetURLS() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.GetURLS.ToString();
			@params = new InternalData();
		}

		public void setService(int? service)
		{
			((InternalData)@params).service = service;
		}

		public int? getService()
		{
			return ((InternalData)@params).service;
		}
	}
}