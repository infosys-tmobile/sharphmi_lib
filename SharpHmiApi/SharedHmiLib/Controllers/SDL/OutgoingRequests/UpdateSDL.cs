﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.SDL.OutgoingRequests
{

	public class UpdateSDL : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.SDL;

		public UpdateSDL() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.UpdateSDL.ToString();
		}

	}
}
