﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Structs;
using HmiApiLib.Common.Enums;
using System.Collections.Generic;

namespace HmiApiLib.Controllers.SDL.IncomingResponses
{
	public class ActivateApp : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.SDL;
        public new InternalData result = null;

        public class InternalData : Base.Result
		{
			public bool? isSDLAllowed;
			public bool? isPermissionsConsentNeeded;
			public bool? isAppPermissionsRevoked;
			public bool? isAppRevoked;
			public DeviceInfo device;
			public List<PermissionItem> appRevokedPermissions;
			public AppPriority priority;
		}

		public bool? getIsSDLAllowed()
		{
			if (result == null)
				setData();
			if (result == null)
				return null;

			return ((InternalData)result).isSDLAllowed;
		}

		public bool? getIsPermissionsConsentNeeded()
		{
			if (result == null)
				setData();
			if (result == null)
				return null;

			return ((InternalData)result).isPermissionsConsentNeeded;
		}

		public bool? getIsAppPermissionsRevoked()
		{
			if (result == null)
				setData();
			if (result == null)
				return null;

			return ((InternalData)result).isAppPermissionsRevoked;
		}

		public bool? getIsAppRevoked()
		{
			if (result == null)
				setData();
			if (result == null)
				return null;

			return ((InternalData)result).isAppRevoked;
		}

		public DeviceInfo getDevice()
		{
			if (result == null)
				setData();
			if (result == null)
				return null;

			return ((InternalData)result).device;
		}

		public List<PermissionItem> getAppRevokedPermissions()
		{
			if (result == null)
				setData();
			if (result == null)
				return null;

			return ((InternalData)result).appRevokedPermissions;
		}

		public AppPriority getPriority()
		{
			if (result == null)
				setData();

			return ((InternalData)result).priority;
		}

		public override Common.Enums.Result getResultCode()
		{
			if (result == null)
				setData();

			return (Common.Enums.Result)((InternalData)result).code;
		}

        public override string getMethod()
        {
            if (result == null)
                setData();

            return result.method;
        }

        public override void setResultCode(Common.Enums.Result res)
		{
		}

		public override void setMethod()
		{
            if (result == null)
                setData();

            ((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.ActivateApp.ToString();
		}

		public ActivateApp() : base(RpcMessageFlow.INCOMING)
		{
        }

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.result);
			result = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}
	}
}
