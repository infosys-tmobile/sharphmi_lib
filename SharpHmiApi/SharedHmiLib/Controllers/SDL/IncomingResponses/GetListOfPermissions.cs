﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Structs;
using System.Collections.Generic;
using HmiApiLib.Common.Enums;
using System;

namespace HmiApiLib.Controllers.SDL.IncomingResponses
{
    public class GetListOfPermissions : RpcResponse
    {
        InterfaceType interfaceType = InterfaceType.SDL;
        public new InternalData result = null;

        public class InternalData : Base.Result
        {
            public List<PermissionItem> allowedFunctions;
        }

        public List<PermissionItem> getAllowedFunctions()
        {
            if (result == null)
                setData();
            if (result == null)
                return null;

            return ((InternalData)result).allowedFunctions;
        }

        public override Common.Enums.Result getResultCode()
        {
            if (result == null)
                setData();

            return (Common.Enums.Result)((InternalData)result).code;
        }

        public override void setResultCode(Common.Enums.Result res)
        {
        }

        public override string getMethod()
        {
            if (result == null)
                setData();

            return result.method;
        }

        public override void setMethod()
        {
            if (result == null)
                setData();

            ((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.GetListOfPermissions.ToString();
        }

        public GetListOfPermissions() : base(RpcMessageFlow.INCOMING)
        {
        }

        private void setData()
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.result);
            result = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
        }
    }
}