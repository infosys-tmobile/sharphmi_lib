﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VR.OutgoingResponses
{
	public class IsReady : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.VR;

		public class InternalData : Base.Result
		{
			public bool? available;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.IsReady.ToString();
		}

		public void setAvailable(bool? available)
		{
			((InternalData)result).available = available;
		}

		public bool? getAvailable()
		{
			return ((InternalData)result).available;		}

		public IsReady() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}
