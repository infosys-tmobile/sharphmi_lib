﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VR.OutgoingResponses
{
	public class PerformInteraction : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.VR;

		public class InternalData : Base.Result
		{
			public int? choiceID;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.PerformInteraction.ToString();
		}

		public void setChoiceID(int? choiceId)
		{
			((InternalData)result).choiceID = choiceId;
		}

		public int? getChoiceID()
		{
			return ((InternalData)result).choiceID;		}

		public PerformInteraction() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}
