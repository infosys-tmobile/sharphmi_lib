﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Types;
using HmiApiLib.Common.Enums;
using System.Collections.Generic;

namespace HmiApiLib.Controllers.VR.IncomingRequests
{
	public class ChangeRegistration : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.VR;
#pragma warning restore 0414

		public class InternalData
		{
			public List<string> vrSynonyms;
			public Language language;
			public int? appID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public Language getLanguage()
		{
			if (@params == null)
				setData();

			return @params.language;
		}

		public List<string> getVrSynonyms()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.vrSynonyms;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public ChangeRegistration() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.ChangeRegistration.ToString();
		}
	}
}