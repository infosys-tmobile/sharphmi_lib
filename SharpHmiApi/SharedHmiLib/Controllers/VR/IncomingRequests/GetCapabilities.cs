﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VR.IncomingRequests
{
	public class GetCapabilities : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.VR;
#pragma warning restore 0414

		public GetCapabilities() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.GetCapabilities.ToString();
		}
	}
}