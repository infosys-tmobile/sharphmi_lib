﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VR.IncomingRequests
{
	public class GetLanguage : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.VR;
#pragma warning restore 0414

		public GetLanguage() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.GetLanguage.ToString();
		}
	}
}