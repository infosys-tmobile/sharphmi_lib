﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.VR.OutGoingNotifications
{
	public class Stopped : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.VR;

		public Stopped() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.Stopped.ToString();
		}
	}
}
