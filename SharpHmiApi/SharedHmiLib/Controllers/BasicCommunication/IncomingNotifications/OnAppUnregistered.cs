﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.IncomingNotifications
{
	public class OnAppUnregistered : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public InternalData data = null;
		public Object @params;

		public OnAppUnregistered() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnAppUnregistered.ToString();
		}

		public class InternalData
		{
			public bool? unexpectedDisconnect;
			public int? appID;
		}

		public bool? getUnexpectedDisconnect()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.unexpectedDisconnect;
		}

		public int? getAppId()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.appID;
		}

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(@params);
			data = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

	}
}
