﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutgoingResponses
{
	public class DecryptCertificate : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;

		public class InternalData : Base.Result
		{

		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.DecryptCertificate.ToString();
		}

		public DecryptCertificate() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}