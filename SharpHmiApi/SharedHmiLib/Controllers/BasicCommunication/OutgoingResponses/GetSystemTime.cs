﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using System;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutgoingResponses
{
    public class GetSystemTime : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;

		public class InternalData : Base.Result
		{
            public Common.Structs.DateTime systemTime;
		}

        public void setSystemTime(Common.Structs.DateTime systemTime)
        {
            ((InternalData)result).systemTime = systemTime;
        }

        public Common.Structs.DateTime getSystemTime()
        {
            return ((InternalData)result).systemTime;
        }

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
            ((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.GetSystemTime.ToString();
		}

        public GetSystemTime() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}