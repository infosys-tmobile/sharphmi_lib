﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.BasicCommunication.IncomingRequests
{
	public class SystemRequest : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public class InternalData
		{
			public RequestType requestType;
			public String fileName;
            public String requestSubType;
			public int? appID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public RequestType getRequestType()
		{
			if (@params == null)
				setData();

			return @params.requestType;
		}

		public String getFileName()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.fileName;
		}

        public String getRequestSubType()
        {
            if (@params == null)
                setData();
            if (@params == null)
                return null;

            return @params.requestSubType;
        }

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public SystemRequest() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.SystemRequest.ToString();
		}
	}
}