﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.BasicCommunication.IncomingRequests
{
    public class GetSystemTime : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
        public GetSystemTime() : base(RpcMessageFlow.INCOMING)
		{
            method = interfaceType.ToString() + "." + FunctionType.GetSystemTime.ToString();
		}
	}
}