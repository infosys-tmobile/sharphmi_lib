﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.BasicCommunication.IncomingRequests
{
	public class ActivateApp : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public class InternalData
		{
			public int? appID;
			public AppPriority priority;
			public HMILevel level;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public HMILevel getLevel()
		{
			if (@params == null)
				setData();

			return @params.level;
		}

		public AppPriority getPriority()
		{
			if (@params == null)
				setData();

			return @params.priority;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public ActivateApp() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.ActivateApp.ToString();
		}
	}
}
