﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.BasicCommunication.IncomingRequests
{
	public class DecryptCertificate : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public class InternalData
		{
			public String fileName;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public String getFileName()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

            return @params.fileName;
		}

		public DecryptCertificate() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.DecryptCertificate.ToString();
		}
	}
}