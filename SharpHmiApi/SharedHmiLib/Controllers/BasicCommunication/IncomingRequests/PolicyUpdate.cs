﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.BasicCommunication.IncomingRequests
{
	public class PolicyUpdate : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public class InternalData
		{
			public String file;
			public int? timeout;
			public List<int> retry;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public String getFile()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.file;
		}

		public int? getTimeout()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.timeout;
		}

		public List<int> getRetry()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.retry;
		}

		public PolicyUpdate() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.PolicyUpdate.ToString();
		}
	}
}