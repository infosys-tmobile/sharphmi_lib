﻿using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.BasicCommunication.IncomingRequests
{
	public class UpdateDeviceList : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public class InternalData
		{
			public List<DeviceInfo> deviceList;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public List<DeviceInfo> getDeviceList()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.deviceList;
		}

		public UpdateDeviceList() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.UpdateDeviceList.ToString();
		}
	}
}