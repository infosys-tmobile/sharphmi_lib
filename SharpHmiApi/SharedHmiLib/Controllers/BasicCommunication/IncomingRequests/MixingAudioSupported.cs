﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.BasicCommunication.IncomingRequests
{
	public class MixingAudioSupported : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public MixingAudioSupported() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.MixingAudioSupported.ToString();
		}
	}
}
