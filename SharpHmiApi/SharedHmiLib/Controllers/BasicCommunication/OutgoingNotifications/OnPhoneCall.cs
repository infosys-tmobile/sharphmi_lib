﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications
{
	public class OnPhoneCall : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public Object @params;
		public class InternalData
		{
			public bool? isActive;
		}

		public void setActive(bool? isActive)
		{
			((InternalData)@params).isActive = isActive;
		}

		public bool? getActive()
		{
			return ((InternalData)@params).isActive;
		}

		public OnPhoneCall() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnPhoneCall.ToString();
			@params = new InternalData();
		}
	}
}