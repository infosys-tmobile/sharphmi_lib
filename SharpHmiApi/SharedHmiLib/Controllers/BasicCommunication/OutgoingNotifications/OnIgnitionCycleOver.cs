﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications
{
	public class OnIgnitionCycleOver : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;

		public OnIgnitionCycleOver() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnIgnitionCycleOver.ToString();
		}
	}
}