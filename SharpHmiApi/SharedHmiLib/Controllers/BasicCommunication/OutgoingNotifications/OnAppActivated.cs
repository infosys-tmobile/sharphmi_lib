﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications
{
	public class OnAppActivated : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public Object @params;

		public class InternalData
		{
			public int? appID;
			public int? windowID;

		}

		public void setAppId(int? id)
		{
			((InternalData)@params).appID = id;
		}

		public int? getAppId()
		{
			return ((InternalData)@params).appID;
		}

		public void setWindowId(int? id)
		{
			((InternalData)@params).windowID = id;
		}

		public int? getWindowId()
		{
			return ((InternalData)@params).windowID;
		}

		public OnAppActivated() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnAppActivated.ToString();
			@params = new InternalData();
		}
	}
}
