﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications
{
public class OnExitAllApplications : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public InternalData data = null;
		public Object @params;

		public OnExitAllApplications() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnExitAllApplications.ToString();
            @params = new InternalData();
		}

		public class InternalData
		{
			public ApplicationsCloseReason? reason;
		}

		public void setApplicationsCloseReason(ApplicationsCloseReason? reason)
		{
			((InternalData)@params).reason = reason;
		}

		public ApplicationsCloseReason? getApplicationsCloseReason()
		{
			return ((InternalData)@params).reason;
		}
	}
}
