﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications
{
    public class OnSystemTimeReady : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;

        public OnSystemTimeReady() : base(RpcMessageFlow.OUTGOING)
		{
            method = interfaceType.ToString() + "." + FunctionType.OnSystemTimeReady.ToString();
		}
	}
}