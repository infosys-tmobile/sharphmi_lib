﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications
{
	public class OnAwakeSDL : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;

		public OnAwakeSDL() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnAwakeSDL.ToString();
		}
	}
}
