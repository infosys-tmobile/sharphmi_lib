﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.BasicCommunication.OutGoingNotifications
{
	public class OnDeactivateHMI : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.BasicCommunication;
		public Object @params;

		public class InternalData
		{
			public bool? isDeactivated;
		}

		public void deactivate(bool? isDeactivated)
		{
			((InternalData)@params).isDeactivated = isDeactivated;
		}

		public bool? getDeactivatedStatus()
		{
			return ((InternalData)@params).isDeactivated;
		}

		public OnDeactivateHMI() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnDeactivateHMI.ToString();
			@params = new InternalData();
		}
	}
}