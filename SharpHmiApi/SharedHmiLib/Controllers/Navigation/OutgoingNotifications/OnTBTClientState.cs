﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.Navigation.OutGoingNotifications
{
	public class OnTBTClientState : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.Navigation;
		public Object @params;

		public class InternalData
		{
			public TBTState? state;
		}

		public void setState(TBTState? state)
		{
			((InternalData)@params).state = state;
		}

		public TBTState? getState()
		{
			return ((InternalData)@params).state;
		}

		public OnTBTClientState() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnTBTClientState.ToString();
			@params = new InternalData();
		}
	}
}