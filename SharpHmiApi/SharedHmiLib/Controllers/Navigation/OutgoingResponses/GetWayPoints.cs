﻿using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.Navigation.OutgoingResponses
{
	public class GetWayPoints : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.Navigation;

		public class InternalData : Base.Result
		{
			public List<LocationDetails> wayPoints;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.GetWayPoints.ToString();
		}

		public void setWayPoints(List<LocationDetails> wayPoints)
		{
			((InternalData)result).wayPoints = wayPoints;
		}

		public List<LocationDetails> getWayPoints()
		{
			return ((InternalData)result).wayPoints;
		}

		public GetWayPoints() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}