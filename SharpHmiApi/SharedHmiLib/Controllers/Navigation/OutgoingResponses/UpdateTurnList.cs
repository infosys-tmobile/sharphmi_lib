﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.Navigation.OutgoingResponses
{
	public class UpdateTurnList : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.Navigation;

		public class InternalData : Base.Result
		{

		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.UpdateTurnList.ToString();
		}

		public UpdateTurnList() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}