﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using System.Collections.Generic;

namespace HmiApiLib.Controllers.Navigation.OutgoingResponses
{
    public class SetVideoConfig : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.Navigation;

		public class InternalData : Base.Result
		{
            public List<string> rejectedParams;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

        public void setRejectedParams(List<string> rejectedParams)
        {
            ((InternalData)result).rejectedParams = rejectedParams;
        }

        public List<string> getRejectedParams()
        {
            return ((InternalData)result).rejectedParams;
        }

		public override void setMethod()
		{
            ((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.SetVideoConfig.ToString();
		}

        public SetVideoConfig() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}