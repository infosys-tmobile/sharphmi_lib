﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.Navigation.IncomingNotifications
{
	public class OnWayPointChange : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.Navigation;
		public InternalData data = null;
		public Object @params;

		public class InternalData
		{
			public List<LocationDetails> wayPoints;
		}

		public List<LocationDetails> getWayPoints()
		{
			if (data == null)
				setData();
			return data.wayPoints;
		}

		public OnWayPointChange() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnWayPointChange.ToString();
		}

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(@params);
			data = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}
	}
}