﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.Navigation.IncomingNotifications
{
	public class OnAudioDataStreaming : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.Navigation;
		public InternalData data = null;
		public Object @params;

		public class InternalData
		{
			public bool? available;
		}

		public bool? getAvailable()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.available;
		}

		public OnAudioDataStreaming() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnAudioDataStreaming.ToString();
		}

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(@params);
			data = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}
	}
}