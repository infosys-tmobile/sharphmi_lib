﻿using HmiApiLib.Common.Structs;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.Navigation.IncomingRequests
{
    public class SetVideoConfig : RpcRequest
	{
		private InterfaceType interfaceType = InterfaceType.Navigation;
		public class InternalData
		{
            public VideoConfig videoConfig;
			public int? appID;
		}

		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

        public VideoConfig getVideoConfig()
        {
            if (@params == null)
                setData();
            if (@params == null)
                return null;

            return @params.videoConfig;
        }

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

        public SetVideoConfig() : base(RpcMessageFlow.INCOMING)
		{
            method = interfaceType.ToString() + "." + FunctionType.SetVideoConfig.ToString();
		}
	}
}