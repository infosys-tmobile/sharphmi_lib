﻿using HmiApiLib.Common.Structs;
using HmiApiLib.Base;
using System.Collections.Generic;
using System;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.Navigation.IncomingRequests
{
	public class SendLocation : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.Navigation;
		public class InternalData
		{
			public int? appID;
			public double? longitudeDegrees;
			public double? latitudeDegrees;
			public String locationName;
			public String locationDescription;
			public List<String> addressLines;
			public String phoneNumber;
			public Image locationImage;
            public DeliveryMode deliveryMode;
			public Common.Structs.DateTime timeStamp;
			public OASISAddress address;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public double? getLongitudeDegrees()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.longitudeDegrees;
		}

		public double? getLatitudeDegrees()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.latitudeDegrees;
		}

		public String getLocationName()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.locationName;
		}

		public String getLocationDescription()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.locationDescription;
		}

		public List<String> getAddressLines()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.addressLines;
		}

		public String getPhoneNumber()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.phoneNumber;
		}

		public Image getLocationImage()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.locationImage;
		}

		public Common.Structs.DateTime getTimeStamp()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.timeStamp;
		}

		public OASISAddress getAddress()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.address;
		}

		public DeliveryMode getDeliveryMode()
		{
			if (@params == null)
				setData();

			return @params.deliveryMode;
		}

		public SendLocation() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.SendLocation.ToString();
		}
	}
}