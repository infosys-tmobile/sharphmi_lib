﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.Navigation.IncomingRequests
{
	public class GetWayPoints : RpcRequest
	{
		private InterfaceType interfaceType = InterfaceType.Navigation;
		public class InternalData
		{
			public WayPointType wayPointType;
			public int? appID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public WayPointType getWayPointType()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return WayPointType.ALL;
			return @params.wayPointType;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public GetWayPoints() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.GetWayPoints.ToString();
		}
	}
}