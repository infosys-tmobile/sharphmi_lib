﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VehicleInfo.IncomingRequests
{
	public class GetDTCs : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.VehicleInfo;
		public class InternalData
		{
			public int? ecuName;
			public int? dtcMask;
			public int? appID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public int? getEcuName()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.ecuName;
		}


		public int? getDtcMask()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.dtcMask;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public GetDTCs() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.GetDTCs.ToString();
		}
	}
}