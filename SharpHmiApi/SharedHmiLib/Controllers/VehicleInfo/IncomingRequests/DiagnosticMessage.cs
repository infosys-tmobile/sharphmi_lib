﻿using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VehicleInfo.IncomingRequests
{
	public class DiagnosticMessage : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.VehicleInfo;
		public class InternalData
		{
			public int? targetID;
			public int? messageLength;
			public List<int> messageData;
			public int? appID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public int? getTargetID()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.targetID;
		}

		public int? getMessageLength()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.messageLength;
		}


		public List<int> getMessageData()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.messageData;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public DiagnosticMessage() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.DiagnosticMessage.ToString();
		}
	}
}