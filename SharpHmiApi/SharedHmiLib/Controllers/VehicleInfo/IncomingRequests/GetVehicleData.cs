﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VehicleInfo.IncomingRequests
{
	public class GetVehicleData : RpcRequest
	{
		InterfaceType interfaceType = InterfaceType.VehicleInfo;
		public class InternalData
		{
			public bool? gps;
			public bool? speed;
			public bool? rpm;
			public bool? fuelLevel;
			public bool? fuelLevel_State;
			public bool? instantFuelConsumption;
			public bool? externalTemperature;
			public bool? vin;
			public bool? prndl;
			public bool? tirePressure;
			public bool? odometer;
			public bool? beltStatus;
			public bool? bodyInformation;
			public bool? deviceStatus;
			public bool? driverBraking;
			public bool? wiperStatus;
			public bool? headLampStatus;
			public bool? engineTorque;
			public bool? accPedalPosition;
			public bool? steeringWheelAngle;
			public bool? eCallInfo;
			public bool? airbagStatus;
			public bool? emergencyEvent;
			public bool? clusterModeStatus;
			public bool? myKey;
            public bool? electronicParkBrakeStatus;
            public bool? engineOilLife;
            public bool? fuelRange;
            public bool? turnSignal;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public bool? getGps()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.gps;
		}

		public bool? getSpeed()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.speed;
		}

		public bool? getRpm()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.rpm;
		}

		public bool? getFuelLevel()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.fuelLevel;
		}

		public bool? getFuelLevel_State()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.fuelLevel_State;
		}

		public bool? getInstantFuelConsumption()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.instantFuelConsumption;
		}

		public bool? getExternalTemperature()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.externalTemperature;
		}

		public bool? getVin()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.vin;
		}

		public bool? getPrndl()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.prndl;
		}

		public bool? getTirePressure()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.tirePressure;
		}

		public bool? getOdometer()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.odometer;
		}

		public bool? getBeltStatus()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.beltStatus;
		}

		public bool? getBodyInformation()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.bodyInformation;
		}

		public bool? getDeviceStatus()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.deviceStatus;
		}

		public bool? getDriverBraking()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.driverBraking;
		}

		public bool? getWiperStatus()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.wiperStatus;
		}

		public bool? getHeadLampStatus()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.headLampStatus;
		}

		public bool? getEngineTorque()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.engineTorque;
		}

		public bool? getAccPedalPosition()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.accPedalPosition;
		}

		public bool? getSteeringWheelAngle()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.steeringWheelAngle;
		}

		public bool? getECallInfo()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.eCallInfo;
		}

		public bool? getAirbagStatus()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.airbagStatus;
		}

		public bool? getEmergencyEvent()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.emergencyEvent;
		}

		public bool? getClusterModeStatus()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.clusterModeStatus;
		}

		public bool? getMyKey()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.myKey;
		}

		public bool? getElectronicParkBrakeStatus()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.electronicParkBrakeStatus;
		}

		public bool? getTurnSignal()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.turnSignal;
		}

        public bool? getEngineOilLife()
        {
            if (@params == null)
                setData();
            if (@params == null)
                return null;

            return @params.engineOilLife;
        }

        public bool? getFuelRange()
        {
            if (@params == null)
                setData();
            if (@params == null)
                return null;

            return @params.fuelRange;
        }

		public GetVehicleData() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.GetVehicleData.ToString();
		}
	}
}