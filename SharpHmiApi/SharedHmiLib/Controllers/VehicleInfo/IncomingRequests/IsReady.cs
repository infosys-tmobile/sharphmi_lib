﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VehicleInfo.IncomingRequests
{
	public class IsReady : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.VehicleInfo;
#pragma warning restore 0414

		public IsReady() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.IsReady.ToString();
		}
	}
}