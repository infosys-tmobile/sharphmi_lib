﻿using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VehicleInfo.OutgoingResponses
{
	public class DiagnosticMessage : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.VehicleInfo;

		public class InternalData : Base.Result
		{
			public List<int> messageDataResult;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.DiagnosticMessage.ToString();
		}

		public void setMessageDataResult(List<int> messageDataResult)
		{
			((InternalData)result).messageDataResult = messageDataResult;
		}

		public List<int> getMessageDataResult()
		{
			return ((InternalData)result).messageDataResult;		}

		public DiagnosticMessage() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}