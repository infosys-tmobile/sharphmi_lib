﻿using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VehicleInfo.OutgoingResponses
{
	public class ReadDID : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.VehicleInfo;

		public class InternalData : Base.Result
		{
			public List<DIDResult> didResult;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.ReadDID.ToString();
		}

		public void setDidResult(List<DIDResult> didResult)
		{
			((InternalData)result).didResult = didResult;
		}

		public List<DIDResult> getDidResult()
		{
			return ((InternalData)result).didResult;		}

		public ReadDID() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}