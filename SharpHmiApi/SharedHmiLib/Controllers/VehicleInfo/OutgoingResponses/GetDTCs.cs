﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VehicleInfo.OutgoingResponses
{
	public class GetDTCs : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.VehicleInfo;

		public class InternalData : Base.Result
		{
			public int? ecuHeader;
			public List<String> dtc;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.GetDTCs.ToString();
		}

		public void setEcuHeader(int? ecuHeader)
		{
			((InternalData)result).ecuHeader = ecuHeader;
		}

		public void setDtc(List<String> dtc)
		{
			((InternalData)result).dtc = dtc;
		}

		public int? getEcuHeader()
		{
			return ((InternalData)result).ecuHeader;
		}

		public List<String> getDtc()
		{
			return ((InternalData)result).dtc;		}

		public GetDTCs() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}