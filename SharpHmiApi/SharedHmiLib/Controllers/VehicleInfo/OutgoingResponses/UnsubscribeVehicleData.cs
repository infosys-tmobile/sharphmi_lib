﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VehicleInfo.OutgoingResponses
{
	public class UnsubscribeVehicleData : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.VehicleInfo;

		public class InternalData : Base.Result
		{
			public VehicleDataResult gps;
			public VehicleDataResult speed;
			public VehicleDataResult rpm;
			public VehicleDataResult fuelLevel;
			public VehicleDataResult fuelLevel_State;
			public VehicleDataResult instantFuelConsumption;
			public VehicleDataResult externalTemperature;
			public VehicleDataResult prndl;
			public VehicleDataResult tirePressure;
			public VehicleDataResult odometer;
			public VehicleDataResult beltStatus;
			public VehicleDataResult bodyInformation;
			public VehicleDataResult deviceStatus;
			public VehicleDataResult driverBraking;
			public VehicleDataResult wiperStatus;
			public VehicleDataResult headLampStatus;
			public VehicleDataResult engineTorque;
			public VehicleDataResult accPedalPosition;
			public VehicleDataResult steeringWheelAngle;
			public VehicleDataResult eCallInfo;
			public VehicleDataResult airbagStatus;
			public VehicleDataResult emergencyEvent;
			public VehicleDataResult clusterModes;
			public VehicleDataResult myKey;
            public VehicleDataResult electronicParkBrakeStatus;
            public VehicleDataResult engineOilLife;
            public VehicleDataResult fuelRange;
			public VehicleDataResult turnSignal;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.UnsubscribeVehicleData.ToString();
		}

		public void setGps(VehicleDataResult gps)
		{
			((InternalData)result).gps = gps;
		}

		public void setSpeed(VehicleDataResult speed)
		{
			((InternalData)result).speed = speed;
		}

		public void setRpm(VehicleDataResult rpm)
		{
			((InternalData)result).rpm = rpm;
		}

		public void setFuelLevel(VehicleDataResult fuelLevel)
		{
			((InternalData)result).fuelLevel = fuelLevel;
		}

		public void setFuelLevel_State(VehicleDataResult fuelLevel_State)
		{
			((InternalData)result).fuelLevel_State = fuelLevel_State;
		}

		public void setInstantFuelConsumption(VehicleDataResult instantFuelConsumption)
		{
			((InternalData)result).instantFuelConsumption = instantFuelConsumption;
		}

		public void setExternalTemperature(VehicleDataResult externalTemperature)
		{
			((InternalData)result).externalTemperature = externalTemperature;
		}

		public void setPrndl(VehicleDataResult prndl)
		{
			((InternalData)result).prndl = prndl;
		}

		public void setTirePressure(VehicleDataResult tirePressure)
		{
			((InternalData)result).tirePressure = tirePressure;
		}

		public void setOdometer(VehicleDataResult odometer)
		{
			((InternalData)result).odometer = odometer;
		}

		public void setBeltStatus(VehicleDataResult beltStatus)
		{
			((InternalData)result).beltStatus = beltStatus;
		}

		public void setBodyInformation(VehicleDataResult bodyInformation)
		{
			((InternalData)result).bodyInformation = bodyInformation;
		}

		public void setDeviceStatus(VehicleDataResult deviceStatus)
		{
			((InternalData)result).deviceStatus = deviceStatus;
		}

		public void setdriverBraking(VehicleDataResult driverBraking)
		{
			((InternalData)result).driverBraking = driverBraking;
		}

		public void setWiperStatus(VehicleDataResult wiperStatus)
		{
			((InternalData)result).wiperStatus = wiperStatus;
		}

		public void setHeadLampStatus(VehicleDataResult headLampStatus)
		{
			((InternalData)result).headLampStatus = headLampStatus;
		}

		public void setEngineTorque(VehicleDataResult engineTorque)
		{
			((InternalData)result).engineTorque = engineTorque;
		}

		public void setAccPedalPosition(VehicleDataResult accPedalPosition)
		{
			((InternalData)result).accPedalPosition = accPedalPosition;
		}

		public void setSteeringWheelAngle(VehicleDataResult steeringWheelAngle)
		{
			((InternalData)result).steeringWheelAngle = steeringWheelAngle;
		}

		public void setECallInfo(VehicleDataResult eCallInfo)
		{
			((InternalData)result).eCallInfo = eCallInfo;
		}

		public void setAirbagStatus(VehicleDataResult airbagStatus)
		{
			((InternalData)result).airbagStatus = airbagStatus;
		}

		public void setEmergencyEvent(VehicleDataResult emergencyEvent)
		{
			((InternalData)result).emergencyEvent = emergencyEvent;
		}

		public void setClusterModes(VehicleDataResult clusterModes)
		{
			((InternalData)result).clusterModes = clusterModes;
		}

		public void setMyKey(VehicleDataResult myKey)
		{
			((InternalData)result).myKey = myKey;
		}

		public void setElectronicParkBrakeStatus(VehicleDataResult electronicParkBrakeStatus)
		{
			((InternalData)result).electronicParkBrakeStatus = electronicParkBrakeStatus;
		}

		public void setTurnSignal(VehicleDataResult turnSignal)
		{
			((InternalData)result).turnSignal = turnSignal;
		}

        public void setEngineOilLife(VehicleDataResult engineOilLife)
        {
            ((InternalData)result).engineOilLife = engineOilLife;
        }

        public void setFuelRange(VehicleDataResult fuelRange)
        {
            ((InternalData)result).fuelRange = fuelRange;
        }

		public VehicleDataResult getGps()
		{
			return ((InternalData)result).gps;
		}

		public VehicleDataResult getSpeed()
		{
			return ((InternalData)result).speed;
		}

		public VehicleDataResult getRpm()
		{
			return ((InternalData)result).rpm;
		}

		public VehicleDataResult getFuelLevel()
		{
			return ((InternalData)result).fuelLevel;
		}

		public VehicleDataResult getFuelLevel_State()
		{
			return ((InternalData)result).fuelLevel_State;
		}

		public VehicleDataResult getInstantFuelConsumption()
		{
			return ((InternalData)result).instantFuelConsumption;
		}

		public VehicleDataResult getExternalTemperature()
		{
			return ((InternalData)result).externalTemperature;
		}

		public VehicleDataResult getPrndl()
		{
			return ((InternalData)result).prndl;
		}

		public VehicleDataResult getTirePressure()
		{
			return ((InternalData)result).tirePressure;
		}

		public VehicleDataResult getOdometer()
		{
			return ((InternalData)result).odometer;
		}

		public VehicleDataResult getBeltStatus()
		{
			return ((InternalData)result).beltStatus;
		}

		public VehicleDataResult getBodyInformation()
		{
			return ((InternalData)result).bodyInformation;
		}

		public VehicleDataResult getDeviceStatus()
		{
			return ((InternalData)result).deviceStatus;
		}

		public VehicleDataResult getdriverBraking()
		{
			return ((InternalData)result).driverBraking;
		}

		public VehicleDataResult getWiperStatus()
		{
			return ((InternalData)result).wiperStatus;
		}

		public VehicleDataResult getHeadLampStatus()
		{
			return ((InternalData)result).headLampStatus;
		}

		public VehicleDataResult getEngineTorque()
		{
			return ((InternalData)result).engineTorque;
		}

		public VehicleDataResult getAccPedalPosition()
		{
			return ((InternalData)result).accPedalPosition;
		}

		public VehicleDataResult getSteeringWheelAngle()
		{
			return ((InternalData)result).steeringWheelAngle;
		}

		public VehicleDataResult getECallInfo()
		{
			return ((InternalData)result).eCallInfo;
		}

		public VehicleDataResult getAirbagStatus()
		{
			return ((InternalData)result).airbagStatus;
		}

		public VehicleDataResult getEmergencyEvent()
		{
			return ((InternalData)result).emergencyEvent;
		}

		public VehicleDataResult getClusterModes()
		{
			return ((InternalData)result).clusterModes;
		}

		public VehicleDataResult getMyKey()
		{
			return ((InternalData)result).myKey;		}

		public VehicleDataResult getElectronicParkBrakeStatus()
		{
			return ((InternalData)result).electronicParkBrakeStatus;
		}

		public VehicleDataResult getTurnSignal()
		{
			return ((InternalData)result).turnSignal;
		}

        public VehicleDataResult getEngineOilLife()
        {
            return ((InternalData)result).engineOilLife;
        }

        public VehicleDataResult getFuelRange()
        {
            return ((InternalData)result).fuelRange;
        }

		public UnsubscribeVehicleData() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}