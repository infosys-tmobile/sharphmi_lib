﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.VehicleInfo.OutgoingResponses
{
	public class GetVehicleType : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.VehicleInfo;

		public class InternalData : Base.Result
		{
			public VehicleType vehicleType;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.GetVehicleType.ToString();
		}

		public void setVehicleType(VehicleType vehicleType)
		{
			((InternalData)result).vehicleType = vehicleType;
		}

		public VehicleType getVehicleType()
		{
			return ((InternalData)result).vehicleType;		}

		public GetVehicleType() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}