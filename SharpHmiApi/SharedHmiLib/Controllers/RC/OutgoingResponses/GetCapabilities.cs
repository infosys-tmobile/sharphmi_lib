﻿using HmiApiLib.Base;
using HmiApiLib.Common.Structs;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.RC.OutgoingResponses
{
	public class GetCapabilities : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.RC;

		public class InternalData : Base.Result
		{
			public RemoteControlCapabilities remoteControlCapability;
			//public SeatLocationCapability seatLocationCapability;

		}

		public void setRemoteControlCapabilities(RemoteControlCapabilities remoteControlCapabilities)
		{
			((InternalData)result).remoteControlCapability = remoteControlCapabilities;
		}

		public RemoteControlCapabilities getRemoteControlCapabilities()
		{
			return ((InternalData)result).remoteControlCapability;
		}
		/*public void setSeatLocationCapability(SeatLocationCapability seatLocationCapability)
		{
			((InternalData)result).seatLocationCapability = seatLocationCapability;
		}

		public SeatLocationCapability getSeatLocationCapability()
		{
			return ((InternalData)result).seatLocationCapability;
		}*/
		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.GetCapabilities.ToString();
		}

		public GetCapabilities() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}
