﻿using HmiApiLib.Base;
using HmiApiLib.Common.Structs;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.RC.OutgoingResponses
{
	public class SetInteriorVehicleData : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.RC;

		public class InternalData : Base.Result
		{
			public ModuleData moduleData;
		}

		public void setModuleData(ModuleData moduleData)
		{
			((InternalData)result).moduleData = moduleData;
		}

		public ModuleData getModuleData()
		{
			return ((InternalData)result).moduleData;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.SetInteriorVehicleData.ToString();
		}

		public SetInteriorVehicleData() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}
