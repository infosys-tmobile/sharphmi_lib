﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;
using System;
using System.Collections.Generic;

namespace HmiApiLib.Controllers.RC.IncomingRequests
{
	public class ReleaseInteriorVehicleDataConsent : RpcRequest
	{
        private InterfaceType interfaceType = InterfaceType.RC;
		public class InternalData
		{
            public ModuleType? moduleType;
			public string moduleId;

		}

		public ModuleType? getModuleType()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.moduleType;
		}
		public string getModuleId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.moduleId;
		}
		

		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public ReleaseInteriorVehicleDataConsent() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.ReleaseInteriorVehicleDataConsent.ToString();
		}
	}
}