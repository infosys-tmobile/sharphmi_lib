﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.RC.IncomingRequests
{
	public class GetCapabilities : RpcRequest
	{
        private InterfaceType interfaceType = InterfaceType.RC;
		public class InternalData
		{

		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public GetCapabilities() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.GetCapabilities.ToString();
		}
	}
}