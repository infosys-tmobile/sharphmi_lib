﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;
using System;
using System.Collections.Generic;

namespace HmiApiLib.Controllers.RC.IncomingRequests
{
	public class GetInteriorVehicleDataConsent : RpcRequest
	{
        private InterfaceType interfaceType = InterfaceType.RC;
		public class InternalData
		{
            public ModuleType? moduleType;
			public List<string> moduleIds;

		}

		public ModuleType? getModuleType()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.moduleType;
		}
		public List<string> getModuleIds()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.moduleIds;
		}
		

		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public GetInteriorVehicleDataConsent() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.GetInteriorVehicleDataConsent.ToString();
		}
	}
}