﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.TTS.OutGoingNotifications
{
	public class OnResetTimeout : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.TTS;
		public Object @params;

		public class InternalData
		{
			public int? appID;
			public String methodName;
		}

		public void setAppId(int? appID)
		{
			((InternalData)@params).appID = appID;
		}

		public void setMethodName(String methodName)
		{
			((InternalData)@params).methodName = methodName;
		}

		public int? getAppId()
		{
			return ((InternalData)@params).appID;
		}

		public String getMethodName()
		{
			return ((InternalData)@params).methodName;		}

		public OnResetTimeout() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnResetTimeout.ToString();
			@params = new InternalData();
		}
	}
}