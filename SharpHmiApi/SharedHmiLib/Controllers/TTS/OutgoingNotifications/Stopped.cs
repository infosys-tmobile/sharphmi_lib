﻿using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.TTS.OutGoingNotifications
{
	public class Stopped : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.TTS;

		public Stopped() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.Stopped.ToString();
		}
	}
}
