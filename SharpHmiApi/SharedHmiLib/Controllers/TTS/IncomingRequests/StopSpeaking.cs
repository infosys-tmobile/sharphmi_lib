﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.TTS.IncomingRequests
{
	public class StopSpeaking : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.TTS;
#pragma warning restore 0414

		public class InternalData
		{
		}

		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}
		public StopSpeaking() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.StopSpeaking.ToString();
		}
	}
}