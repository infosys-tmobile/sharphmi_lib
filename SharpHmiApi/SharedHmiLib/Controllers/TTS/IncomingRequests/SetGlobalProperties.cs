﻿using HmiApiLib.Common.Structs;
using HmiApiLib.Base;
using System.Collections.Generic;
using HmiApiLib.Types;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.TTS.IncomingRequests
{
	public class SetGlobalProperties : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.TTS;
#pragma warning restore 0414

		public class InternalData
		{
			public List<TTSChunk> helpPrompt;
			public List<TTSChunk> timeoutPrompt;
			public int? appID;
		}

		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public List<TTSChunk> getHelpPrompt()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.helpPrompt;
		}

		public List<TTSChunk> getTimeoutPrompt()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.timeoutPrompt;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public SetGlobalProperties() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.SetGlobalProperties.ToString();
		}
	}
}