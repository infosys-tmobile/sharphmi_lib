﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.TTS.OutgoingResponses
{
	public class StopSpeaking : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.TTS;

		public class InternalData : Base.Result
		{

		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.StopSpeaking.ToString();
		}

		public StopSpeaking() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}
