﻿using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.TTS.OutgoingResponses
{
	public class GetCapabilities : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.TTS;

		public class InternalData : Base.Result
		{
			public List<SpeechCapabilities> speechCapabilities;
			public List<PrerecordedSpeech> prerecordedSpeechCapabilities;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.GetCapabilities.ToString();
		}

		public void setSpeechCapabilities(List<SpeechCapabilities> speechCapabilities)
		{
			((InternalData)result).speechCapabilities = speechCapabilities;
		}

		public void setPrerecordedSpeechCapabilities(List<PrerecordedSpeech> prerecordedSpeechCapabilities)
		{
			((InternalData)result).prerecordedSpeechCapabilities = prerecordedSpeechCapabilities;
		}

		public List<SpeechCapabilities> getSpeechCapabilities()
		{
			return ((InternalData)result).speechCapabilities;
		}

		public List<PrerecordedSpeech> getPrerecordedSpeechCapabilities()
		{
			return ((InternalData)result).prerecordedSpeechCapabilities;		}

		public GetCapabilities() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}