﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.SDL.IncomingResponses
{
	public class ButtonPress : RpcResponse
	{
        InterfaceType interfaceType = InterfaceType.Buttons;
		public Object @params;

		public class InternalData : Base.Result
		{
		}

		public override Common.Enums.Result getResultCode()
		{
			if (result == null)
				setData();

			return (Common.Enums.Result)((InternalData)result).code;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.ButtonPress.ToString();
		}

		public ButtonPress() : base(RpcMessageFlow.INCOMING)
		{
			setMethod();
		}

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(@params);
			result = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}
	}
}
