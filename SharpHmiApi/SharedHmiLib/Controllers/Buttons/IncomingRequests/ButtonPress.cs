﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.Buttons.IncomingRequests
{
	public class ButtonPress : RpcRequest
	{
		private InterfaceType interfaceType = InterfaceType.Buttons;
		public class InternalData
		{
			public ModuleType? moduleType;
			public ButtonName? buttonName;
			public ButtonPressMode? buttonPressMode;
			public int? appID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public ModuleType? getModuleType()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;
            
			return @params.moduleType;
		}

		public ButtonName? getButtonName()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.buttonName;
		}

		public ButtonPressMode? getButtonPressMode()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.buttonPressMode;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public ButtonPress() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.ButtonPress.ToString();
		}
	}
}