﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.Buttons.OutGoingNotifications
{
	public class OnButtonPress : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.Buttons;
		public Object @params;

		public class InternalData
		{
			public ButtonName? name;
			public ButtonPressMode? mode;
			public int? customButtonID;
			public int? appID;
		}

		public void setName(ButtonName? name)
		{
			((InternalData)@params).name = name;
		}

		public void setMode(ButtonPressMode? mode)
		{
			((InternalData)@params).mode = mode;
		}

		public void setCustomButtonID(int? customButtonID)
		{
			((InternalData)@params).customButtonID = customButtonID;
		}

		public void setAppId(int? appID)
		{
			((InternalData)@params).appID = appID;
		}

		public ButtonName? getName()
		{
			return ((InternalData)@params).name;
		}

		public ButtonPressMode? getMode()
		{
			return ((InternalData)@params).mode;
		}

		public int? getCustomButtonID()
		{
			return ((InternalData)@params).customButtonID;
		}

		public int? getAppId()
		{
			return ((InternalData)@params).appID;		}

		public OnButtonPress() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnButtonPress.ToString();
			@params = new InternalData();
		}
	}
}