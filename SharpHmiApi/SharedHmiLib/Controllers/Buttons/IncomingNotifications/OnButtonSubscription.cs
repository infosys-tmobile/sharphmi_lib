﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.Buttons.IncomingNotifications
{
	public class OnButtonSubscription : RequestNotifyMessage
	{
		InterfaceType interfaceType = InterfaceType.Buttons;
		public InternalData data = null;
		public Object @params;

		public class InternalData
		{
			public ButtonName name;
			public bool? isSubscribed;
			public int? appID;
		}

		public ButtonName getName()
		{
			if (data == null)
				setData();
			
			return data.name;
		}

		public bool? getSubscribe()
		{
			if (data == null)
				setData();
			if (data == null)
				return null;

			return data.isSubscribed;
		}

		public int? getAppId()
		{
			if (data == null)
                setData();
			if (data == null)
				return null;

			return data.appID;
		}


		public OnButtonSubscription() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnButtonSubscription.ToString();
		}

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(@params);
			data = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}
	}
}