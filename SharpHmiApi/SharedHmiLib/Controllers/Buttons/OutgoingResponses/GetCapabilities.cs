﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.Buttons.OutgoingResponses
{
	public class GetCapabilities : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.Buttons;

		public class InternalData : Base.Result
		{
			public List<ButtonCapabilities> capabilities;
			public PresetBankCapabilities presetBankCapabilities;
		}

		public void setButtonCapabilities(List<ButtonCapabilities> capabilities)
		{
			((InternalData)result).capabilities = (List<ButtonCapabilities>)capabilities;
		}

		public void setPresetBankCapabilities(PresetBankCapabilities presetBankCapabilities)
		{
			((InternalData)result).presetBankCapabilities = (PresetBankCapabilities)presetBankCapabilities;
		}

		public List<ButtonCapabilities> getButtonCapabilities()
		{
			return ((InternalData)result).capabilities;
		}

		public PresetBankCapabilities getPresetBankCapabilities()
		{
			return ((InternalData)result).presetBankCapabilities;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.GetCapabilities.ToString();
		}

		public GetCapabilities() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}
