﻿using HmiApiLib.Common.Structs;
using HmiApiLib.Base;
using HmiApiLib.Types;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.UI.IncomingRequests
{
	public class AddSubMenu : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.UI;
#pragma warning restore 0414

		public class InternalData
		{
			public int? menuID;
			public MenuParams menuParams;
            public Image menuIcon;
			public int? appID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public MenuParams getMenuParams()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.menuParams;
		}

		public int? getMenuID()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.menuID;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public Image getMenuIcon()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

            Image icon = @params.menuIcon;
			return icon;
		}

		public AddSubMenu() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.AddSubMenu.ToString();
		}
	}
}