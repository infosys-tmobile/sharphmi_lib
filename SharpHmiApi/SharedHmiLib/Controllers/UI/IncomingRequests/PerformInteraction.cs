﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.IncomingRequests
{
	public class PerformInteraction : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.UI;
#pragma warning restore 0414

		public class InternalData
		{
			public TextFieldStruct initialText;
			public List<Choice> choiceSet;
			public String vrHelpTitle;
			public List<VrHelpItem> vrHelp;
			public int? timeout;
			public LayoutMode interactionLayout;
			public int? appID;
		}

		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public TextFieldStruct getInitialText()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			TextFieldStruct initialText = @params.initialText;
			return initialText;
		}

		public int? getTimeout()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.timeout;
		}

		public String getVrHelpTitle()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.vrHelpTitle;
		}

		public LayoutMode getInteractionLayout()
		{
			if (@params == null)
				setData();

			return @params.interactionLayout;
		}

		public List<VrHelpItem> getVrHelp()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.vrHelp;
		}

		public List<Choice> getChoiceSet()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.choiceSet;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public PerformInteraction() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.PerformInteraction.ToString();
		}
	}
}