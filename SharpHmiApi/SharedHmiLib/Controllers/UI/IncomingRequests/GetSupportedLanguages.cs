﻿using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.IncomingRequests
{
	public class GetSupportedLanguages : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.UI;
#pragma warning restore 0414

		public GetSupportedLanguages() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.GetSupportedLanguages.ToString();
		}
	}
}