﻿using System;
using HmiApiLib.Common.Structs;
using HmiApiLib.Base;
using System.Collections.Generic;
using HmiApiLib.Types;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.UI.IncomingRequests
{
	public class PerformAudioPassThru : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.UI;
#pragma warning restore 0414

		public class InternalData
		{
			public int? appID;
			public List<TextFieldStruct> audioPassThruDisplayTexts;
			public int? maxDuration;
			public bool? muteAudio;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public List<TextFieldStruct> getAudioPassThruDisplayTexts()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.audioPassThruDisplayTexts;
		}


		public int? getMaxDuration()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.maxDuration;
		}

		public bool? getMuteAudio()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.muteAudio;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public PerformAudioPassThru() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.PerformAudioPassThru.ToString();
		}
	}
}