﻿using System;
using HmiApiLib.Common.Structs;
using HmiApiLib.Base;
using HmiApiLib.Types;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.UI.IncomingRequests
{
	public class SetAppIcon : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.UI;
#pragma warning restore 0414

		public class InternalData
		{
			public int? appID;
			public Image syncFileName;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public Image getAppIcon()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			Image icon = @params.syncFileName;
			return icon;
		}

		public SetAppIcon() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.SetAppIcon.ToString();
		}
	}
}
