﻿using HmiApiLib.Common.Structs;
using HmiApiLib.Base;
using System.Collections.Generic;
using HmiApiLib.Types;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.UI.IncomingRequests
{
	public class ScrollableMessage : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.UI;
#pragma warning restore 0414

		public class InternalData
		{
			public TextFieldStruct messageText;
			public int? timeout;
			public List<SoftButton> softButtons;
			public int? appID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public TextFieldStruct getMessageText()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.messageText;
		}


		public int? getTimeout()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.timeout;
		}

		public List<SoftButton> getSoftButtons()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.softButtons;
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public ScrollableMessage() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.ScrollableMessage.ToString();
		}
	}
}