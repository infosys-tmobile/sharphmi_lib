﻿using HmiApiLib.Common.Structs;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.IncomingRequests
{
	public class SetMediaClockTimer : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.UI;
#pragma warning restore 0414

		public class InternalData
		{
			public TimeFormat startTime;
			public TimeFormat endTime;
			public ClockUpdateMode updateMode;
			public int? appID;
            public bool? enableSeek;
            public AudioStreamingIndicator audioStreamingIndicator;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public TimeFormat getStartTime()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.startTime;
		}


		public TimeFormat getEndTime()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.endTime;
		}

		public ClockUpdateMode getUpdateMode()
		{
			if (@params == null)
				setData();

			return @params.updateMode;
		}

        public AudioStreamingIndicator getAudioStreamingIndicator()
        {
            if (@params == null)
                setData();

            return @params.audioStreamingIndicator;
        }

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

        public bool? getEnableSeek()
        {
            if (@params == null)
                setData();
            if (@params == null)
                return null;

            return @params.enableSeek;
        }

        public SetMediaClockTimer() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.SetMediaClockTimer.ToString();
		}
	}
}