﻿using System;
using HmiApiLib.Common.Structs;
using HmiApiLib.Base;
using HmiApiLib.Types;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.UI.IncomingRequests
{
	public class CloseApplication : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.UI;
#pragma warning restore 0414

		public class InternalData
		{
			public int? appID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public int? getAppId()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.appID;
		}

		public CloseApplication() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.CloseApplication.ToString();
		}
	}
}
