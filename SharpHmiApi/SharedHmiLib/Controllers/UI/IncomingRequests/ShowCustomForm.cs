﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.IncomingRequests
{
	public class ShowCustomForm : RpcRequest
	{
#pragma warning disable 0414
		InterfaceType interfaceType = InterfaceType.UI;
#pragma warning restore 0414

		private static readonly String DEFAULT_PARENT_FORM_ID = "DEFAULT";

		public class InternalData
		{
			public String customFormID;
			public String parentFormID;
		}
		public new InternalData @params = null;

		private void setData()
		{
			string json = Newtonsoft.Json.JsonConvert.SerializeObject(base.@params);
			@params = Newtonsoft.Json.JsonConvert.DeserializeObject<InternalData>(json);
		}

		public String getCustomFormID()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return null;

			return @params.customFormID;
		}

		public String getParentFormID()
		{
			if (@params == null)
				setData();
			if (@params == null)
				return DEFAULT_PARENT_FORM_ID;

			return @params.parentFormID;
		}

		public ShowCustomForm() : base(RpcMessageFlow.INCOMING)
		{
			method = interfaceType.ToString() + "." + FunctionType.ShowCustomForm.ToString();
		}
	}
}