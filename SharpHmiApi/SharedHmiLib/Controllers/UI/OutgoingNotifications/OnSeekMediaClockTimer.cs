﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.OutGoingNotifications
{
	public class OnSeekMediaClockTimer : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.UI;
		public Object @params;

		public class InternalData
		{
            public TimeFormat seekTime;
            public int? appID;
        }

		public void setSeekTime(TimeFormat seekTime)
		{
			((InternalData)@params).seekTime = seekTime;
		}

		public TimeFormat getSeekTime()
		{
			return ((InternalData)@params).seekTime;
		}

        public void setAppId(int? id)
        {
            ((InternalData)@params).appID = id;
        }

        public int? getAppId()
        {
            return ((InternalData)@params).appID;
        }

        public OnSeekMediaClockTimer() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnSeekMediaClockTimer.ToString();
			@params = new InternalData();
		}
	}
}