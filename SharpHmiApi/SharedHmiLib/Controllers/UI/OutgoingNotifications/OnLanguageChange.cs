﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.OutGoingNotifications
{
	public class OnLanguageChange : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.UI;
		public Object @params;

		public class InternalData
		{
			public Language? language;
		}

		public void setLanguage(Language? language)
		{
			((InternalData)@params).language = language;
		}

		public Language? getLanguage()
		{
			return ((InternalData)@params).language;		}

		public OnLanguageChange() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnLanguageChange.ToString();
			@params = new InternalData();
		}
	}
}