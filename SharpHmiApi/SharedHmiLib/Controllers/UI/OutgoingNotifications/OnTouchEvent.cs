﻿using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.OutGoingNotifications
{
	public class OnTouchEvent : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.UI;
		public Object @params;

		public class InternalData
		{
			public TouchType? type;
			public List<TouchEvent> @event;
		}

		public void setTouchType(TouchType? type)
		{
			((InternalData)@params).type = type;
		}

		public void setTouchEvent(List<TouchEvent> _event)
		{
			((InternalData)@params).@event = _event;
		}

		public TouchType? getTouchType()
		{
			return ((InternalData)@params).type;
		}

		public List<TouchEvent> getTouchEvent()
		{
			return ((InternalData)@params).@event;		}

		public OnTouchEvent() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnTouchEvent.ToString();
			@params = new InternalData();
		}
	}
}