﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.OutGoingNotifications
{
	public class OnCommand : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.UI;
		public Object @params;

		public class InternalData
		{
			public int? cmdID;
			public int? appID;
		}

		public void setCmdID(int? cmdID)
		{
			((InternalData)@params).cmdID = cmdID;
		}

		public void setAppId(int? id)
		{
			((InternalData)@params).appID = id;
		}

		public int? getCmdID()
		{
			return ((InternalData)@params).cmdID;
		}

		public int? getAppId()
		{
			return ((InternalData)@params).appID;		}

		public OnCommand() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnCommand.ToString();
			@params = new InternalData();
		}
	}
}