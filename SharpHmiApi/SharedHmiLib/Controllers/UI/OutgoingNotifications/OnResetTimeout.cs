﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.OutGoingNotifications
{
	public class OnResetTimeout : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.UI;
		public Object @params;

		public class InternalData
		{
			public int? appID;
			public String methodName;
		}

		public void setMethodName(String methodName)
		{
			((InternalData)@params).methodName = methodName;
		}

		public void setAppId(int? id)
		{
			((InternalData)@params).appID = id;
		}

		public String getMethodName()
		{
			return ((InternalData)@params).methodName;
		}

		public int? getAppId()
		{
			return ((InternalData)@params).appID;		}

		public OnResetTimeout() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnResetTimeout.ToString();
			@params = new InternalData();
		}
	}
}