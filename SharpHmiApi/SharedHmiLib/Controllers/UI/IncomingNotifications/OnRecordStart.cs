﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.IncomingNotifications
{
	public class OnRecordStart : RequestNotifyMessage
	{
		private InterfaceType interfaceType = InterfaceType.UI;
		public Object @params;

		public class InternalData
		{
			public int? appID;
		}

		public void setAppId(int? id)
		{
			((InternalData)@params).appID = id;
		}

		public int? getAppId()
		{
			return ((InternalData)@params).appID;
		}

		public OnRecordStart() : base(RpcMessageFlow.OUTGOING)
		{
			method = interfaceType.ToString() + "." + FunctionType.OnRecordStart.ToString();
			@params = new InternalData();
		}
	}
}