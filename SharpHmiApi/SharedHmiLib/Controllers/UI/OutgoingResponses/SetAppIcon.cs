﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Controllers.UI.OutgoingResponses
{
	public class SetAppIcon : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.UI;

		public class InternalData : Base.Result
		{

		}

		public SetAppIcon() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.SetAppIcon.ToString();
		}
	}
}
