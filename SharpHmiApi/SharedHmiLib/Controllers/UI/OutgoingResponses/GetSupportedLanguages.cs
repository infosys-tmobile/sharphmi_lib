﻿using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.OutgoingResponses
{
	public class GetSupportedLanguages : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.UI;

		public class InternalData : Base.Result
		{
			public List<Language> languages;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.GetSupportedLanguages.ToString();
		}

		public void setLanguage(List<Language> languages)
		{
			((InternalData)result).languages = languages;
		}

		public List<Language> getLanguages()
		{
			return ((InternalData)result).languages;		}

		public GetSupportedLanguages() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}