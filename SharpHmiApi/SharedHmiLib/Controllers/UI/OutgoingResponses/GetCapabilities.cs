﻿using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Common.Structs;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.OutgoingResponses
{
	public class GetCapabilities : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.UI;

		public class InternalData : Base.Result
		{
			public DisplayCapabilities displayCapabilities;
			public AudioPassThruCapabilities audioPassThruCapabilities;
			public HmiZoneCapabilities hmiZoneCapabilities;
			public List<SoftButtonCapabilities> softButtonCapabilities;
			public HMICapabilities hmiCapabilities;
            public SystemCapabilities systemCapabilities;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}

		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.GetCapabilities.ToString();
		}

		public void setDisplayCapabilities(DisplayCapabilities displayCapabilities)
		{
			((InternalData)result).displayCapabilities = displayCapabilities;
		}

		public void setAudioPassThruCapabilities(AudioPassThruCapabilities audioPassThruCapabilities)
		{
			((InternalData)result).audioPassThruCapabilities = audioPassThruCapabilities;
		}

		public void setHmiZoneCapabilities(HmiZoneCapabilities hmiZoneCapabilities)
		{
			((InternalData)result).hmiZoneCapabilities = hmiZoneCapabilities;
		}

		public void setSoftButtonCapabilities(List<SoftButtonCapabilities> softButtonCapabilities)
		{
			((InternalData)result).softButtonCapabilities = softButtonCapabilities;
		}

		public void setHMICapabilities(HMICapabilities hmiCapabilities)
		{
			((InternalData)result).hmiCapabilities = hmiCapabilities;
		}

        public void setSystemCapabilities(SystemCapabilities systemCapabilities)
        {
            ((InternalData)result).systemCapabilities = systemCapabilities;
        }

		public DisplayCapabilities getDisplayCapabilities()
		{
			return ((InternalData)result).displayCapabilities;
		}

		public AudioPassThruCapabilities getAudioPassThruCapabilities()
		{
			return ((InternalData)result).audioPassThruCapabilities;
		}

		public HmiZoneCapabilities getHmiZoneCapabilities()
		{
			return ((InternalData)result).hmiZoneCapabilities;
		}

		public List<SoftButtonCapabilities> getSoftButtonCapabilities()
		{
			return ((InternalData)result).softButtonCapabilities;
		}

		public HMICapabilities getHMICapabilities()
		{
			return ((InternalData)result).hmiCapabilities;
		}

        public SystemCapabilities getSystemCapabilities()
        {
            return ((InternalData)result).systemCapabilities;
        }

		public GetCapabilities() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}