﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;
using HmiApiLib.Types;

namespace HmiApiLib.Controllers.UI.OutgoingResponses
{
	public class DeleteWindow : RpcResponse
	{
		InterfaceType interfaceType = InterfaceType.UI;

		public class InternalData : Base.Result
		{
			public bool? success;
			public string info;
		}

		public override void setResultCode(Common.Enums.Result res)
		{
			((InternalData)result).code = (int)res;
		}
		public  void setSuccess(bool? res)
		{
			((InternalData)result).success = res;
		}
		public bool? getSuccess()
		{
			return ((InternalData)result).success;
		}
		public  void setInfo(string res)
		{
			((InternalData)result).info = res;
		}
		public string getInfo()
		{
			return ((InternalData)result).info;
		}
		public override void setMethod()
		{
			((InternalData)result).method = interfaceType.ToString() + "." + FunctionType.DeleteWindow.ToString();
		}

		public DeleteWindow() : base(RpcMessageFlow.OUTGOING)
		{
			result = new InternalData();
			setMethod();
		}
	}
}
