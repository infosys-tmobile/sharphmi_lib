﻿namespace HmiApiLib.Types
{
	public enum FunctionType
	{
		OnReady, IsReady, OnButtonSubscription, OnButtonEvent, OnStartDeviceDiscovery, OnTouchEvent,
		OnAppRegistered, OnAppUnregistered, OnVideoDataStreaming, OnButtonPress, OnUpdateDeviceList,
		SetAppIcon, ActivateApp, AddCommand, CreateWindow, AddSubMenu, DeleteSubMenu, PerformAudioPassThru,
		Show, Speak, Alert, OnSystemContext, OnResetTimeout, OnKeyboardInput, OnRecordStart,
		Started, Stopped, StopSpeaking, GetCapabilities, SetMediaClockTimer, EndAudioPassThru,
		MixingAudioSupported, GetLanguage, DeleteCommand, DeleteWindow, Slider, ScrollableMessage, ShowConstantTBT,
		OnAppActivated, OnAppDeactivated, OnExitApplication, OnPhoneCall, OnCommand, OnDriverDistraction,
		OnExitAllApplications, OnEmergencyEvent, PerformInteraction, ClosePopUp, AlertManeuver,
		OnResumeAudioSource, OnSDLPersistenceComplete, OnFileRemoved, OnDeviceChosen, OnFindApplications, OnAwakeSDL,
		OnSDLClose, OnPutFile, OnSystemInfoChanged, OnIgnitionCycleOver, OnDeactivateHMI, OnEventChanged, OnLanguageChange,
		AllowDeviceToConnect, UpdateAppList, UpdateDeviceList, DialNumber, GetSystemInfo, ChangeRegistration,
		GetSupportedLanguages, SetGlobalProperties, SetDisplayLayout, ShowCustomForm, SendLocation,
		UpdateTurnList, StartStream, StopStream, StartAudioStream, StopAudioStream, GetWayPoints,
		SubscribeWayPoints, UnsubscribeWayPoints, GetVehicleType, ReadDID, GetDTCs, DiagnosticMessage,
		SubscribeVehicleData, UnsubscribeVehicleData, GetVehicleData, GetUserFriendlyMessage,
		GetListOfPermissions, UpdateSDL, GetStatusUpdate, GetURLS, PolicyUpdate,
		OnTBTClientState, OnAudioDataStreaming, OnWayPointChange, OnVehicleData, OnAllowSDLFunctionality,
		OnReceivedPolicyUpdate, OnPolicyUpdate, OnAppPermissionConsent, OnAppPermissionChanged, OnDeviceStateChanged,
		OnSDLConsentNeeded, OnStatusUpdate, OnSystemError, AddStatisticsInfo, SystemRequest,
		OnSystemRequest, ButtonPress, SetInteriorVehicleData, GetInteriorVehicleData, GetInteriorVehicleDataConsent, ReleaseInteriorVehicleDataConsent,

		OnInteriorVehicleData, OnRemoteControlSettings, DecryptCertificate, OnRCStatus, SetVideoConfig, GetSystemTime, OnSystemTimeReady,
        OnSeekMediaClockTimer, ShowAppMenu, CloseApplication
    }
}