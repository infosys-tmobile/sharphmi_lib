﻿using System;
namespace HmiApiLib.Types
{
	public enum InterfaceType { UI, BasicCommunication, Buttons, VR, TTS, Navigation, VehicleInfo, Common, SDL, RC }
}
