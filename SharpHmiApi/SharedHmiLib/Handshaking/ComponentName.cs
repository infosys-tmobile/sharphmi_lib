﻿using System;
using HmiApiLib.Types;
namespace HmiApiLib.Handshaking
{
	public class ComponentName
	{
		public string componentName { get; set; }

		public ComponentName()
		{
		}

		public void setComponentName(InterfaceType type)
		{
			componentName = type.ToString();
		}

	}
}
