﻿using System;
using System.Collections;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Handshaking
{
	public class RegisterComponent : RpcRequest
	{
		string REG_METHOD = ComponentPrefix.MB.ToString() + "." + MessageBrokerTypes.registerComponent.ToString();

		public RegisterComponent() : base(RpcMessageFlow.OUTGOING)
		{
			method = REG_METHOD;
			@params = new ComponentName();
		}


		public void setComponent(InterfaceType type)
		{
			((ComponentName)@params).setComponentName(type);
		}
	}
}
