﻿using System;
using HmiApiLib.Types;
using HmiApiLib.Base;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Handshaking
{
	public class OnButtonSubscription : RpcNotification
	{
		string REG_METHOD = ComponentPrefix.MB.ToString() + "." + MessageBrokerTypes.subscribeTo;

		public OnButtonSubscription() : base(RpcMessageFlow.OUTGOING)
		{
			method = REG_METHOD;
			@params = new PropertyName();
		}

		public void setPropertyName(ComponentPrefix prefix, FunctionType type)
		{
			((PropertyName)@params).setPropertyName(prefix, type);
		}

        public void setPropertyName(PropertyName propertyName)
		{
            @params = propertyName;
		}
	}
}
