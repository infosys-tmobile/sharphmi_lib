﻿using HmiApiLib.Manager;
using HmiApiLib.Interfaces;
using HmiApiLib.Builder;
using HmiApiLib.Common.Structs;
using System.Collections.Generic;
using HmiApiLib.Common.Enums;
using HmiApiLib.Base;
using HmiApiLib.Controllers.UI.IncomingRequests;
using HmiApiLib.Controllers.BasicCommunication.IncomingNotifications;
using HmiApiLib.Controllers.BasicCommunication.IncomingRequests;
using HmiApiLib.Controllers.Buttons.IncomingNotifications;
using HmiApiLib.Controllers.Navigation.IncomingRequests;
using HmiApiLib.Controllers.TTS.IncomingRequests;
using HmiApiLib.Controllers.VehicleInfo.IncomingRequests;
using HmiApiLib.Controllers.Navigation.IncomingNotifications;
using HmiApiLib.Controllers.SDL.IncomingNotifications;
using HmiApiLib.Controllers.RC.IncomingRequests;
using HmiApiLib.Controllers.SDL.IncomingResponses;
using HmiApiLib.Controllers.RC.IncomingNotifications;

namespace HmiApiLib.Proxy
{
	public abstract class ProxyHelper : IMessageListener
	{
		ConnectionManager messageManager;
		public Dictionary<int, int> vrGrammerAddCommandDictionary =
			new Dictionary<int, int>();

		public void initConnectionManager(string ipAddr, int portNum, IMessageListener messageListener, IConnectionListener connectionListener, IDispatchingHelper<LogMessage> dispatchingHelper, InitialConnectionCommandConfig initialConnectionCommandConfig)
		{
			messageManager = ConnectionManager.Instance;
			messageManager.setMessageListener(messageListener);
			messageManager.setConnectionListener(connectionListener);
			messageManager.setConsoleLogListener(dispatchingHelper);
			messageManager.setIpAddress(ipAddr);
			messageManager.setPortNumber(portNum);
            messageManager.setInitialConnectionCommandConfig(initialConnectionCommandConfig);
			messageManager.handleSocket();
		}

        public void initConnectionManager(string ipAddr, int portNum, IMessageListener messageListener, IConnectionListener connectionListener, IDispatchingHelper<LogMessage> dispatchingHelper)
        {
            initConnectionManager(ipAddr, portNum, messageListener, connectionListener, dispatchingHelper, null);
        }

        public void sendRpc(RpcMessage rpcMessage)
		{
            if (messageManager == null) return;
                messageManager.sendRpc(rpcMessage);
		}

		//For managing the logging to console from Application/Library as common queue.
		public void handleInputConsoleLogging(LogMessage logMessage)
		{
            if (messageManager == null) return;
                messageManager.handleInputConsoleLog(logMessage);
		}

		public virtual void onVrAddCommandRequest(Controllers.VR.IncomingRequests.AddCommand msg)
		{
			int? cmdId = msg.getCmdId();
			int? grammerId = msg.getGrammarId();

			if ((vrGrammerAddCommandDictionary != null) && (grammerId != null)
				&& (cmdId != null) && (grammerId != -1))
			{
				vrGrammerAddCommandDictionary.Add((int)grammerId, (int)cmdId);
			}
		}

		public int getVrAddCommandId(List<int> grammerId)
		{
			int vrAddCommandId = -1;

			if ((grammerId != null) && (vrGrammerAddCommandDictionary != null))
			{

				foreach (int item in grammerId)
				{
					if (vrGrammerAddCommandDictionary.ContainsKey(item))
					{
						vrAddCommandId = vrGrammerAddCommandDictionary[item];
						break;
					}
				}
			}
			return vrAddCommandId;
		}

		public void addButtonToList(ButtonName name, List<ButtonCapabilities> capabilities)
		{
			ButtonCapabilities button1 = new ButtonCapabilities();
			button1.name = name;
			button1.longPressAvailable = true;
			button1.shortPressAvailable = true;
			button1.upDownAvailable = true;
			capabilities.Add(button1);
		}

        public void onBcActivateAppRequest(Controllers.BasicCommunication.IncomingRequests.ActivateApp msg, Common.Enums.Result? result)
		{
            RpcResponse bcActivateApp = BuildRpc.buildBasicCommunicationActivateAppResponse(msg.getId(), result);
			sendRpc(bcActivateApp);
		}

		private void sendActivateAppRequest(int? appId)
		{
			int nextId = BuildRpc.getNextId();
            RpcRequest activateApp = BuildRpc.buildSdlActivateAppRequest(nextId, appId);
            sendRpc(activateApp);
		}

		//Activate App on a user command
		public void sendOnAppActivatedNotification(int appId,int windowId)
		{
            RequestNotifyMessage onAppActivated = BuildRpc.buildBasicCommunicationOnAppActivated(appId, windowId);
            sendRpc(onAppActivated);
		}

		//De-Activate App on a user command
		public void sendOnAppDeActivatedNotification(int appId,int windowId)
		{
			RequestNotifyMessage onAppDeActivated = BuildRpc.buildBasicCommunicationOnAppDeactivated(appId, windowId);
			sendRpc(onAppDeActivated);
		}

		//Exit App on a user command
		public void OnExitApplicationNotification(ApplicationExitReason applicationExitReason, int appId)
		{
			RequestNotifyMessage onExitAllApplication = BuildRpc.buildBasicCommunicationOnExitApplication(applicationExitReason, appId);
			sendRpc(onExitAllApplication);
		}

		//Exit All App on a user command
		public void OnExitAllApplicationsNotification(ApplicationsCloseReason applicationsCloseReason)
		{
            RequestNotifyMessage onExitAllApplications = BuildRpc.buildBasicCommunicationOnExitAllApplications(applicationsCloseReason);
            sendRpc(onExitAllApplications);
		}

        public string getProxyVersionInfo()
        {
            if (Version.VERSION != null)
                return Version.VERSION;

            return null;
        }

		public abstract void onUiSetAppIconRequest(SetAppIcon msg);
		public abstract void onUiShowRequest(Show msg);
		public abstract void onUiAddCommandRequest(AddCommand msg);
		public abstract void onUiAlertRequest(Alert msg);
		public abstract void onUiPerformInteractionRequest(PerformInteraction msg);
		public abstract void onUiGetLanguageRequest(Controllers.UI.IncomingRequests.GetLanguage msg);
		public abstract void onUiDeleteCommandRequest(DeleteCommand msg);
		public abstract void onUiIsReadyRequest(Controllers.UI.IncomingRequests.IsReady msg);
        public abstract void onUiRecordStartNotification(Controllers.UI.IncomingNotifications.OnRecordStart msg);
        public abstract void onUiAddSubMenuRequest(AddSubMenu msg);
		public abstract void onUiChangeRegistrationRequest(Controllers.UI.IncomingRequests.ChangeRegistration msg);
		public abstract void onUiClosePopUpRequest(ClosePopUp msg);
		public abstract void onUiDeleteSubMenuRequest(DeleteSubMenu msg);
		public abstract void onUiDeleteWindowRequest(DeleteWindow msg);
		public abstract void onUiCreateWindowRequest(CreateWindow msg);

		public abstract void onUiEndAudioPassThruRequest(EndAudioPassThru msg);
		public abstract void onUiGetCapabilitiesRequest(Controllers.UI.IncomingRequests.GetCapabilities msg);
		public abstract void onUiGetSupportedLanguagesRequest(Controllers.UI.IncomingRequests.GetSupportedLanguages msg);
		public abstract void onUiPerformAudioPassThruRequest(PerformAudioPassThru msg);
		public abstract void onUiScrollableMessageRequest(ScrollableMessage msg);
		public abstract void onUiSetDisplayLayoutRequest(SetDisplayLayout msg);
		public abstract void onUiSetGlobalPropertiesRequest(Controllers.UI.IncomingRequests.SetGlobalProperties msg);
		public abstract void onUiSetMediaClockTimerRequest(SetMediaClockTimer msg);
		public abstract void onUiShowCustomFormRequest(ShowCustomForm msg);
		public abstract void onUiSliderRequest(Slider msg);
		public abstract void onTtsSpeakRequest(Speak msg);
		public abstract void onTtsStopSpeakingRequest(StopSpeaking msg);
		public abstract void onTtsGetLanguageRequest(Controllers.TTS.IncomingRequests.GetLanguage msg);
		public abstract void onTtsIsReadyRequest(Controllers.TTS.IncomingRequests.IsReady msg);
		public abstract void onTtsChangeRegistrationRequest(Controllers.TTS.IncomingRequests.ChangeRegistration msg);
		public abstract void onTtsGetCapabilitiesRequest(Controllers.TTS.IncomingRequests.GetCapabilities msg);
		public abstract void onTtsGetSupportedLanguagesRequest(Controllers.TTS.IncomingRequests.GetSupportedLanguages msg);
		public abstract void onTtsSetGlobalPropertiesRequest(Controllers.TTS.IncomingRequests.SetGlobalProperties msg);
		public abstract void onVrGetLanguageRequest(Controllers.VR.IncomingRequests.GetLanguage msg);
		public abstract void onVrDeleteCommandRequest(Controllers.VR.IncomingRequests.DeleteCommand msg);
		public abstract void onVrIsReadyRequest(Controllers.VR.IncomingRequests.IsReady msg);
		public abstract void onVrPerformInteractionRequest(Controllers.VR.IncomingRequests.PerformInteraction msg);
		public abstract void onVrChangeRegistrationRequest(Controllers.VR.IncomingRequests.ChangeRegistration msg);
		public abstract void onVrGetCapabilitiesRequest(Controllers.VR.IncomingRequests.GetCapabilities msg);
		public abstract void onVrGetSupportedLanguagesRequest(Controllers.VR.IncomingRequests.GetSupportedLanguages msg);
		public abstract void onNavIsReadyRequest(Controllers.Navigation.IncomingRequests.IsReady msg);
		public abstract void onNavAlertManeuverRequest(AlertManeuver msg);
		public abstract void onNavGetWayPointsRequest(GetWayPoints msg);
		public abstract void onNavSendLocationRequest(SendLocation msg);
		public abstract void onNavShowConstantTBTRequest(ShowConstantTBT msg);
		public abstract void onNavStartAudioStreamRequest(StartAudioStream msg);
		public abstract void onNavStartStreamRequest(StartStream msg);
		public abstract void onNavStopAudioStreamRequest(StopAudioStream msg);
		public abstract void onNavStopStreamRequest(StopStream msg);
		public abstract void onNavSubscribeWayPointsRequest(SubscribeWayPoints msg);
		public abstract void onNavUnsubscribeWayPointsRequest(UnsubscribeWayPoints msg);
		public abstract void onNavUpdateTurnListRequest(UpdateTurnList msg);
		public abstract void onVehicleInfoIsReadyRequest(Controllers.VehicleInfo.IncomingRequests.IsReady msg);
		public abstract void onVehicleInfoDiagnosticMessageRequest(DiagnosticMessage msg);
		public abstract void onVehicleInfoGetDTCsRequest(GetDTCs msg);
		public abstract void onVehicleInfoGetVehicleDataRequest(GetVehicleData msg);
		public abstract void onVehicleInfoGetVehicleTypeRequest(GetVehicleType msg);
		public abstract void onVehicleInfoReadDIDRequest(ReadDID msg);
		public abstract void onVehicleInfoSubscribeVehicleDataRequest(SubscribeVehicleData msg);
		public abstract void onVehicleInfoUnsubscribeVehicleDataRequest(UnsubscribeVehicleData msg);
		public abstract void onBcAppRegisteredNotification(OnAppRegistered msg);
		public abstract void onBcAppUnRegisteredNotification(OnAppUnregistered msg);
		public abstract void onBcPutfileNotification(OnPutFile msg);
		public abstract void onBcMixingAudioSupportedRequest(MixingAudioSupported msg);
		public abstract void onBcAllowDeviceToConnectRequest(AllowDeviceToConnect msg);
		public abstract void onBcDialNumberRequest(DialNumber msg);
		public abstract void onBcGetSystemInfoRequest(GetSystemInfo msg);
		public abstract void onBcPolicyUpdateRequest(PolicyUpdate msg);
		public abstract void onBcSystemRequestRequest(SystemRequest msg);
		public abstract void onBcUpdateAppListRequest(UpdateAppList msg);
		public abstract void onBcUpdateDeviceListRequest(UpdateDeviceList msg);
        public abstract void onButtonsGetCapabilitiesRequest(Controllers.Buttons.IncomingRequests.GetCapabilities msg, InitialConnectionCommandConfig.ButtonCapabilitiesResponseParam buttonCapabilitiesResponseParam);
        public abstract void OnButtonSubscriptionNotification(OnButtonSubscription msg);
        public abstract void onNavOnAudioDataStreamingNotification(OnAudioDataStreaming msg);
        public abstract void onNavOnVideoDataStreamingNotification(OnVideoDataStreaming msg);
        public abstract void onNavOnWayPointChangeNotification(OnWayPointChange msg);
        public abstract void onBcOnResumeAudioSourceNotification(OnResumeAudioSource msg);
        public abstract void onBcOnSDLPersistenceCompleteNotification(OnSDLPersistenceComplete msg);
        public abstract void onBcOnFileRemovedNotification(OnFileRemoved msg);
        public abstract void onBcOnSDLCloseNotification(OnSDLClose msg);
        public abstract void onBcDecryptCertificateRequest(DecryptCertificate msg);
        public abstract void onSDLActivateAppResponse(Controllers.SDL.IncomingResponses.ActivateApp msg);
        public abstract void OnSDLOnAppPermissionChangedNotification(OnAppPermissionChanged msg);
        public abstract void OnSDLOnSDLConsentNeededNotification(OnSDLConsentNeeded msg);
        public abstract void OnSDLOnStatusUpdateNotification(OnStatusUpdate msg);
        public abstract void OnSDLOnSystemErrorNotification(OnSystemError msg);
        public abstract void OnSDLAddStatisticsInfoNotification(AddStatisticsInfo msg);
        public abstract void OnSDLOnDeviceStateChangedNotification(OnDeviceStateChanged msg);
        public abstract void onRcGetCapabilitiesRequest(Controllers.RC.IncomingRequests.GetCapabilities msg);
        public abstract void onRcGetInteriorVehicleDataRequest(GetInteriorVehicleData msg);
        public abstract void onRcGetInteriorVehicleDataConsentRequest(GetInteriorVehicleDataConsent msg);
		public abstract void onRcReleaseInteriorVehicleDataConsentRequest(ReleaseInteriorVehicleDataConsent msg);
		public abstract void onRcIsReadyRequest(Controllers.RC.IncomingRequests.IsReady msg);
        public abstract void onRcSetInteriorVehicleDataRequest(SetInteriorVehicleData msg);
        public abstract void onButtonsButtonPressRequest(Controllers.Buttons.IncomingRequests.ButtonPress msg);
        public abstract void onSDLGetListOfPermissionsResponse(GetListOfPermissions msg);
        public abstract void onSDLGetStatusUpdateResponse(GetStatusUpdate msg);
        public abstract void onSDLGetURLSResponse(GetURLS msg);
        public abstract void onSDLGetUserFriendlyMessageResponse(GetUserFriendlyMessage msg);
        public abstract void onSDLUpdateSDLResponse(UpdateSDL msg);
        public abstract void onRcOnRCStatusNotification(OnRCStatus msg);
        public abstract void onNavSetVideoConfigRequest(SetVideoConfig msg);
        public abstract void onBcGetSystemTimeRequest(GetSystemTime msg);
        public abstract void onUiShowAppMenuRequest(ShowAppMenu msg);
        public abstract void onUiCloseApplicationRequest(CloseApplication msg);
    }
}
