﻿using System;
using HmiApiLib.Base;
using HmiApiLib.Types;
using HmiApiLib.Manager;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HmiApiLib
{
    public class Utils
    {
		private const string methodParam = "method";
		private const string resultParam = "result";

        public static string getMethodName(string inputJson)
        {
            string method = null;

			if (inputJson == null) return method;

			JObject level1 = null;
			JObject level2 = null;

			try
			{
				level1 = JObject.Parse(inputJson);
			}
#pragma warning disable CS0168 // Variable is declared but never used
#pragma warning disable RECS0022 // A catch clause that catches System.Exception and has an empty body
			catch (Exception ex)
#pragma warning restore RECS0022 // A catch clause that catches System.Exception and has an empty body
#pragma warning restore CS0168 // Variable is declared but never used
			{
			}

			if ((level1 != null) && (level1[methodParam] != null))
			{
				method = level1[methodParam].ToString();
			}
			else
			{
				//Neither Incoming Request / Notification, lets check if its Incoming Response

				if ((level1 != null) && (level1[resultParam] != null))
				{
					try
					{
						level2 = JObject.Parse(level1[resultParam].ToString());
					}
#pragma warning disable CS0168 // Variable is declared but never used
#pragma warning disable RECS0022 // A catch clause that catches System.Exception and has an empty body
					catch (Exception ex)
#pragma warning restore RECS0022 // A catch clause that catches System.Exception and has an empty body
#pragma warning restore CS0168 // Variable is declared but never used
					{

					}
				}

				if ((level2 != null) && (level2[methodParam] != null))
				{
					method = level2[methodParam].ToString();
				}
			}

            return method;

		}

        public static string getSerializedRpcMessage(RpcMessage inputRpcMessage)
        {
            if (inputRpcMessage == null) return null;

			string rawJSON = null;
            string methodName = "";

			if (inputRpcMessage is RpcRequest)
			{
				methodName = ((RpcRequest)inputRpcMessage).method;
			}
			else if (inputRpcMessage is RpcResponse)
			{
				methodName = ((RpcResponse)inputRpcMessage).getMethod();
			}
			else if (inputRpcMessage is RpcNotification)
			{
				methodName = ((RpcNotification)inputRpcMessage).method;
			}
			else if (inputRpcMessage is RequestNotifyMessage)
			{
				methodName = ((RequestNotifyMessage)inputRpcMessage).method;
			}

			try
			{
				rawJSON = JsonConvert.SerializeObject(inputRpcMessage, Formatting.Indented, new JsonSerializerSettings
				{
					NullValueHandling = NullValueHandling.Ignore
				});
			}

#pragma warning disable CS0168 // Variable is declared but never used
			catch (Exception ex)
#pragma warning restore CS0168 // Variable is declared but never used
			{
				try
				{
					rawJSON = methodName +
						" (" + inputRpcMessage.getRpcMessageFlow().ToString().ToLower() + " " + inputRpcMessage.getRpcMessageType().ToString().ToLower() + ")";
				}

#pragma warning disable CS0168 // Variable is declared but never used
				catch (Exception e1)
#pragma warning restore CS0168 // Variable is declared but never used
				{
					rawJSON = "Undefined";
				}
			}

            return rawJSON;
		}

        public static RpcMessage getDeserializedRpcMessage(string inputJson)
		{
            RpcMessage rpcMessage = null;
			string method = null;
			StringLogMessage myStringLog = null;
			
            method = getMethodName(inputJson);

			if (method == null)
			{
				myStringLog = new StringLogMessage(inputJson);
                ConnectionManager.Instance.handleInputConsoleLog(myStringLog);
				return null;
			}

			try
			{
                if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnAppActivated))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutGoingNotifications.OnAppActivated>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnAppDeactivated))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutGoingNotifications.OnAppDeactivated>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnAwakeSDL))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutGoingNotifications.OnAwakeSDL>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnDeactivateHMI))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutGoingNotifications.OnDeactivateHMI>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnDeviceChosen))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutGoingNotifications.OnDeviceChosen>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnEmergencyEvent))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutGoingNotifications.OnEmergencyEvent>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnEventChanged))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutGoingNotifications.OnEventChanged>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnExitAllApplications))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutGoingNotifications.OnExitAllApplications>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnExitApplication))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutGoingNotifications.OnExitApplication>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnFindApplications))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutGoingNotifications.OnFindApplications>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnIgnitionCycleOver))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutGoingNotifications.OnIgnitionCycleOver>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnPhoneCall))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutGoingNotifications.OnPhoneCall>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnReady))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutGoingNotifications.OnReady>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnStartDeviceDiscovery))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutGoingNotifications.OnStartDeviceDiscovery>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnSystemInfoChanged))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutGoingNotifications.OnSystemInfoChanged>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnUpdateDeviceList))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutGoingNotifications.OnUpdateDeviceList>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.OnSystemTimeReady))
                {
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutGoingNotifications.OnSystemTimeReady>(inputJson);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.ActivateApp))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutgoingResponses.ActivateApp>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.AllowDeviceToConnect))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.DecryptCertificate))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutgoingResponses.DecryptCertificate>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.DialNumber))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutgoingResponses.DialNumber>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.GetSystemInfo))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.GetSystemTime))
                {
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutgoingResponses.GetSystemTime>(inputJson);
                }
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.MixingAudioSupported))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.PolicyUpdate))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.SystemRequest))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutgoingResponses.SystemRequest>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.UpdateAppList))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutgoingResponses.UpdateAppList>(inputJson);
				}
                else if (method.Equals(InterfaceType.BasicCommunication + "." + FunctionType.UpdateDeviceList))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList>(inputJson);
				}
                else if (method.Equals(InterfaceType.Buttons + "." + FunctionType.OnButtonEvent))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.Buttons.OutGoingNotifications.OnButtonEvent>(inputJson);
				}
                else if (method.Equals(InterfaceType.Buttons + "." + FunctionType.OnButtonPress))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.Buttons.OutGoingNotifications.OnButtonPress>(inputJson);
				}
                else if (method.Equals(InterfaceType.Buttons + "." + FunctionType.ButtonPress))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.Buttons.OutgoingResponses.ButtonPress>(inputJson);
				}
                else if (method.Equals(InterfaceType.Buttons + "." + FunctionType.GetCapabilities))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.Buttons.OutgoingResponses.GetCapabilities>(inputJson);
				}
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.OnTBTClientState))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.Navigation.OutGoingNotifications.OnTBTClientState>(inputJson);
				}
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.AlertManeuver))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.Navigation.OutgoingResponses.AlertManeuver>(inputJson);
				}
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.GetWayPoints))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.Navigation.OutgoingResponses.GetWayPoints>(inputJson);
				}
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.IsReady))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.Navigation.OutgoingResponses.IsReady>(inputJson);
				}
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.SendLocation))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.Navigation.OutgoingResponses.SendLocation>(inputJson);
				}
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.ShowConstantTBT))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.Navigation.OutgoingResponses.ShowConstantTBT>(inputJson);
				}
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.StartAudioStream))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.Navigation.OutgoingResponses.StartAudioStream>(inputJson);
				}
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.StartStream))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.Navigation.OutgoingResponses.StartStream>(inputJson);
				}
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.StopAudioStream))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.Navigation.OutgoingResponses.StopAudioStream>(inputJson);
				}
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.StopStream))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.Navigation.OutgoingResponses.StopStream>(inputJson);
				}
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.SubscribeWayPoints))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.Navigation.OutgoingResponses.SubscribeWayPoints>(inputJson);
				}
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.UnsubscribeWayPoints))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints>(inputJson);
				}
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.UpdateTurnList))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.Navigation.OutgoingResponses.UpdateTurnList>(inputJson);
				}
                else if (method.Equals(InterfaceType.Navigation + "." + FunctionType.SetVideoConfig))
                {
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.Navigation.OutgoingResponses.SetVideoConfig>(inputJson);
                }
                else if (method.Equals(InterfaceType.RC + "." + FunctionType.OnInteriorVehicleData))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.RC.OutGoingNotifications.OnInteriorVehicleData>(inputJson);
				}
                else if (method.Equals(InterfaceType.RC + "." + FunctionType.OnRemoteControlSettings))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.RC.OutGoingNotifications.OnRemoteControlSettings>(inputJson);
				}
                else if (method.Equals(InterfaceType.RC + "." + FunctionType.GetCapabilities))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.RC.OutgoingResponses.GetCapabilities>(inputJson);
				}
                else if (method.Equals(InterfaceType.RC + "." + FunctionType.GetInteriorVehicleData))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.RC.OutgoingResponses.GetInteriorVehicleData>(inputJson);
				}
                else if (method.Equals(InterfaceType.RC + "." + FunctionType.GetInteriorVehicleDataConsent))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent>(inputJson);
				}
                else if (method.Equals(InterfaceType.RC + "." + FunctionType.IsReady))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.RC.OutgoingResponses.IsReady>(inputJson);
				}
                else if (method.Equals(InterfaceType.RC + "." + FunctionType.SetInteriorVehicleData))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.RC.OutgoingResponses.SetInteriorVehicleData>(inputJson);
				}
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.OnAllowSDLFunctionality))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.SDL.OutGoingNotifications.OnAllowSDLFunctionality>(inputJson);
				}
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.OnAppPermissionConsent))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.SDL.OutGoingNotifications.OnAppPermissionConsent>(inputJson);
				}
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.OnPolicyUpdate))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.SDL.OutGoingNotifications.OnPolicyUpdate>(inputJson);
				}
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.OnReceivedPolicyUpdate))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.SDL.OutGoingNotifications.OnReceivedPolicyUpdate>(inputJson);
				}
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.ActivateApp))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.SDL.OutgoingRequests.ActivateApp>(inputJson);
				}
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.GetListOfPermissions))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.SDL.OutgoingRequests.GetListOfPermissions>(inputJson);
				}
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.GetStatusUpdate))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.SDL.OutgoingRequests.GetStatusUpdate>(inputJson);
				}
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.GetURLS))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.SDL.OutgoingRequests.GetURLS>(inputJson);
				}
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.GetUserFriendlyMessage))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.SDL.OutgoingRequests.GetUserFriendlyMessage>(inputJson);
				}
                else if (method.Equals(InterfaceType.SDL + "." + FunctionType.UpdateSDL))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.SDL.OutgoingRequests.UpdateSDL>(inputJson);
				}
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.OnLanguageChange))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.TTS.OutGoingNotifications.OnLanguageChange>(inputJson);
				}
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.OnResetTimeout))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.TTS.OutGoingNotifications.OnResetTimeout>(inputJson);
				}
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.Started))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.TTS.OutGoingNotifications.Started>(inputJson);
				}
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.Stopped))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.TTS.OutGoingNotifications.Stopped>(inputJson);
				}
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.ChangeRegistration))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.TTS.OutgoingResponses.ChangeRegistration>(inputJson);
				}
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.GetCapabilities))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.TTS.OutgoingResponses.GetCapabilities>(inputJson);
				}
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.GetLanguage))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.TTS.OutgoingResponses.GetLanguage>(inputJson);
				}
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.GetSupportedLanguages))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.TTS.OutgoingResponses.GetSupportedLanguages>(inputJson);
				}
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.IsReady))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.TTS.OutgoingResponses.IsReady>(inputJson);
				}
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.SetGlobalProperties))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.TTS.OutgoingResponses.SetGlobalProperties>(inputJson);
				}
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.Speak))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.TTS.OutgoingResponses.Speak>(inputJson);
				}
                else if (method.Equals(InterfaceType.TTS + "." + FunctionType.StopSpeaking))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.TTS.OutgoingResponses.StopSpeaking>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.OnCommand))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutGoingNotifications.OnCommand>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.OnDriverDistraction))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutGoingNotifications.OnDriverDistraction>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.OnSeekMediaClockTimer))
                {
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutGoingNotifications.OnSeekMediaClockTimer>(inputJson);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.OnKeyboardInput))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutGoingNotifications.OnKeyboardInput>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.OnLanguageChange))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutGoingNotifications.OnLanguageChange>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.OnRecordStart))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.IncomingNotifications.OnRecordStart>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.OnResetTimeout))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutGoingNotifications.OnResetTimeout>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.OnSystemContext))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutGoingNotifications.OnSystemContext>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.OnTouchEvent))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutGoingNotifications.OnTouchEvent>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.AddCommand))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.AddCommand>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.AddSubMenu))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.AddSubMenu>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.ShowAppMenu))
                {
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.ShowAppMenu>(inputJson);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.CloseApplication))
                {
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.CloseApplication>(inputJson);
                }
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.Alert))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.Alert>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.ChangeRegistration))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.ChangeRegistration>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.ClosePopUp))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.ClosePopUp>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.DeleteCommand))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.DeleteCommand>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.DeleteSubMenu))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.DeleteSubMenu>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.EndAudioPassThru))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.EndAudioPassThru>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.GetCapabilities))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.GetCapabilities>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.GetLanguage))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.GetLanguage>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.GetSupportedLanguages))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.GetSupportedLanguages>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.IsReady))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.IsReady>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.PerformAudioPassThru))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.PerformAudioPassThru>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.PerformInteraction))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.PerformInteraction>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.ScrollableMessage))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.ScrollableMessage>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.SetAppIcon))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.SetAppIcon>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.SetDisplayLayout))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.SetDisplayLayout>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.SetGlobalProperties))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.SetGlobalProperties>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.SetMediaClockTimer))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.SetMediaClockTimer>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.Show))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.Show>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.ShowCustomForm))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.ShowCustomForm>(inputJson);
				}
                else if (method.Equals(InterfaceType.UI + "." + FunctionType.Slider))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.UI.OutgoingResponses.Slider>(inputJson);
				}
                else if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.OnVehicleData))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.VehicleInfo.OutGoingNotifications.OnVehicleData>(inputJson);
				}
                else if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.DiagnosticMessage))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage>(inputJson);
				}
                else if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.GetDTCs))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.VehicleInfo.OutgoingResponses.GetDTCs>(inputJson);
				}
                else if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.GetVehicleData))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.VehicleInfo.OutgoingResponses.GetVehicleData>(inputJson);
				}
                else if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.GetVehicleType))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.VehicleInfo.OutgoingResponses.GetVehicleType>(inputJson);
				}
                else if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.IsReady))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.VehicleInfo.OutgoingResponses.IsReady>(inputJson);
				}
                else if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.ReadDID))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.VehicleInfo.OutgoingResponses.ReadDID>(inputJson);
				}
                else if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.SubscribeVehicleData))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData>(inputJson);
				}
                else if (method.Equals(InterfaceType.VehicleInfo + "." + FunctionType.UnsubscribeVehicleData))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData>(inputJson);
				}
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.OnCommand))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.VR.OutGoingNotifications.OnCommand>(inputJson);
				}
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.OnLanguageChange))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.VR.OutGoingNotifications.OnLanguageChange>(inputJson);
				}
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.Started))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.VR.OutGoingNotifications.Started>(inputJson);
				}
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.Stopped))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.VR.OutGoingNotifications.Stopped>(inputJson);
				}
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.AddCommand))
				{
                    rpcMessage = JsonConvert.DeserializeObject<Controllers.VR.OutgoingResponses.AddCommand>(inputJson);
				}
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.ChangeRegistration))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.VR.OutgoingResponses.ChangeRegistration>(inputJson);
				}
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.DeleteCommand))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.VR.OutgoingResponses.DeleteCommand>(inputJson);
				}
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.GetCapabilities))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.VR.OutgoingResponses.GetCapabilities>(inputJson);
				}
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.GetLanguage))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.VR.OutgoingResponses.GetLanguage>(inputJson);
				}
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.GetSupportedLanguages))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.VR.OutgoingResponses.GetSupportedLanguages>(inputJson);
				}
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.IsReady))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.VR.OutgoingResponses.IsReady>(inputJson);
				}
                else if (method.Equals(InterfaceType.VR + "." + FunctionType.PerformInteraction))
				{
					rpcMessage = JsonConvert.DeserializeObject<Controllers.VR.OutgoingResponses.PerformInteraction>(inputJson);
				}
				else
				{
					myStringLog = new StringLogMessage("Method not recognized !!!");
                    myStringLog.setData(inputJson);
					ConnectionManager.Instance.handleInputConsoleLog(myStringLog);
				}
			}
			catch (Exception ex)
			{
				myStringLog = new StringLogMessage("Incoming: could not deserialize, json = " + inputJson);
				myStringLog.setData("Exception = " + ex);
				ConnectionManager.Instance.handleInputConsoleLog(myStringLog);
			}

            return rpcMessage;
		}

        public static Object getEnumValue(Type enumType, string inputString)
        {
            Object enumValue = null;

            try{
                enumValue = Enum.Parse(enumType, inputString);             
            }
#pragma warning disable CS0168 // Variable is declared but never used
            catch (Exception ex)
            {
#pragma warning restore CS0168 // Variable is declared but never used
            }

            return enumValue;
        }
    }
}
