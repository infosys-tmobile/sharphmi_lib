﻿using System;
namespace HmiApiLib
{
	public class LogMessage
	{

		private string date;
		private string sPrependComment = "";

		public LogMessage()
		{
			date = DateTime.Now.ToString("HH:mm:ss tt");
		}

		public string getDate()
		{
			return date;
		}

		public void setDate(string date)
		{
			this.date = date;
		}

		public string getPrependComment()
		{
			return sPrependComment;
		}

		public void setPrependComment(string sPrependComment)
		{
			this.sPrependComment = sPrependComment;
		}
	}
}
