﻿using System;
using System.Collections.Generic;
using HmiApiLib.Types;
using HmiApiLib.Handshaking;
using HmiApiLib.Common.Structs;

namespace HmiApiLib
{
	public class InitialConnectionCommandConfig
	{

        public List<InterfaceType> registerComponents = null;
        public Boolean isBcOnReady = false;
        public Boolean isBcOnSystemTimeReady = false;
        public List<PropertyName> subscribeNotifications = null;
        public ButtonCapabilitiesResponseParam buttonCapabilitiesResponseParam = null;
        public Common.Enums.Result? activateAppResultCode = null;

		public InitialConnectionCommandConfig()
		{
		}

		public List<InterfaceType> getRegisterComponents()
		{
			return this.registerComponents;
		}

		public void setRegisterComponents(List<InterfaceType> registerComponents)
		{
			this.registerComponents = registerComponents;
		}

        public List<PropertyName> getSubscribeNotifications()
		{
			return this.subscribeNotifications;
		}

		public void setSubscribeNotifications(List<PropertyName> subscribeNotifications)
		{
			this.subscribeNotifications = subscribeNotifications;
		}

		public Boolean getIsBcOnReady()
		{
			return this.isBcOnReady;
		}

		public void setIsBcOnReady(Boolean isBcOnReady)
		{
			this.isBcOnReady = isBcOnReady;
		}

        public Boolean getIsBcOnSystemTimeReady()
        {
            return this.isBcOnSystemTimeReady;
        }

        public void setIsBcOnSystemTimeReady(Boolean isBcOnSystemTimeReady)
        {
            this.isBcOnSystemTimeReady = isBcOnSystemTimeReady;
        }

		public Common.Enums.Result? getActivateAppResultCode()
		{
			return this.activateAppResultCode;
		}

		public void setActivateAppResultCode(Common.Enums.Result? activateAppResultCode)
		{
			this.activateAppResultCode = activateAppResultCode;
		}

		public ButtonCapabilitiesResponseParam getButtonsCapabilitiesResponseParam()
		{
			return this.buttonCapabilitiesResponseParam;
		}

		public void setButtonsCapabilitiesResponseParam(ButtonCapabilitiesResponseParam buttonCapabilitiesResponseParam)
		{
			this.buttonCapabilitiesResponseParam = buttonCapabilitiesResponseParam;
		}

        public class ButtonCapabilitiesResponseParam{
            public List<ButtonCapabilities> capabilities;
            public PresetBankCapabilities presetBankCapabilities;
            public Common.Enums.Result? result;

			public List<ButtonCapabilities> getCapabilities()
			{
				return this.capabilities;
			}

			public void setCapabilities(List<ButtonCapabilities> capabilities)
			{
				this.capabilities = capabilities;
			}

			public PresetBankCapabilities getPresetBankCapabilities()
			{
				return this.presetBankCapabilities;
			}

			public void setPresetBankCapabilities(PresetBankCapabilities presetBankCapabilities)
			{
				this.presetBankCapabilities = presetBankCapabilities;
			}

			public Common.Enums.Result? getResult()
			{
                return this.result;
			}

			public void setResult(Common.Enums.Result? result)
			{
				this.result = result;
			}
        }
	}
}
