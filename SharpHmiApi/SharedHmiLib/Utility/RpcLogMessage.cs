﻿using System;
using HmiApiLib.Base;

namespace HmiApiLib
{
	public class RpcLogMessage : LogMessage
	{

		private RpcMessage message;

		public RpcLogMessage(RpcMessage rpc)
		{
			message = rpc;
		}

		public RpcMessage getMessage()
		{
			return message;
		}

		public void setMessage(RpcMessage message)
		{
			this.message = message;
		}
	}
}