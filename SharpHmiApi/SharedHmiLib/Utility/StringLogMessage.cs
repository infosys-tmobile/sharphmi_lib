﻿using System;
namespace HmiApiLib
{
	public class StringLogMessage : LogMessage
	{

		private string message;
		private string sData = "";

		public StringLogMessage(string message)
		{
			this.message = message;
		}

		public string getMessage()
		{
			return message;
		}

		public void setMessage(string message)
		{
			this.message = message;
		}

		public void setData(string theData)
		{
			this.sData = theData;
		}

		public string getData()
		{
			return sData;
		}
	}
}