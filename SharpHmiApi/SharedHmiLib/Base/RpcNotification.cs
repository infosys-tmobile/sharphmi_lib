﻿using System;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Base
{
	public class RpcNotification : RpcMessage
	{
		public int id = -1;
		public Object @params;
		public string method;

		public RpcNotification(RpcMessageFlow rpcMessageFlow) : base(rpcMessageFlow, RpcMessageType.NOTIFICATION)
		{
		}

		public void setId(int id)
		{
			this.id = id;
		}

		public int getId()
		{
			return id;
		}

		public Object getParams()
		{
			return @params;
		}

		public string getMethod()
		{
			return method;
		}
	}
}
