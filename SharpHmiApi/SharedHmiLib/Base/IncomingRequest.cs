﻿using System;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Base
{
	public class IncomingRequest : RpcRequest
	{
		public IncomingRequest() : base(RpcMessageFlow.INCOMING)
		{
		}
	}
}
