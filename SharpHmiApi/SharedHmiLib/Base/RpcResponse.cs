﻿using System;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Base
{
	public abstract class RpcResponse : RpcMessage
	{
		public int? id;
		public Object result;

		public RpcResponse(RpcMessageFlow rpcMessageFlow) : base(rpcMessageFlow, RpcMessageType.RESPONSE)
		{

		}

		public void setId(int? id)
		{
			this.id = id;
		}

		public int? getId()
		{
			return this.id;
		}

		public virtual Common.Enums.Result getResultCode()
		{
			return (Common.Enums.Result)((Result)result).code;
		}

        public virtual string getMethod()
		{
			return ((Result)result).method;
		}

		public abstract void setResultCode(Common.Enums.Result res);

		public abstract void setMethod();
	}
}
