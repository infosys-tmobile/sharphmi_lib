﻿using HmiApiLib.Common.Enums;

namespace HmiApiLib.Base
{
    class IncomingResponse : RpcResponse
    {
        public class InternalData : Base.Result
        {
        }

        public IncomingResponse() : base(RpcMessageFlow.INCOMING)
		{
            result = new InternalData();
        }

        public override void setMethod()
        {
        }

        public override void setResultCode(Common.Enums.Result res)
        {
        }
    }
}
