﻿using System;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Base
{
	public abstract class RpcRequest : RpcMessage
	{
		public int id = -1;
		public string method;

		public Object @params;

		public RpcRequest(RpcMessageFlow rpcMessageFlow) : base(rpcMessageFlow, RpcMessageType.REQUEST)
		{
		}

		public void setId(int id)
		{
			this.id = id;
		}

		public int getId()
		{
			return id;
		}

		public Object getParams()
		{
			return @params;
		}

		public string getMethod()
		{
			return method;
		}
	}
}
