﻿using System;
using HmiApiLib.Common.Enums;

namespace HmiApiLib.Base
{
	public abstract class RequestNotifyMessage : RpcMessage
	{
		public string method;

		public RequestNotifyMessage(RpcMessageFlow rpcMessageFlow) : base(rpcMessageFlow, RpcMessageType.REQUEST_NOTIFY)
		{
		}

		public string getMethod()
		{
			return method;
		}
	}
}
