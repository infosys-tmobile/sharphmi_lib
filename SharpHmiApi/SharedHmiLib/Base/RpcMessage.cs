﻿using System;
using HmiApiLib.Common.Enums;
using Newtonsoft.Json;

namespace HmiApiLib.Base
{
	public abstract class RpcMessage : RpcStruct
	{
        public string jsonrpc = "2.0";

        [JsonIgnore]
		public RpcMessageType rpcMessageType;
		[JsonIgnore]
		public RpcMessageFlow rpcMessageFlow;

		public RpcMessage(RpcMessageFlow rpcMessageFlow, RpcMessageType rpcMessageType)
		{
            this.rpcMessageFlow = rpcMessageFlow;
            this.rpcMessageType = rpcMessageType;
		}

		public RpcMessageType getRpcMessageType()
		{
			return this.rpcMessageType;
		}

		public RpcMessageFlow getRpcMessageFlow()
		{
			return this.rpcMessageFlow;
		}
	}
}