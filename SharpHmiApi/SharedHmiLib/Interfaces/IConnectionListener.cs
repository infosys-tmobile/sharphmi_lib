﻿using System;
namespace HmiApiLib.Interfaces
{
	public interface IConnectionListener
	{
		//Web Socket connection listener interface callbacks
		void onOpen();
		void onClose();
		void onError();
	}
}
