﻿namespace HmiApiLib.Interfaces
{
	public interface IMessageListener
	{
		//UI interface incoming callbacks
		void onUiSetAppIconRequest(Controllers.UI.IncomingRequests.SetAppIcon msg);
		void onUiShowRequest(Controllers.UI.IncomingRequests.Show msg);
		void onUiAddCommandRequest(Controllers.UI.IncomingRequests.AddCommand msg);
		void onUiAlertRequest(Controllers.UI.IncomingRequests.Alert msg);
		void onUiPerformInteractionRequest(Controllers.UI.IncomingRequests.PerformInteraction msg);
		void onUiGetLanguageRequest(Controllers.UI.IncomingRequests.GetLanguage msg);
		void onUiDeleteCommandRequest(Controllers.UI.IncomingRequests.DeleteCommand msg);
		void onUiCreateWindowRequest(Controllers.UI.IncomingRequests.CreateWindow msg);
		void onUiDeleteWindowRequest(Controllers.UI.IncomingRequests.DeleteWindow msg);
		void onUiIsReadyRequest(Controllers.UI.IncomingRequests.IsReady msg);
        void onUiRecordStartNotification(Controllers.UI.IncomingNotifications.OnRecordStart msg);
        void onUiAddSubMenuRequest(Controllers.UI.IncomingRequests.AddSubMenu msg);
		void onUiChangeRegistrationRequest(Controllers.UI.IncomingRequests.ChangeRegistration msg);
		void onUiClosePopUpRequest(Controllers.UI.IncomingRequests.ClosePopUp msg);
		void onUiDeleteSubMenuRequest(Controllers.UI.IncomingRequests.DeleteSubMenu msg);
		void onUiEndAudioPassThruRequest(Controllers.UI.IncomingRequests.EndAudioPassThru msg);
		void onUiGetCapabilitiesRequest(Controllers.UI.IncomingRequests.GetCapabilities msg);
		void onUiGetSupportedLanguagesRequest(Controllers.UI.IncomingRequests.GetSupportedLanguages msg);
		void onUiPerformAudioPassThruRequest(Controllers.UI.IncomingRequests.PerformAudioPassThru msg);
		void onUiScrollableMessageRequest(Controllers.UI.IncomingRequests.ScrollableMessage msg);
		void onUiSetDisplayLayoutRequest(Controllers.UI.IncomingRequests.SetDisplayLayout msg);
		void onUiSetGlobalPropertiesRequest(Controllers.UI.IncomingRequests.SetGlobalProperties msg);
		void onUiSetMediaClockTimerRequest(Controllers.UI.IncomingRequests.SetMediaClockTimer msg);
		void onUiShowCustomFormRequest(Controllers.UI.IncomingRequests.ShowCustomForm msg);
		void onUiSliderRequest(Controllers.UI.IncomingRequests.Slider msg);
        void onUiShowAppMenuRequest(Controllers.UI.IncomingRequests.ShowAppMenu msg);
        void onUiCloseApplicationRequest(Controllers.UI.IncomingRequests.CloseApplication msg);

        //TTS interface incoming callbacks
        void onTtsSpeakRequest(Controllers.TTS.IncomingRequests.Speak msg);
		void onTtsStopSpeakingRequest(Controllers.TTS.IncomingRequests.StopSpeaking msg);
		void onTtsGetLanguageRequest(Controllers.TTS.IncomingRequests.GetLanguage msg);
		void onTtsIsReadyRequest(Controllers.TTS.IncomingRequests.IsReady msg);
		void onTtsChangeRegistrationRequest(Controllers.TTS.IncomingRequests.ChangeRegistration msg);
		void onTtsGetCapabilitiesRequest(Controllers.TTS.IncomingRequests.GetCapabilities msg);
		void onTtsGetSupportedLanguagesRequest(Controllers.TTS.IncomingRequests.GetSupportedLanguages msg);
		void onTtsSetGlobalPropertiesRequest(Controllers.TTS.IncomingRequests.SetGlobalProperties msg);

		//VR interface incoming callbacks
		void onVrAddCommandRequest(Controllers.VR.IncomingRequests.AddCommand msg);
		void onVrGetLanguageRequest(Controllers.VR.IncomingRequests.GetLanguage msg);
		void onVrDeleteCommandRequest(Controllers.VR.IncomingRequests.DeleteCommand msg);
		void onVrIsReadyRequest(Controllers.VR.IncomingRequests.IsReady msg);
		void onVrPerformInteractionRequest(Controllers.VR.IncomingRequests.PerformInteraction msg);
		void onVrChangeRegistrationRequest(Controllers.VR.IncomingRequests.ChangeRegistration msg);
		void onVrGetCapabilitiesRequest(Controllers.VR.IncomingRequests.GetCapabilities msg);
		void onVrGetSupportedLanguagesRequest(Controllers.VR.IncomingRequests.GetSupportedLanguages msg);

		//Navigation interface incoming callbacks
		void onNavIsReadyRequest(Controllers.Navigation.IncomingRequests.IsReady msg);
		void onNavAlertManeuverRequest(Controllers.Navigation.IncomingRequests.AlertManeuver msg);
		void onNavGetWayPointsRequest(Controllers.Navigation.IncomingRequests.GetWayPoints msg);
		void onNavSendLocationRequest(Controllers.Navigation.IncomingRequests.SendLocation msg);
		void onNavShowConstantTBTRequest(Controllers.Navigation.IncomingRequests.ShowConstantTBT msg);
		void onNavStartAudioStreamRequest(Controllers.Navigation.IncomingRequests.StartAudioStream msg);
		void onNavStartStreamRequest(Controllers.Navigation.IncomingRequests.StartStream msg);
		void onNavStopAudioStreamRequest(Controllers.Navigation.IncomingRequests.StopAudioStream msg);
		void onNavStopStreamRequest(Controllers.Navigation.IncomingRequests.StopStream msg);
		void onNavSubscribeWayPointsRequest(Controllers.Navigation.IncomingRequests.SubscribeWayPoints msg);
		void onNavUnsubscribeWayPointsRequest(Controllers.Navigation.IncomingRequests.UnsubscribeWayPoints msg);
		void onNavUpdateTurnListRequest(Controllers.Navigation.IncomingRequests.UpdateTurnList msg);
        void onNavOnAudioDataStreamingNotification(Controllers.Navigation.IncomingNotifications.OnAudioDataStreaming msg);
        void onNavOnVideoDataStreamingNotification(Controllers.Navigation.IncomingNotifications.OnVideoDataStreaming msg);
		void onNavOnWayPointChangeNotification(Controllers.Navigation.IncomingNotifications.OnWayPointChange msg);
        void onNavSetVideoConfigRequest(Controllers.Navigation.IncomingRequests.SetVideoConfig msg);

		//VehicleInfo interface incoming callbacks
		void onVehicleInfoIsReadyRequest(Controllers.VehicleInfo.IncomingRequests.IsReady msg);
		void onVehicleInfoDiagnosticMessageRequest(Controllers.VehicleInfo.IncomingRequests.DiagnosticMessage msg);
		void onVehicleInfoGetDTCsRequest(Controllers.VehicleInfo.IncomingRequests.GetDTCs msg);
		void onVehicleInfoGetVehicleDataRequest(Controllers.VehicleInfo.IncomingRequests.GetVehicleData msg);
		void onVehicleInfoGetVehicleTypeRequest(Controllers.VehicleInfo.IncomingRequests.GetVehicleType msg);
		void onVehicleInfoReadDIDRequest(Controllers.VehicleInfo.IncomingRequests.ReadDID msg);
		void onVehicleInfoSubscribeVehicleDataRequest(Controllers.VehicleInfo.IncomingRequests.SubscribeVehicleData msg);
		void onVehicleInfoUnsubscribeVehicleDataRequest(Controllers.VehicleInfo.IncomingRequests.UnsubscribeVehicleData msg);

		//Bc interface incoming callbacks
		void onBcAppRegisteredNotification(Controllers.BasicCommunication.IncomingNotifications.OnAppRegistered msg);
		void onBcAppUnRegisteredNotification(Controllers.BasicCommunication.IncomingNotifications.OnAppUnregistered msg);
        void onBcOnResumeAudioSourceNotification(Controllers.BasicCommunication.IncomingNotifications.OnResumeAudioSource msg);
		void onBcOnSDLPersistenceCompleteNotification(Controllers.BasicCommunication.IncomingNotifications.OnSDLPersistenceComplete msg);
		void onBcOnFileRemovedNotification(Controllers.BasicCommunication.IncomingNotifications.OnFileRemoved msg);
		void onBcOnSDLCloseNotification(Controllers.BasicCommunication.IncomingNotifications.OnSDLClose msg);
		void onBcPutfileNotification(Controllers.BasicCommunication.IncomingNotifications.OnPutFile msg);
		void onBcMixingAudioSupportedRequest(Controllers.BasicCommunication.IncomingRequests.MixingAudioSupported msg);
		void onBcActivateAppRequest(Controllers.BasicCommunication.IncomingRequests.ActivateApp msg, Common.Enums.Result? result);
		void onBcAllowDeviceToConnectRequest(Controllers.BasicCommunication.IncomingRequests.AllowDeviceToConnect msg);
		void onBcDialNumberRequest(Controllers.BasicCommunication.IncomingRequests.DialNumber msg);
		void onBcGetSystemInfoRequest(Controllers.BasicCommunication.IncomingRequests.GetSystemInfo msg);
        void onBcGetSystemTimeRequest(Controllers.BasicCommunication.IncomingRequests.GetSystemTime msg);
		void onBcPolicyUpdateRequest(Controllers.BasicCommunication.IncomingRequests.PolicyUpdate msg);
		void onBcSystemRequestRequest(Controllers.BasicCommunication.IncomingRequests.SystemRequest msg);
		void onBcUpdateAppListRequest(Controllers.BasicCommunication.IncomingRequests.UpdateAppList msg);
		void onBcUpdateDeviceListRequest(Controllers.BasicCommunication.IncomingRequests.UpdateDeviceList msg);
		void onBcDecryptCertificateRequest(Controllers.BasicCommunication.IncomingRequests.DecryptCertificate msg);

        //SDL interface incoming callbacks
        void onSDLActivateAppResponse(Controllers.SDL.IncomingResponses.ActivateApp msg);
        void onSDLGetListOfPermissionsResponse(Controllers.SDL.IncomingResponses.GetListOfPermissions msg);
        void onSDLGetStatusUpdateResponse(Controllers.SDL.IncomingResponses.GetStatusUpdate msg);
        void onSDLGetURLSResponse(Controllers.SDL.IncomingResponses.GetURLS msg);
        void onSDLGetUserFriendlyMessageResponse(Controllers.SDL.IncomingResponses.GetUserFriendlyMessage msg);
        void onSDLUpdateSDLResponse(Controllers.SDL.IncomingResponses.UpdateSDL msg);
        void OnSDLOnAppPermissionChangedNotification(Controllers.SDL.IncomingNotifications.OnAppPermissionChanged msg);
        void OnSDLOnSDLConsentNeededNotification(Controllers.SDL.IncomingNotifications.OnSDLConsentNeeded msg);
        void OnSDLOnStatusUpdateNotification(Controllers.SDL.IncomingNotifications.OnStatusUpdate msg);
        void OnSDLOnSystemErrorNotification(Controllers.SDL.IncomingNotifications.OnSystemError msg);
        void OnSDLAddStatisticsInfoNotification(Controllers.SDL.IncomingNotifications.AddStatisticsInfo msg);
        void OnSDLOnDeviceStateChangedNotification(Controllers.SDL.IncomingNotifications.OnDeviceStateChanged msg);

		//Buttons interface incoming callbacks 
		void onButtonsGetCapabilitiesRequest(Controllers.Buttons.IncomingRequests.GetCapabilities msg, InitialConnectionCommandConfig.ButtonCapabilitiesResponseParam buttonsCapabilitiesResponseParam);
        void onButtonsButtonPressRequest(Controllers.Buttons.IncomingRequests.ButtonPress msg);
		void OnButtonSubscriptionNotification(Controllers.Buttons.IncomingNotifications.OnButtonSubscription msg);

		//RC interface incoming callbacks 
        void onRcGetCapabilitiesRequest(Controllers.RC.IncomingRequests.GetCapabilities msg);
        void onRcGetInteriorVehicleDataRequest(Controllers.RC.IncomingRequests.GetInteriorVehicleData msg);
        void onRcGetInteriorVehicleDataConsentRequest(Controllers.RC.IncomingRequests.GetInteriorVehicleDataConsent msg);
		void onRcReleaseInteriorVehicleDataConsentRequest(Controllers.RC.IncomingRequests.ReleaseInteriorVehicleDataConsent msg);
		void onRcIsReadyRequest(Controllers.RC.IncomingRequests.IsReady msg);
        void onRcSetInteriorVehicleDataRequest(Controllers.RC.IncomingRequests.SetInteriorVehicleData msg);
		void onRcOnRCStatusNotification(Controllers.RC.IncomingNotifications.OnRCStatus msg);
	}
}
