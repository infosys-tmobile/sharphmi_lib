﻿using System;
namespace HmiApiLib.Interfaces
{
	public interface IDispatchingHelper<T>
	{
		void dispatch(T message);

		void handleDispatchingError(String info, Exception ex);

		void handleQueueingError(String info, Exception ex);
	}
}
