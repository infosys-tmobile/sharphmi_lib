﻿using HmiApiLib.Common.Enums;
using System;
using System.Collections.Generic;
using HmiApiLib.Base;
using HmiApiLib.Common.Structs;

namespace HmiApiLib.Builder
{
	public class BuildDefaults
	{
		public static RpcMessage buildDefaultMessage(Type type, int corrId)
		{
			if (type == typeof(Controllers.UI.OutgoingResponses.SetAppIcon))
			{
				return BuildRpc.buildUiSetAppIconResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.Show))
			{
				return BuildRpc.buildUiShowResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.AddCommand))
			{
				return BuildRpc.buildUiAddCommandResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.Alert))
			{
				return BuildRpc.buildUiAlertResponse(corrId, Common.Enums.Result.SUCCESS, 5);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.PerformInteraction))
			{
				return BuildRpc.buildUiPerformInteractionResponse(corrId, 1, "Manual Text Entry", Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.GetLanguage))
			{
				return BuildRpc.buildUiGetLanguageResponse(corrId, Language.EN_US, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.DeleteCommand))
			{
				return BuildRpc.buildUiDeleteCommandResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.IsReady))
			{
				return BuildRpc.buildIsReadyResponse(corrId, Types.InterfaceType.UI, true, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.AddSubMenu))
			{
				return BuildRpc.buildUiAddSubMenuResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.ShowAppMenu))
			{
				return BuildRpc.buildUiShowAppMenuResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.CloseApplication))
			{
				return BuildRpc.buildUiCloseApplicationResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.ChangeRegistration))
			{
				return BuildRpc.buildUiChangeRegistrationResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.ClosePopUp))
			{
				return BuildRpc.buildUiClosePopUpResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.DeleteSubMenu))
			{
				return BuildRpc.buildUiDeleteSubMenuResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.EndAudioPassThru))
			{
				return BuildRpc.buildUiEndAudioPassThruResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.GetCapabilities))
			{
				DisplayCapabilities displayCapabilities = buildDefaultDisplayCapabilities();
				AudioPassThruCapabilities audioPassThruCapabilities = buildDefaultAudioPassThruCapabilities();
				List<SoftButtonCapabilities> softButtonCapsList = buildDefaultSoftButtonCapabilities();
				HMICapabilities hmiCapabilities = buildDefaultHmiCapabilities();
				SystemCapabilities systemCapabilities = buildDefaultSystemCapabilities();

				return BuildRpc.buildUiGetCapabilitiesResponse(corrId, Common.Enums.Result.SUCCESS, displayCapabilities,
															   audioPassThruCapabilities, HmiZoneCapabilities.FRONT, softButtonCapsList, hmiCapabilities, systemCapabilities);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.GetSupportedLanguages))
			{
				List<Language> languages = buildDefaultLanguageList();
				return BuildRpc.buildUiGetSupportedLanguagesResponse(corrId, Common.Enums.Result.SUCCESS, languages);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.PerformAudioPassThru))
			{
				return BuildRpc.buildUiPerformAudioPassThruResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.ScrollableMessage))
			{
				return BuildRpc.buildUiScrollableMessageResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.SetDisplayLayout))
			{
				DisplayCapabilities displayCapabilities = buildDefaultDisplayCapabilities();
				List<ButtonCapabilities> buttonCapabilities = buildDefaultButtonCapsList();
				List<SoftButtonCapabilities> softButtonCapsList = buildDefaultSoftButtonCapabilities();
				PresetBankCapabilities presetBankCapabilities = new PresetBankCapabilities()
				{
					onScreenPresetsAvailable = true
				};

				return BuildRpc.buildUiSetDisplayLayoutResponse(corrId, Common.Enums.Result.SUCCESS, displayCapabilities, buttonCapabilities, softButtonCapsList, presetBankCapabilities);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.SetGlobalProperties))
			{
				return BuildRpc.buildUiSetGlobalPropertiesResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.SetMediaClockTimer))
			{
				return BuildRpc.buildUiSetMediaClockTimerResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.ShowCustomForm))
			{
				return BuildRpc.buildUiShowCustomFormResponse(corrId, Common.Enums.Result.SUCCESS, "info");
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.Slider))
			{
				return BuildRpc.buildUiSliderResponse(corrId, Common.Enums.Result.SUCCESS, 1);
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.CreateWindow))
			{
				return BuildRpc.buildUICreateWindowResponse(corrId, Common.Enums.Result.SUCCESS, true, "");
			}
			else if (type == typeof(Controllers.UI.OutgoingResponses.DeleteWindow))
			{
				return BuildRpc.buildUIDeleteWindowResponse(corrId, Common.Enums.Result.SUCCESS, true, "");
			}
			else if (type == typeof(Controllers.TTS.OutgoingResponses.Speak))
			{
				return BuildRpc.buildTtsSpeakResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.TTS.OutgoingResponses.StopSpeaking))
			{
				return BuildRpc.buildTtsStopSpeakingResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.TTS.OutgoingResponses.GetLanguage))
			{
				return BuildRpc.buildTtsGetLanguageResponse(corrId, Language.EN_US, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.TTS.OutgoingResponses.IsReady))
			{
				return BuildRpc.buildIsReadyResponse(corrId, Types.InterfaceType.TTS, true, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.TTS.OutgoingResponses.ChangeRegistration))
			{
				return BuildRpc.buildTTSChangeRegistrationResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.TTS.OutgoingResponses.GetCapabilities))
			{
				List<SpeechCapabilities> speechCapabilities = buildDefaultSpeechCaps();
				List<PrerecordedSpeech> prerecordedSpeechCapabilities = buildDefaultPrerecordeSpeechCaps();

				return BuildRpc.buildTTSGetCapabilitiesResponse(corrId, Common.Enums.Result.SUCCESS, speechCapabilities, prerecordedSpeechCapabilities);
			}
			else if (type == typeof(Controllers.TTS.OutgoingResponses.GetSupportedLanguages))
			{
				List<Language> languageList = buildDefaultLanguageList();
				return BuildRpc.buildTTSGetSupportedLanguagesResponse(corrId, Common.Enums.Result.SUCCESS, languageList);
			}
			else if (type == typeof(Controllers.TTS.OutgoingResponses.SetGlobalProperties))
			{
				return BuildRpc.buildTTSSetGlobalPropertiesResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.VR.OutgoingResponses.AddCommand))
			{
				return BuildRpc.buildVrAddCommandResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.VR.OutgoingResponses.GetLanguage))
			{
				return BuildRpc.buildVrGetLanguageResponse(corrId, Language.EN_US, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.VR.OutgoingResponses.DeleteCommand))
			{
				return BuildRpc.buildVrDeleteCommandResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.VR.OutgoingResponses.IsReady))
			{
				return BuildRpc.buildIsReadyResponse(corrId, Types.InterfaceType.VR, true, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.VR.OutgoingResponses.PerformInteraction))
			{
				return BuildRpc.buildVrPerformInteractionResponse(corrId, 1, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.VR.OutgoingResponses.ChangeRegistration))
			{
				return BuildRpc.buildVrChangeRegistrationResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.VR.OutgoingResponses.GetCapabilities))
			{
				List<VrCapabilities> vrCapsList = buildDefaultVrCapabilities();
				return BuildRpc.buildVrGetCapabilitiesResponse(corrId, Common.Enums.Result.SUCCESS, vrCapsList);
			}
			else if (type == typeof(Controllers.VR.OutgoingResponses.GetSupportedLanguages))
			{
				List<Language> languageList = buildDefaultLanguageList();
				return BuildRpc.buildVrGetSupportedLanguagesResponse(corrId, Common.Enums.Result.SUCCESS, languageList);
			}
			else if (type == typeof(Controllers.Navigation.OutgoingResponses.IsReady))
			{
				return BuildRpc.buildIsReadyResponse(corrId, Types.InterfaceType.Navigation, true, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.Navigation.OutgoingResponses.AlertManeuver))
			{
				return BuildRpc.buildNavAlertManeuverResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.Navigation.OutgoingResponses.GetWayPoints))
			{
				List<LocationDetails> wayPoints = buildDefaultWayPoints();
				return BuildRpc.buildNavGetWayPointsResponse(corrId, Common.Enums.Result.SUCCESS, wayPoints);
			}
			else if (type == typeof(Controllers.Navigation.OutgoingResponses.SendLocation))
			{
				return BuildRpc.buildNavSendLocationResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.Navigation.OutgoingResponses.ShowConstantTBT))
			{
				return BuildRpc.buildNavShowConstantTBTResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.Navigation.OutgoingResponses.StartAudioStream))
			{
				return BuildRpc.buildNavStartAudioStreamResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.Navigation.OutgoingResponses.StartStream))
			{
				return BuildRpc.buildNavStartStreamResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.Navigation.OutgoingResponses.StopAudioStream))
			{
				return BuildRpc.buildNavStopAudioStreamResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.Navigation.OutgoingResponses.StopStream))
			{
				return BuildRpc.buildNavStopStreamResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.Navigation.OutgoingResponses.SubscribeWayPoints))
			{
				return BuildRpc.buildNavSubscribeWayPointsResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.Navigation.OutgoingResponses.UnsubscribeWayPoints))
			{
				return BuildRpc.buildNavUnsubscribeWayPointsResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.Navigation.OutgoingResponses.UpdateTurnList))
			{
				return BuildRpc.buildNavUpdateTurnListResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.Navigation.OutgoingResponses.SetVideoConfig))
			{
				return BuildRpc.buildNavSetVideoConfigResponse(corrId, Common.Enums.Result.SUCCESS, null);
			}
			else if (type == typeof(Controllers.VehicleInfo.OutgoingResponses.IsReady))
			{
				return BuildRpc.buildIsReadyResponse(corrId, Types.InterfaceType.VehicleInfo, true, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.VehicleInfo.OutgoingResponses.DiagnosticMessage))
			{
				List<int> messageDataResult = new List<int>() { 1, 2, 3 };
				return BuildRpc.buildVehicleInfoDiagnosticMessageResponse(corrId, Common.Enums.Result.SUCCESS, messageDataResult);
			}
			else if (type == typeof(Controllers.VehicleInfo.OutgoingResponses.GetDTCs))
			{
				List<string> dtcList = new List<string>() { "dtc1", "dtc2" };
				return BuildRpc.buildVehicleInfoGetDTCsResponse(corrId, Common.Enums.Result.SUCCESS, 1, dtcList);
			}
			else if (type == typeof(Controllers.VehicleInfo.OutgoingResponses.GetVehicleData))
			{
				GPSData gPSData = buildDefaultGPSData();
				TireStatus tireStatus = buildDefaultTireStatus();
				BeltStatus beltStatus = buildDefaultBeltStatus();
				BodyInformation bodyInformation = buildDefaultBodyInformation();
				DeviceStatus deviceStatus = buildDefaultDeviceStatus();
				HeadLampStatus headLampStatus = buildDefaultHeadLampStatus();
				ECallInfo eCallInfo = buildDefaultECallInfo();
				AirbagStatus airbagStatus = buildDefaultAirBagStaus();
				EmergencyEvent emergencyEvent = buildDefaultEmergencyEvent();
				ClusterModeStatus clusterModeStatus = buildDefaultClusterModeStatus();
				MyKey myKey = buildDefaultMyKey();
				List<FuelRange> fuelRangeList = buildDefaultFuelRangeList();

				return BuildRpc.buildVehicleInfoGetVehicleDataResponse(corrId, Common.Enums.Result.SUCCESS,
																	   gPSData, 100, 1000, -6, ComponentVolumeStatus.NORMAL, 0, -40, "VIN0123456789FFFF", PRNDL.DRIVE, TurnSignal.BOTH, tireStatus,
																	   11431, beltStatus, bodyInformation, deviceStatus, VehicleDataEventStatus.NO_EVENT, WiperStatus.AUTO_ADJUST,
																	   headLampStatus, -1000, 25, -2000, eCallInfo, airbagStatus, emergencyEvent, clusterModeStatus, myKey,
																	   ElectronicParkBrakeStatus.OPEN, 10, fuelRangeList);
			}
			else if (type == typeof(Controllers.VehicleInfo.OutgoingResponses.GetVehicleType))
			{
				VehicleType vehicleType = buildDefaultVehicleType();
				return BuildRpc.buildVehicleInfoGetVehicleTypeResponse(corrId, Common.Enums.Result.SUCCESS, vehicleType);
			}
			else if (type == typeof(Controllers.VehicleInfo.OutgoingResponses.ReadDID))
			{
				List<DIDResult> didResultList = buidDefaultDidResultList();
				return BuildRpc.buildVehicleInfoReadDIDResponse(corrId, Common.Enums.Result.SUCCESS, didResultList);
			}
			else if (type == typeof(Controllers.VehicleInfo.OutgoingResponses.SubscribeVehicleData))
			{
				VehicleDataResult vehicleDataResult = buidDefaultVehicleDataResult();
				return BuildRpc.buildVehicleInfoSubscribeVehicleDataResponse(corrId, Common.Enums.Result.SUCCESS,
																			 vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult,
																			 vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult,
																			 vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult,
																			 vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult,
																			 vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult,
																			 vehicleDataResult, vehicleDataResult);
			}
			else if (type == typeof(Controllers.VehicleInfo.OutgoingResponses.UnsubscribeVehicleData))
			{
				VehicleDataResult vehicleDataResult = buidDefaultVehicleDataResult();
				return BuildRpc.buildVehicleInfoUnsubscribeVehicleDataResponse(corrId, Common.Enums.Result.SUCCESS,
																			 vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult,
																			 vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult,
																			 vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult,
																			 vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult,
																			 vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult, vehicleDataResult,
																			 vehicleDataResult, vehicleDataResult);
			}
			else if (type == typeof(Controllers.BasicCommunication.OutgoingResponses.MixingAudioSupported))
			{
				return BuildRpc.buildBasicCommunicationMixingAudioSupportedResponse(corrId, true, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.BasicCommunication.OutgoingResponses.AllowDeviceToConnect))
			{
				return BuildRpc.buildBasicCommunicationAllowDeviceToConnectResponse(corrId, Common.Enums.Result.SUCCESS, true);
			}
			else if (type == typeof(Controllers.BasicCommunication.OutgoingResponses.DialNumber))
			{
				return BuildRpc.buildBasicCommunicationDialNumberResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.BasicCommunication.OutgoingResponses.GetSystemInfo))
			{
				return BuildRpc.buildBasicCommunicationGetSystemInfoResponse(corrId, Common.Enums.Result.SUCCESS, "CCPU VERSION", Language.EN_US, "WERS_CODE");
			}
			else if (type == typeof(Controllers.BasicCommunication.OutgoingResponses.GetSystemTime))
			{
				Common.Structs.DateTime systemTime = new Common.Structs.DateTime()
				{
					millisecond = 0,
					second = 0,
					minute = 0,
					hour = 0,
					day = 1,
					month = 1,
					year = 2000,
					tz_hour = 0,
					tz_minute = 0
				};
				return BuildRpc.buildBasicCommunicationGetSystemTimeResponse(corrId, Common.Enums.Result.SUCCESS, systemTime);
			}
			else if (type == typeof(Controllers.BasicCommunication.OutgoingResponses.PolicyUpdate))
			{
				return BuildRpc.buildBasicCommunicationPolicyUpdateResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.BasicCommunication.OutgoingResponses.SystemRequest))
			{
				return BuildRpc.buildBasicCommunicationSystemRequestResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.BasicCommunication.OutgoingResponses.UpdateAppList))
			{
				return BuildRpc.buildBasicCommunicationUpdateAppListResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.BasicCommunication.OutgoingResponses.UpdateDeviceList))
			{
				return BuildRpc.buildBasicCommunicationUpdateDeviceListResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.Buttons.OutgoingResponses.GetCapabilities))
			{
				PresetBankCapabilities presetBankCapabilities = new PresetBankCapabilities()
				{
					onScreenPresetsAvailable = true
				};
				List<ButtonCapabilities> capabilities = buildDefaultButtonCapsList();
				return BuildRpc.buildButtonsGetCapabilitiesResponse(corrId, capabilities, presetBankCapabilities, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.Buttons.OutgoingResponses.ButtonPress))
			{
				return BuildRpc.buildButtonsButtonPressResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.RC.OutgoingResponses.GetCapabilities))
			{
				RemoteControlCapabilities remoteControlCapabilities = new RemoteControlCapabilities
				{
					climateControlCapabilities = buildDefaultClimateCaps(),
					radioControlCapabilities = buildDefaultRadioCapsList(),
					buttonCapabilities = buildDefaultButtonCapsList(),
					seatControlCapabilities = buildDefaultSeatCapsList(),
					audioControlCapabilities = buildDefaultAudioCapsList(),
					lightControlCapabilities = buildDefaultLightCaps(),
					hmiSettingsControlCapabilities = buildDefaultHmiSettingCaps()
				};
				return BuildRpc.buildRcGetCapabilitiesResponse(corrId, Common.Enums.Result.SUCCESS, remoteControlCapabilities);
			}
			else if (type == typeof(Controllers.RC.OutgoingResponses.GetInteriorVehicleData))
			{
				ModuleData moduleData = buildDefaultModuleData();
				return BuildRpc.buildRcGetInteriorVehicleDataResponse(corrId, Common.Enums.Result.SUCCESS, moduleData, false);
			}
			else if (type == typeof(Controllers.RC.OutgoingResponses.GetInteriorVehicleDataConsent))
			{
				return BuildRpc.buildRcGetInteriorVehicleDataConsentResponse(corrId, Common.Enums.Result.SUCCESS, true, true, "");
			}
			else if (type == typeof(Controllers.RC.OutgoingResponses.ReleaseInteriorVehicleDataConsent))
			{
				return BuildRpc.buildRcReleaseInteriorVehicleDataConsentResponse(corrId, Common.Enums.Result.SUCCESS, true, "");
			}
			else if (type == typeof(Controllers.RC.OutgoingResponses.IsReady))
			{
				return BuildRpc.buildIsReadyResponse(corrId, Types.InterfaceType.RC, true, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.RC.OutgoingResponses.SetInteriorVehicleData))
			{
				ModuleData moduleData = buildDefaultModuleData();
				return BuildRpc.buildRcSetInteriorVehicleDataResponse(corrId, Common.Enums.Result.SUCCESS, moduleData);
			}
			else if (type == typeof(Controllers.BasicCommunication.OutgoingResponses.ActivateApp))
			{
				return BuildRpc.buildBasicCommunicationActivateAppResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			else if (type == typeof(Controllers.BasicCommunication.OutgoingResponses.DecryptCertificate))
			{
				return BuildRpc.buildBasicCommunicationDecryptCertificateResponse(corrId, Common.Enums.Result.SUCCESS);
			}
			return null;
		

	}

	static ModuleData buildDefaultModuleData()
        {
            ModuleData moduleData = new ModuleData();
            moduleData.moduleType = ModuleType.CLIMATE;
            moduleData.climateControlData = buildClimateControlData();

            return moduleData;
        }

        static VehicleDataResult buidDefaultVehicleDataResult()
        {
            VehicleDataResult vehicleDataResult = new VehicleDataResult()
            {
                dataType = VehicleDataType.VEHICLEDATA_ACCPEDAL,
                resultCode = VehicleDataResultCode.SUCCESS
            };

            return vehicleDataResult;
        }

        static List<DIDResult> buidDefaultDidResultList()
        {
            DIDResult dIDResult = new DIDResult()
            {
                resultCode = VehicleDataResultCode.SUCCESS,
                didLocation = 1,
                data = "data"
            };
            List<DIDResult> didResultList = new List<DIDResult>(){dIDResult};

            return didResultList;
        }
		public static List<ImageField> buildDefaultImageField()
		{
			ImageField imageField1 = buildImageField(ImageFieldName.softButtonImage);
			ImageField imageField2 = buildImageField(ImageFieldName.choiceImage);
			ImageField imageField3 = buildImageField(ImageFieldName.appIcon);
			ImageField imageField4 = buildImageField(ImageFieldName.graphic);

			List<ImageField> imageFieldList = new List<ImageField>
			{
				imageField1,imageField2,imageField3,imageField4
			};
			return imageFieldList;
		}
		public static List<TextField> buildDefaultTextField()
		{
			TextField field1 = buildTextField(TextFieldName.mainField1);
			TextField field2 = buildTextField(TextFieldName.mainField2);
			TextField field3 = buildTextField(TextFieldName.mainField3);
			TextField field4 = buildTextField(TextFieldName.mainField4);


			List<TextField> textFieldList = new List<TextField>
			{
				field1,field2,field3,field4
			};
			return textFieldList;
		}
		static VehicleType buildDefaultVehicleType()
        {
            VehicleType vehicleType = new VehicleType
            {
                make = "Ford",
                model = "Edge",
                trim = "SEL",
                modelYear = "2017"
            };

            return vehicleType;
        }

        public static List<FuelRange> buildDefaultFuelRangeList()
        {
            FuelRange fuelRange = new FuelRange();
            fuelRange.range = 5;
            fuelRange.type = FuelType.GASOLINE;

            List<FuelRange> fuelRangeList = new List<FuelRange>();
            fuelRangeList.Add(fuelRange);

            return fuelRangeList;
        }

        static MyKey buildDefaultMyKey()
        {
            MyKey myKey = new MyKey();
            myKey.e911Override = VehicleDataStatus.OFF;

            return myKey;
        }

        public static ClusterModeStatus buildDefaultClusterModeStatus()
        {
            ClusterModeStatus clusterModeStatus = new ClusterModeStatus();
            clusterModeStatus.powerModeActive = true;
            clusterModeStatus.powerModeQualificationStatus = PowerModeQualificationStatus.POWER_MODE_OK;
            clusterModeStatus.carModeStatus = CarModeStatus.NORMAL;
            clusterModeStatus.powerModeStatus = PowerModeStatus.RUNNING_2;

            return clusterModeStatus;
        }

        public static EmergencyEvent buildDefaultEmergencyEvent()
        {
            EmergencyEvent emergencyEvent = new EmergencyEvent();
            emergencyEvent.emergencyEventType = EmergencyEventType.NO_EVENT;
            emergencyEvent.fuelCutoffStatus = FuelCutoffStatus.NORMAL_OPERATION;
            emergencyEvent.rolloverEvent = VehicleDataEventStatus.NO_EVENT;
            emergencyEvent.maximumChangeVelocity = VehicleDataEventStatus.NO_EVENT;
            emergencyEvent.multipleEvents = VehicleDataEventStatus.NO_EVENT;

            return emergencyEvent;
        }
        public static AirbagStatus buildDefaultAirBagStaus()
        {
            AirbagStatus airbagStatus = new AirbagStatus();
            airbagStatus.driverAirbagDeployed = VehicleDataEventStatus.NO_EVENT;
            airbagStatus.driverSideAirbagDeployed = VehicleDataEventStatus.NO_EVENT;
            airbagStatus.driverCurtainAirbagDeployed = VehicleDataEventStatus.NO_EVENT;
            airbagStatus.passengerAirbagDeployed = VehicleDataEventStatus.NO_EVENT;
            airbagStatus.passengerCurtainAirbagDeployed = VehicleDataEventStatus.NO_EVENT;
            airbagStatus.driverKneeAirbagDeployed = VehicleDataEventStatus.NO_EVENT;
            airbagStatus.passengerKneeAirbagDeployed = VehicleDataEventStatus.NO_EVENT;
            airbagStatus.passengerSideAirbagDeployed = VehicleDataEventStatus.NO_EVENT;

            return airbagStatus;
        }

        public static ECallInfo buildDefaultECallInfo()
        {
            ECallInfo eCallInfo = new ECallInfo();
            eCallInfo.auxECallNotificationStatus = VehicleDataNotificationStatus.NOT_SUPPORTED;
            eCallInfo.eCallNotificationStatus = VehicleDataNotificationStatus.NOT_SUPPORTED;
            eCallInfo.eCallConfirmationStatus = ECallConfirmationStatus.NORMAL;

            return eCallInfo;
        }

        public static HeadLampStatus buildDefaultHeadLampStatus()
        {
            HeadLampStatus headLampStatus = new HeadLampStatus();
            headLampStatus.lowBeamsOn = true;
            headLampStatus.highBeamsOn = true;
            headLampStatus.ambientLightSensorStatus = AmbientLightStatus.DAY;

            return headLampStatus;
        }

       
        

        public static BeltStatus buildDefaultBeltStatus()
        {
            BeltStatus beltStatus = new BeltStatus();
            beltStatus.driverBeltDeployed = VehicleDataEventStatus.NO_EVENT;
            beltStatus.passengerBeltDeployed = VehicleDataEventStatus.NO_EVENT;
            beltStatus.passengerBuckleBelted = VehicleDataEventStatus.NO_EVENT;
            beltStatus.driverBuckleBelted = VehicleDataEventStatus.YES;
            beltStatus.leftRow2BuckleBelted = VehicleDataEventStatus.NO_EVENT;
            beltStatus.passengerChildDetected = VehicleDataEventStatus.NO_EVENT;
            beltStatus.rightRow2BuckleBelted = VehicleDataEventStatus.NO_EVENT;
            beltStatus.middleRow2BuckleBelted = VehicleDataEventStatus.NO_EVENT;
            beltStatus.middleRow3BuckleBelted = VehicleDataEventStatus.NO_EVENT;
            beltStatus.leftRow3BuckleBelted = VehicleDataEventStatus.NO_EVENT;
            beltStatus.rightRow3BuckleBelted = VehicleDataEventStatus.NO_EVENT;
            beltStatus.leftRearInflatableBelted = VehicleDataEventStatus.NO_EVENT;
            beltStatus.rightRearInflatableBelted = VehicleDataEventStatus.NO_EVENT;
            beltStatus.middleRow1BeltDeployed = VehicleDataEventStatus.NO_EVENT;
            beltStatus.middleRow1BuckleBelted = VehicleDataEventStatus.NO_EVENT;

            return beltStatus;
        }

        public static TireStatus buildDefaultTireStatus()
        {
            TireStatus tireStatus = new TireStatus();
            tireStatus.pressureTelltale = WarningLightStatus.OFF;
            SingleTireStatus singleTire = new SingleTireStatus();
            singleTire.status = ComponentVolumeStatus.NORMAL;
            tireStatus.leftFront = singleTire;
            tireStatus.rightFront = singleTire;
            tireStatus.leftRear = singleTire;
            tireStatus.rightRear = singleTire;
            tireStatus.innerLeftRear = singleTire;
            tireStatus.innerRightRear = singleTire;

            return tireStatus;
        }


        static List<LocationDetails> buildDefaultWayPoints()
        {
            LocationDetails locationDetails1 = new LocationDetails()
            {
                coordinate = new Coordinate()
                {
                    latitudeDegrees = 1,
                    longitudeDegrees = 1
                },
                locationName = "Location Name",
                addressLines = new List<string>() { "Address 1", "Address 2", "Address 3" },
                locationDescription = "Location Description",
                phoneNumber = "1234567890",
                locationImage = new Image()
                {
                    value = "Image Value",
                    imageType = ImageType.STATIC,
                    isTemplate = false
                },
                searchAddress = new OASISAddress()
                {
                    countryName = "countryName",
                    countryCode = "countryCode",
                    postalCode = "12345",
                    administrativeArea = "Administrative Area",
                    subAdministrativeArea = "Sub-Administrative Area",
                    locality = "Locality",
                    subLocality = "Sub-Locality",
                    thoroughfare = "thorough fare",
                    subThoroughfare = "subThoroughfare"
                }
            };
            List<LocationDetails> wayPoints = new List<LocationDetails>(){locationDetails1};
            return wayPoints;
        }

        static List<VrCapabilities> buildDefaultVrCapabilities()
        {
            List<VrCapabilities> vrCapsList = new List<VrCapabilities>
            {
                VrCapabilities.TEXT
            };

            return vrCapsList;
        }

        public static List<ButtonCapabilities> buildDefaultButtonCapsList()
        {
            ButtonCapabilities caps0 = new ButtonCapabilities();
            ButtonCapabilities caps1 = new ButtonCapabilities();
            ButtonCapabilities caps2 = new ButtonCapabilities();
            ButtonCapabilities caps3 = new ButtonCapabilities();
            ButtonCapabilities caps4 = new ButtonCapabilities();
            ButtonCapabilities caps5 = new ButtonCapabilities();
            ButtonCapabilities caps6 = new ButtonCapabilities();
            ButtonCapabilities caps7 = new ButtonCapabilities();
            ButtonCapabilities caps8 = new ButtonCapabilities();
            ButtonCapabilities caps9 = new ButtonCapabilities();
            ButtonCapabilities caps10 = new ButtonCapabilities();
            ButtonCapabilities caps11 = new ButtonCapabilities();
            ButtonCapabilities caps12 = new ButtonCapabilities();
            ButtonCapabilities caps13 = new ButtonCapabilities();
            ButtonCapabilities caps14 = new ButtonCapabilities();
            ButtonCapabilities caps15 = new ButtonCapabilities();
            ButtonCapabilities caps16 = new ButtonCapabilities();
            ButtonCapabilities caps17 = new ButtonCapabilities();
            ButtonCapabilities caps18 = new ButtonCapabilities();
            ButtonCapabilities caps19 = new ButtonCapabilities();        

            caps0.name = ButtonName.SEEKLEFT;
            caps0.shortPressAvailable = true;
            caps0.longPressAvailable = true;
            caps0.upDownAvailable = true;

            caps1.name = ButtonName.SEEKRIGHT;
            caps1.shortPressAvailable = true;
            caps1.longPressAvailable = true;
            caps1.upDownAvailable = true;

            caps2.name = ButtonName.OK;
            caps2.shortPressAvailable = true;
            caps2.longPressAvailable = true;
            caps2.upDownAvailable = true;

            caps3.name = ButtonName.TUNEDOWN;
            caps3.shortPressAvailable = true;
            caps3.longPressAvailable = true;
            caps3.upDownAvailable = true;

            caps4.name = ButtonName.TUNEUP;
            caps4.shortPressAvailable = true;
            caps4.longPressAvailable = true;
            caps4.upDownAvailable = true;

            caps5.name = ButtonName.PRESET_0;
            caps5.shortPressAvailable = true;
            caps5.longPressAvailable = true;
            caps5.upDownAvailable = true;

            caps6.name = ButtonName.PRESET_1;
            caps6.shortPressAvailable = true;
            caps6.longPressAvailable = true;
            caps6.upDownAvailable = true;

            caps7.name = ButtonName.CUSTOM_BUTTON;
            caps7.shortPressAvailable = true;
            caps7.longPressAvailable = true;
            caps7.upDownAvailable = true;

            caps8.name = ButtonName.AC_MAX;
            caps8.shortPressAvailable = true;
            caps8.longPressAvailable = true;
            caps8.upDownAvailable = true;

            caps9.name = ButtonName.AC;
            caps9.shortPressAvailable = true;
            caps9.longPressAvailable = true;
            caps9.upDownAvailable = true;

            caps10.name = ButtonName.SEARCH;
            caps10.shortPressAvailable = true;
            caps10.longPressAvailable = true;
            caps10.upDownAvailable = true;

            caps11.name = ButtonName.PLAY_PAUSE;
            caps11.shortPressAvailable = true;
            caps11.longPressAvailable = true;
            caps11.upDownAvailable = true;

            caps12.name = ButtonName.PRESET_2;
            caps12.shortPressAvailable = true;
            caps12.longPressAvailable = true;
            caps12.upDownAvailable = true;

            caps13.name = ButtonName.PRESET_3;
            caps13.shortPressAvailable = true;
            caps13.longPressAvailable = true;
            caps13.upDownAvailable = true;

            caps14.name = ButtonName.PRESET_4;
            caps14.shortPressAvailable = true;
            caps14.longPressAvailable = true;
            caps14.upDownAvailable = true;

            caps15.name = ButtonName.PRESET_5;
            caps15.shortPressAvailable = true;
            caps15.longPressAvailable = true;
            caps15.upDownAvailable = true;

            caps16.name = ButtonName.PRESET_6;
            caps16.shortPressAvailable = true;
            caps16.longPressAvailable = true;
            caps16.upDownAvailable = true;

            caps17.name = ButtonName.PRESET_7;
            caps17.shortPressAvailable = true;
            caps17.longPressAvailable = true;
            caps17.upDownAvailable = true;

            caps18.name = ButtonName.PRESET_8;
            caps18.shortPressAvailable = true;
            caps18.longPressAvailable = true;
            caps18.upDownAvailable = true;

            caps19.name = ButtonName.PRESET_9;
            caps19.shortPressAvailable = true;
            caps19.longPressAvailable = true;
            caps19.upDownAvailable = true;


            List<ButtonCapabilities> buttonCapabilitiesList = new List<ButtonCapabilities>
                {
                    caps0,
                    caps1,
                    caps2,
                    caps3,
                    caps4,
                    caps5,
                    caps6,
                    caps7,
                    caps8,
                    caps9,
                    caps10,
                    caps11,
                    caps12,
                    caps13,
                    caps14,
                    caps15,
                    caps16,
                    caps17,
                    caps18,
                    caps19
                };
            return buttonCapabilitiesList;
        }

     
		
		

		

		

		public static DeviceStatus buildDefaultDeviceStatus()
		{
			DeviceStatus deviceStatus = new DeviceStatus();
			deviceStatus.voiceRecOn = true;
			deviceStatus.btIconOn = true;
			deviceStatus.callActive = true;
			deviceStatus.phoneRoaming = true;
			deviceStatus.textMsgAvailable = true;
			deviceStatus.battLevelStatus = DeviceLevelStatus.FOUR_LEVEL_BARS;
			deviceStatus.stereoAudioOutputMuted = true;
			deviceStatus.monoAudioOutputMuted = true;
			deviceStatus.signalLevelStatus = DeviceLevelStatus.FOUR_LEVEL_BARS;
			deviceStatus.primaryAudioSource = PrimaryAudioSource.BLUETOOTH_STEREO_BTST;
			deviceStatus.eCallEventActive = true;

			return deviceStatus;
		}

		public static BodyInformation buildDefaultBodyInformation()
		{
			BodyInformation bodyInformation = new BodyInformation();
			bodyInformation.parkBrakeActive = true;
			bodyInformation.ignitionStableStatus = IgnitionStableStatus.IGNITION_SWITCH_STABLE;
			bodyInformation.ignitionStatus = IgnitionStatus.RUN;
			bodyInformation.driverDoorAjar = true;
			bodyInformation.passengerDoorAjar = true;
			bodyInformation.rearRightDoorAjar = true;
			bodyInformation.rearLeftDoorAjar = true;

			return bodyInformation;
		}

		

		

		public static GPSData buildDefaultGPSData()
		{
			GPSData gPSData = new GPSData();
			gPSData.longitudeDegrees = -180;
			gPSData.latitudeDegrees = -90;
			gPSData.utcYear = 2017;
			gPSData.utcMonth = 1;
			gPSData.utcDay = 1;
			gPSData.utcHours = 0;
			gPSData.utcMinutes = 0;
			gPSData.utcSeconds = 0;
			gPSData.compassDirection = CompassDirection.EAST;
			gPSData.pdop = 0;
			gPSData.hdop = 0;
			gPSData.vdop = 0;
			gPSData.actual = true;
			gPSData.satellites = 1;
			gPSData.dimension = Dimension.Dimension_2D;
			gPSData.altitude = -10000;
			gPSData.heading = 0;
			gPSData.speed = 100;
			gPSData.shifted = true;

			return gPSData;
		}


		

		
		public static List<ClimateControlCapabilities> buildDefaultClimateCaps()
		{
			ClimateControlCapabilities climateControlCapabilities = new HmiApiLib.Common.Structs.ClimateControlCapabilities();

			climateControlCapabilities.moduleName = "Climate";
			climateControlCapabilities.currentTemperatureAvailable = true;
			climateControlCapabilities.fanSpeedAvailable = true;
			climateControlCapabilities.desiredTemperatureAvailable = true;
			climateControlCapabilities.acEnableAvailable = true;
			climateControlCapabilities.acMaxEnableAvailable = true;
			climateControlCapabilities.circulateAirEnableAvailable = true;
			climateControlCapabilities.autoModeEnableAvailable = true;
			climateControlCapabilities.dualModeEnableAvailable = true;
			climateControlCapabilities.defrostZoneAvailable = true;
			climateControlCapabilities.ventilationModeAvailable = true;
			climateControlCapabilities.moduleInfo = buildDefaultModulInfo();

			List<DefrostZone> defrostZoneList = new List<DefrostZone>();
			defrostZoneList.Add(DefrostZone.FRONT);
			defrostZoneList.Add(DefrostZone.REAR);
			defrostZoneList.Add(DefrostZone.ALL);
			defrostZoneList.Add(DefrostZone.NONE);
			climateControlCapabilities.defrostZone = defrostZoneList;

			List<VentilationMode> ventilationModeList = new List<VentilationMode>();
			ventilationModeList.Add(VentilationMode.UPPER);
			ventilationModeList.Add(VentilationMode.LOWER);
			ventilationModeList.Add(VentilationMode.BOTH);
			ventilationModeList.Add(VentilationMode.NONE);
			climateControlCapabilities.ventilationMode = ventilationModeList;
			climateControlCapabilities.heatedSteeringWheelAvailable = true;
			climateControlCapabilities.heatedWindshieldAvailable = true;
			climateControlCapabilities.heatedRearWindowAvailable = true;
			climateControlCapabilities.heatedMirrorsAvailable = true;
			climateControlCapabilities.climateEnableAvailable = true;

			List<ClimateControlCapabilities> climateControlCapabilitiesList = new List<ClimateControlCapabilities>();
			climateControlCapabilitiesList.Add(climateControlCapabilities);

			return climateControlCapabilitiesList;
		}

		public static List<RadioControlCapabilities> buildDefaultRadioCapsList()
		{
			RadioControlCapabilities radioControlCapabilities = new HmiApiLib.Common.Structs.RadioControlCapabilities();

			radioControlCapabilities.moduleName = "Radio";
			radioControlCapabilities.radioEnableAvailable = true;
			radioControlCapabilities.radioBandAvailable = true;
			radioControlCapabilities.radioFrequencyAvailable = true;
			radioControlCapabilities.hdChannelAvailable = true;
			radioControlCapabilities.rdsDataAvailable = true;
			radioControlCapabilities.availableHDsAvailable = true;
			radioControlCapabilities.stateAvailable = true;
			radioControlCapabilities.signalStrengthAvailable = true;
			radioControlCapabilities.signalChangeThresholdAvailable = true;
			radioControlCapabilities.sisDataAvailable = true;
			radioControlCapabilities.hdRadioEnableAvailable = true;
			radioControlCapabilities.siriusxmRadioAvailable = true;
			radioControlCapabilities.availableHdChannelsAvailable = true;

			List<RadioControlCapabilities> radioControlCapabilitiesList = new List<RadioControlCapabilities>();
			radioControlCapabilitiesList.Add(radioControlCapabilities);

			return radioControlCapabilitiesList;
		}

		public static List<SeatControlCapabilities> buildDefaultSeatCapsList()
		{
			SeatControlCapabilities seatControlCapabilities = new HmiApiLib.Common.Structs.SeatControlCapabilities();

			seatControlCapabilities.moduleName = "Seat";
			seatControlCapabilities.heatingEnabledAvailable = true;
			seatControlCapabilities.coolingEnabledAvailable = true;
			seatControlCapabilities.heatingLevelAvailable = true;
			seatControlCapabilities.coolingLevelAvailable = true;
			seatControlCapabilities.horizontalPositionAvailable = true;
			seatControlCapabilities.verticalPositionAvailable = true;
			seatControlCapabilities.frontVerticalPositionAvailable = true;
			seatControlCapabilities.backVerticalPositionAvailable = true;
			seatControlCapabilities.backTiltAngleAvailable = true;
			seatControlCapabilities.headSupportHorizontalPositionAvailable = true;
			seatControlCapabilities.headSupportVerticalPositionAvailable = true;
			seatControlCapabilities.massageEnabledAvailable = true;
			seatControlCapabilities.massageModeAvailable = true;
			seatControlCapabilities.massageCushionFirmnessAvailable = true;
			seatControlCapabilities.memoryAvailable = true;

			List<SeatControlCapabilities> seatControlCapabilitiesList = new List<SeatControlCapabilities>();
			seatControlCapabilitiesList.Add(seatControlCapabilities);

			return seatControlCapabilitiesList;
		}

		public static List<AudioControlCapabilities> buildDefaultAudioCapsList()
		{
			AudioControlCapabilities audioControlCapabilities = new HmiApiLib.Common.Structs.AudioControlCapabilities();

			audioControlCapabilities.moduleName = "Audio";
			audioControlCapabilities.sourceAvailable = true;
			audioControlCapabilities.volumeAvailable = true;
			audioControlCapabilities.equalizerAvailable = true;
			audioControlCapabilities.equalizerMaxChannelId = 1;


			List<AudioControlCapabilities> audioControlCapabilitiesList = new List<AudioControlCapabilities>();
			audioControlCapabilitiesList.Add(audioControlCapabilities);

			return audioControlCapabilitiesList;
		}

		public static LightControlCapabilities buildDefaultLightCaps()
		{
			LightControlCapabilities lightControlCapabilities = new HmiApiLib.Common.Structs.LightControlCapabilities();

			lightControlCapabilities.moduleName = "Light";
			LightCapabilities lightCapabilities = new LightCapabilities();
			lightCapabilities.densityAvailable = true;
			lightCapabilities.name = LightName.AMBIENT_LIGHTS;
			lightCapabilities.rgbColorSpaceAvailable = true;
			lightCapabilities.statusAvailable = true;
			List<LightCapabilities> lightCapabilitiesList = new List<LightCapabilities>();
			lightCapabilitiesList.Add(lightCapabilities);
			lightControlCapabilities.supportedLights = lightCapabilitiesList;

			return lightControlCapabilities;
		}

		public static HMISettingsControlCapabilities buildDefaultHmiSettingCaps()
		{
			HMISettingsControlCapabilities hmiSettingControlCapabilities = new HmiApiLib.Common.Structs.HMISettingsControlCapabilities();

			hmiSettingControlCapabilities.moduleName = "HMISETTINGS";
			hmiSettingControlCapabilities.distanceUnitAvailable = true;
			hmiSettingControlCapabilities.temperatureUnitAvailable = true;
			hmiSettingControlCapabilities.displayModeUnitAvailable = true;

			return hmiSettingControlCapabilities;
		}

		public static List<SpeechCapabilities> buildDefaultSpeechCaps()

        {
            List<SpeechCapabilities> speechCapabilities = new List<SpeechCapabilities>
                 {
                    SpeechCapabilities.LHPLUS_PHONEMES,
                    SpeechCapabilities.PRE_RECORDED,
                    SpeechCapabilities.SAPI_PHONEMES,
                    SpeechCapabilities.SILENCE,
                    SpeechCapabilities.TEXT,
                    SpeechCapabilities.FILE
                 };
            return speechCapabilities;
        }

        public static List<PrerecordedSpeech> buildDefaultPrerecordeSpeechCaps()
        {
            List<PrerecordedSpeech> prerecordedSpeechCapabilities = new List<PrerecordedSpeech>
                 {
                    PrerecordedSpeech.HELP_JINGLE,
                    PrerecordedSpeech.INITIAL_JINGLE,
                    PrerecordedSpeech.LISTEN_JINGLE,
                    PrerecordedSpeech.NEGATIVE_JINGLE,
                    PrerecordedSpeech.POSITIVE_JINGLE
                 };
            return prerecordedSpeechCapabilities;
        }

        static HMICapabilities buildDefaultHmiCapabilities()
        {
            HMICapabilities hMICapabilities = new HMICapabilities();
            hMICapabilities.navigation = true;
            hMICapabilities.phoneCall = true;
            hMICapabilities.videoStreaming = true;

            return hMICapabilities;
        }

       

        static AudioPassThruCapabilities buildDefaultAudioPassThruCapabilities()
        {
            AudioPassThruCapabilities audioPassThruCapabilities = new AudioPassThruCapabilities();
            audioPassThruCapabilities.audioType = AudioType.PCM;
            audioPassThruCapabilities.bitsPerSample = BitsPerSample.RATE_8_BIT;
            audioPassThruCapabilities.samplingRate = SamplingRate.RATE_16KHZ;

            return audioPassThruCapabilities;
        }

        static TextField buildTextField(TextFieldName fieldName, int? rows = 1)
        {
            TextField field = new TextField();
            field.name = fieldName;
            field.characterSet = CharacterSet.TYPE2SET;
            field.width = 500;
            field.rows = rows;

            return field;
        }

        static ImageField buildImageField(ImageFieldName fieldName)
        {
            List<FileType> fileTypeList = new List<FileType>
            {
                FileType.AUDIO_AAC,FileType.AUDIO_MP3,FileType.AUDIO_WAVE,
                FileType.BINARY,FileType.GRAPHIC_BMP,FileType.GRAPHIC_JPEG,
                FileType.GRAPHIC_PNG,FileType.JSON
            };

            ImageResolution imageRes = new ImageResolution();
            imageRes.resolutionWidth = 64;
            imageRes.resolutionHeight = 64;

            ImageField imageField = new ImageField();
            imageField.name = fieldName;
            imageField.imageResolution = imageRes;
            imageField.imageTypeSupported = fileTypeList;

            return imageField;
        }

        static DisplayCapabilities buildDefaultDisplayCapabilities()
        {
            DisplayCapabilities displayCapabilities = new DisplayCapabilities();
            displayCapabilities.displayType = DisplayType.GEN3_8_INCH;
            displayCapabilities.graphicSupported = true;
            List<String> templateList = new List<String>
            {
                "Template1",
                "Template2"
            };
            displayCapabilities.templatesAvailable = templateList;
            displayCapabilities.numCustomPresetsAvailable = 10;

            TextField field1 = buildTextField(TextFieldName.mainField1);
            TextField field2 = buildTextField(TextFieldName.mainField2);
            TextField field3 = buildTextField(TextFieldName.mainField3);
            TextField field4 = buildTextField(TextFieldName.mainField4);
            TextField field5 = buildTextField(TextFieldName.statusBar);
            TextField field6 = buildTextField(TextFieldName.mediaClock);
            TextField field7 = buildTextField(TextFieldName.mediaTrack);
            TextField field8 = buildTextField(TextFieldName.alertText1);
            TextField field9 = buildTextField(TextFieldName.alertText2);
            TextField field10 = buildTextField(TextFieldName.alertText3);
            TextField field11 = buildTextField(TextFieldName.scrollableMessageBody,5);
            TextField field12 = buildTextField(TextFieldName.initialInteractionText);
            TextField field13 = buildTextField(TextFieldName.navigationText1);
            TextField field14 = buildTextField(TextFieldName.navigationText2);
            TextField field15 = buildTextField(TextFieldName.ETA);
            TextField field16 = buildTextField(TextFieldName.totalDistance);
            TextField field17 = buildTextField(TextFieldName.audioPassThruDisplayText1);
            TextField field18 = buildTextField(TextFieldName.audioPassThruDisplayText2);
            TextField field19 = buildTextField(TextFieldName.sliderHeader);
            TextField field20 = buildTextField(TextFieldName.sliderFooter);
            TextField field21 = buildTextField(TextFieldName.menuName);
            TextField field22 = buildTextField(TextFieldName.secondaryText);
            TextField field23 = buildTextField(TextFieldName.tertiaryText);
            TextField field24 = buildTextField(TextFieldName.menuTitle);
            TextField field25 = buildTextField(TextFieldName.navigationText);
            TextField field26 = buildTextField(TextFieldName.notificationText);
            TextField field27 = buildTextField(TextFieldName.locationName);
            TextField field28 = buildTextField(TextFieldName.locationDescription);
            TextField field29 = buildTextField(TextFieldName.addressLines);
            TextField field30 = buildTextField(TextFieldName.phoneNumber);
            TextField field31 = buildTextField(TextFieldName.timeToDestination);
            TextField field32 = buildTextField(TextFieldName.turnText);

            List<TextField> textFieldList = new List<TextField>
            {
                field1,field2,field3,field4,field5,field6,field7,field8,field9,field10,field11,field12,field13,field14,field15,field16,field17,field18,field19,field20,field21,field22,field23,field24,field25,field26,field27,field28,field29,field30,field31,field32
            };
            displayCapabilities.textFields = textFieldList;

            ImageField imageField1 = buildImageField(ImageFieldName.softButtonImage);
            ImageField imageField2 = buildImageField(ImageFieldName.choiceImage);
            ImageField imageField3 = buildImageField(ImageFieldName.choiceSecondaryImage);
            ImageField imageField4 = buildImageField(ImageFieldName.vrHelpItem);
            ImageField imageField5 = buildImageField(ImageFieldName.turnIcon);
            ImageField imageField6 = buildImageField(ImageFieldName.menuIcon);
            ImageField imageField7 = buildImageField(ImageFieldName.cmdIcon);
            ImageField imageField8 = buildImageField(ImageFieldName.appIcon);
            ImageField imageField9 = buildImageField(ImageFieldName.graphic);
            ImageField imageField10 = buildImageField(ImageFieldName.showConstantTBTIcon);
            ImageField imageField11 = buildImageField(ImageFieldName.showConstantTBTNextTurnIcon);
            ImageField imageField12 = buildImageField(ImageFieldName.locationImage);

            List<ImageField> imageFieldList = new List<ImageField>
            {
                imageField1,imageField2,imageField3,imageField4,imageField5,imageField6,imageField7,imageField8,imageField9,imageField10,imageField11,imageField12
            };
            displayCapabilities.imageFields = imageFieldList;

            List<MediaClockFormat> mediaClockFormatList = new List<MediaClockFormat>
            {
                MediaClockFormat.CLOCK1,MediaClockFormat.CLOCK2,MediaClockFormat.CLOCK3,MediaClockFormat.CLOCKTEXT1,MediaClockFormat.CLOCKTEXT2,MediaClockFormat.CLOCKTEXT3,MediaClockFormat.CLOCKTEXT4
            };
            displayCapabilities.mediaClockFormats = mediaClockFormatList;

            List<ImageType> imageTypeList = new List<ImageType>
            {
                ImageType.DYNAMIC,ImageType.STATIC
            };
            displayCapabilities.imageCapabilities = imageTypeList;

            ImageResolution screenParamRes = new ImageResolution();
            screenParamRes.resolutionWidth = 800;
            screenParamRes.resolutionHeight = 480;
            ScreenParams screenParams = new ScreenParams();
            screenParams.resolution = screenParamRes;
            TouchEventCapabilities touchCaps = new TouchEventCapabilities();
            touchCaps.doublePressAvailable = true;
            touchCaps.pressAvailable = true;
            touchCaps.pressAvailable = true;
            touchCaps.multiTouchAvailable = true;

            screenParams.touchEventAvailable = touchCaps;

            displayCapabilities.screenParams = screenParams;
            displayCapabilities.numCustomPresetsAvailable = 10;

            return displayCapabilities;
        }

        

		
		
		public static ModuleInfo buildDefaultModulInfo()
		{
			ModuleInfo moduleInfo = new ModuleInfo();
			moduleInfo.moduleId = "0";
			moduleInfo.allowMultipleAccess = true;
			moduleInfo.location = buildDefaultGrid();
			moduleInfo.serviceArea = buildDefaultGrid();

			return moduleInfo;
		}
		static Grid buildDefaultGrid()
		{
			Grid grid = new Grid();
			grid.col = 0;
			grid.row = 0;
			grid.level = 0;
			grid.colspan = 1;
			grid.rowSpan = 1;
			grid.levelSpan = 1;

			return grid;
		}
		public static DisplayCapability buildDefaultDisplayCapability()
		{
			DisplayCapability displaycapability = new DisplayCapability()
			{
			displayName="Display1",
			windowTypeSupported =new WindowTypeCapabilities()
			{
				type = 0,
				maxNumberOfWindows = 1
			},
		};
		return displaycapability;
		}
		public static WindowCapability buildDefaultWindowCapability()
		{
			List<ImageType> imageTypeList = new List<ImageType>
								{
							ImageType.DYNAMIC,ImageType.STATIC
								};
			List<String> templateList = new List<String>
			{
				"Template1",
				"Template2"
			};
			WindowCapability windowCapabilities = new WindowCapability()
			{
				windowID = 0,
				softButtonCapabilities = buildDefaultSoftButtonCapabilities(),
				buttonCapabilities = buildDefaultButtonCapsList(),
				imageTypeSupported = imageTypeList,
				textFields = buildDefaultTextField(),
				templatesAvailable = templateList,
				numCustomPresetsAvailable = 0,
				imageFields = buildDefaultImageField()
			};
			return windowCapabilities;
		}
		public static SystemCapabilities buildDefaultSystemCapabilities()
		{
			List<ImageType> imageTypeList = new List<ImageType>
								{
							ImageType.DYNAMIC,ImageType.STATIC
								};
			List<String> templateList = new List<String>
			{
				"Template1",
				"Template2"
			};
			
			SystemCapabilities systemCapabilities = new SystemCapabilities()
			{
				navigationCapability = new NavigationCapability()
				{
					sendLocationEnabled = true,
					getWayPointsEnabled = true
				},
				 seatLocationCapability = new SeatLocationCapability()
				{
					rows = 1,
					columns = 1,
					levels = 1,
					seats = new List<SeatLocation>
				{
					new SeatLocation()
				{
					grid = new Grid
				{	col=0,
					row=0,
					level=0,
					colspan=1,
					rowSpan=1,
					levelSpan=1,

				}
				}
				},
				},
			displaycapability = new List<DisplayCapability>
				{
					new DisplayCapability()
				{
					displayName="Display1",

					windowTypeSupported =new WindowTypeCapabilities()
					{
					maxNumberOfWindows=1,
					type =0
					},

					windowCapabilities = new List<WindowCapability>
					{
						new WindowCapability()
						{

							windowID = 0,
							softButtonCapabilities = buildDefaultSoftButtonCapabilities(),
							buttonCapabilities = buildDefaultButtonCapsList(),
							imageTypeSupported = imageTypeList,
							textFields = buildDefaultTextField(),
							templatesAvailable = templateList,
							numCustomPresetsAvailable = 0,
							imageFields = buildDefaultImageField()


						}
					}
					

				}
				},

				phoneCapability = new PhoneCapability()
				{
					dialNumberEnabled = true
				},
				videoStreamingCapability = new VideoStreamingCapability()
				{
					preferredResolution = new ImageResolution()
					{
						resolutionWidth = 800,
						resolutionHeight = 480
					},
					supportedFormats = new List<VideoStreamingFormat>{
						new VideoStreamingFormat(){
							codec = VideoStreamingCodec.H264,
							protocol = VideoStreamingProtocol.RAW
						}
					},
					maxBitrate = 0,
					hapticSpatialDataSupported = true
				}
			};

			return systemCapabilities;
		}

		public static List<SoftButtonCapabilities> buildDefaultSoftButtonCapabilities()
		{
			SoftButtonCapabilities softButtonCapabilities1 = new SoftButtonCapabilities();
			softButtonCapabilities1.imageSupported = true;
			softButtonCapabilities1.longPressAvailable = true;
			softButtonCapabilities1.upDownAvailable = true;
			softButtonCapabilities1.shortPressAvailable = true;
			softButtonCapabilities1.textSupported = true;


			SoftButtonCapabilities softButtonCapabilities2 = new SoftButtonCapabilities();
			softButtonCapabilities2.imageSupported = true;
			softButtonCapabilities2.longPressAvailable = true;
			softButtonCapabilities2.upDownAvailable = true;
			softButtonCapabilities2.shortPressAvailable = true;
			softButtonCapabilities2.textSupported = true;


			SoftButtonCapabilities softButtonCapabilities3 = new SoftButtonCapabilities();
			softButtonCapabilities3.imageSupported = true;
			softButtonCapabilities3.longPressAvailable = true;
			softButtonCapabilities3.upDownAvailable = true;
			softButtonCapabilities3.shortPressAvailable = true;
			softButtonCapabilities3.textSupported = true;


			SoftButtonCapabilities softButtonCapabilities4 = new SoftButtonCapabilities();
			softButtonCapabilities4.imageSupported = true;
			softButtonCapabilities4.longPressAvailable = true;
			softButtonCapabilities4.upDownAvailable = true;
			softButtonCapabilities4.shortPressAvailable = true;
			softButtonCapabilities4.textSupported = true;


			List<SoftButtonCapabilities> softButtonCapsList = new List<SoftButtonCapabilities>()
			{
				softButtonCapabilities1,softButtonCapabilities2,softButtonCapabilities3,softButtonCapabilities4
			};

			return softButtonCapsList;
		}

		

	
		static List<Language> buildDefaultLanguageList()
		{
			List<Language> languages = new List<Language>
			{
				Language.EN_US,
				Language.EN_GB,
				Language.ES_MX,
				Language.FR_CA,
				Language.ES_ES,
				Language.DE_DE
			};
			return languages;
		}

		public static SisData buildSisData()
		{
			SisData sisData = new SisData();
			sisData.stationShortName = "stationShortName";
			sisData.stationIDNumber = buildStationIDNumber();
			sisData.stationLongName = "stationLongName";
			sisData.stationLocation = buildDefaultGPSData();
			sisData.stationMessage = "stationMessage";

			return sisData;
		}

		public static StationIDNumber buildStationIDNumber()
		{
			StationIDNumber stationIDNumber = new StationIDNumber();
			stationIDNumber.countryCode = 0;
			stationIDNumber.fccFacilityId = 0;

			return stationIDNumber;
		}

		public static RadioControlData buildRadioControlData()
		{
			RdsData rdsData = new RdsData();
			rdsData.PS = "PS";
			rdsData.RT = "RT";
			rdsData.CT = "2018-01-31T11:08:45.sTZD";
			rdsData.PI = "PI";
			rdsData.PTY = 0;
			rdsData.TP = true;
			rdsData.TA = true;
			rdsData.REG = "REG";

			StationIDNumber iDNumber = new StationIDNumber();
			iDNumber.countryCode = 0;
			iDNumber.fccFacilityId = 0;

			GPSData gPSData = new GPSData();
			gPSData.longitudeDegrees = -180;
			gPSData.latitudeDegrees = -90;
			gPSData.altitude = -99999;

			SisData sisData = new SisData();
			sisData.stationShortName = "short";
			sisData.stationIDNumber = iDNumber;
			sisData.stationLongName = "Station Long Name";
			sisData.stationLocation = gPSData;
			sisData.stationMessage = "Station Message";

			RadioControlData radioControlData = new RadioControlData();
			radioControlData.frequencyInteger = 0;
			radioControlData.frequencyFraction = 0;
			radioControlData.band = RadioBand.AM;
			radioControlData.rdsData = rdsData;
			radioControlData.availableHDs = 0;
			radioControlData.hdChannel = 0;
			radioControlData.signalStrength = 0;
			radioControlData.signalChangeThreshold = 0;
			radioControlData.radioEnable = true;
			radioControlData.state = RadioState.ACQUIRING;
			radioControlData.sisData = sisData;
			radioControlData.hdRadioEnable = true;

			List<int> availableHdChannels = new List<int>() { 0, 1, 2 };

			radioControlData.availableHdChannels = availableHdChannels;

			return radioControlData;
		}

		public static ClimateControlData buildClimateControlData()
		{
			Temperature temperature = new Temperature();
			temperature.unit = TemperatureUnit.FAHRENHEIT;
			temperature.value = 56;

			ClimateControlData climateControlData = new ClimateControlData();
			climateControlData.fanSpeed = 2;
			climateControlData.currentTemperature = temperature;
			climateControlData.desiredTemperature = temperature;
			climateControlData.acEnable = true;
			climateControlData.circulateAirEnable = true;
			climateControlData.autoModeEnable = true;
			climateControlData.defrostZone = DefrostZone.FRONT;
			climateControlData.dualModeEnable = true;
			climateControlData.acMaxEnable = false;
			climateControlData.ventilationMode = VentilationMode.UPPER;
			climateControlData.heatedSteeringWheelEnable = true;
			climateControlData.heatedWindshieldEnable = true;
			climateControlData.heatedRearWindowEnable = true;
			climateControlData.heatedMirrorsEnable = true;
			climateControlData.climateEnable = true;

			return climateControlData;
		}
	}
}